-- MySQL Workbench Synchronization
-- Generated: 2019-07-14 16:28
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Carlos MAsson

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE DATABASE IF NOT EXISTS `tfg` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXISTS `tfg`.`producto` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  `hash` VARCHAR(45) NULL DEFAULT NULL,
  `activo` TINYINT(1) NULL DEFAULT NULL,
  `id_externo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`estado` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`tipo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`propiedades` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  `costo` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`evento` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`venta` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_externo` VARCHAR(255) NOT NULL,
  `estado_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `estado_id`),
  INDEX `fk_venta_estado1_idx` (`estado_id` ASC),
  CONSTRAINT `fk_venta_estado1`
    FOREIGN KEY (`estado_id`)
    REFERENCES `tfg`.`estado` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`registro` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `hash` VARCHAR(255) NOT NULL,
  `fecha` DATETIME NULL DEFAULT NULL,
  `evento_id` INT(11) NOT NULL,
  `producto_id` INT(11) NOT NULL,
  `nodo_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `producto_id`, `nodo_id`),
  INDEX `fk_registro_evento1_idx` (`evento_id` ASC),
  INDEX `fk_registro_producto1_idx` (`producto_id` ASC),
  INDEX `fk_registro_nodo1_idx` (`nodo_id` ASC),
  CONSTRAINT `fk_registro_evento1`
    FOREIGN KEY (`evento_id`)
    REFERENCES `tfg`.`evento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_producto1`
    FOREIGN KEY (`producto_id`)
    REFERENCES `tfg`.`producto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_nodo1`
    FOREIGN KEY (`nodo_id`)
    REFERENCES `tfg`.`nodo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`nodo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  `ip` VARCHAR(15) NULL DEFAULT NULL,
  `hash` VARCHAR(45) NULL DEFAULT NULL,
  `credito` VARCHAR(45) NULL DEFAULT NULL,
  `activo` TINYINT(1) NULL DEFAULT NULL,
  `tipo_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_nodo_tipo1_idx` (`tipo_id` ASC),
  CONSTRAINT `fk_nodo_tipo1`
    FOREIGN KEY (`tipo_id`)
    REFERENCES `tfg`.`tipo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`rProductoPropiedades` (
  `producto_id` INT(11) NOT NULL,
  `propiedades_id` INT(11) NOT NULL,
  PRIMARY KEY (`producto_id`, `propiedades_id`),
  INDEX `fk_producto_has_propiedades_propiedades1_idx` (`propiedades_id` ASC),
  INDEX `fk_producto_has_propiedades_producto_idx` (`producto_id` ASC),
  CONSTRAINT `fk_producto_has_propiedades_producto`
    FOREIGN KEY (`producto_id`)
    REFERENCES `tfg`.`producto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_has_propiedades_propiedades1`
    FOREIGN KEY (`propiedades_id`)
    REFERENCES `tfg`.`propiedades` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`compra` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_externo` VARCHAR(255) NOT NULL,
  `estado_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `estado_id`),
  INDEX `fk_compra_estado1_idx` (`estado_id` ASC),
  CONSTRAINT `fk_compra_estado1`
    FOREIGN KEY (`estado_id`)
    REFERENCES `tfg`.`estado` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`rVentaProducto` (
  `venta_id` INT(11) NOT NULL,
  `producto_id` INT(11) NOT NULL,
  PRIMARY KEY (`venta_id`, `producto_id`),
  INDEX `fk_venta_has_producto_producto1_idx` (`producto_id` ASC),
  INDEX `fk_venta_has_producto_venta1_idx` (`venta_id` ASC),
  CONSTRAINT `fk_venta_has_producto_venta1`
    FOREIGN KEY (`venta_id`)
    REFERENCES `tfg`.`venta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_has_producto_producto1`
    FOREIGN KEY (`producto_id`)
    REFERENCES `tfg`.`producto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`rCompraProducto` (
  `compra_id` INT(11) NOT NULL,
  `producto_id` INT(11) NOT NULL,
  PRIMARY KEY (`compra_id`, `producto_id`),
  INDEX `fk_compra_has_producto_producto1_idx` (`producto_id` ASC),
  INDEX `fk_compra_has_producto_compra1_idx` (`compra_id` ASC),
  UNIQUE INDEX `producto_id_UNIQUE` (`producto_id` ASC),
  CONSTRAINT `fk_compra_has_producto_compra1`
    FOREIGN KEY (`compra_id`)
    REFERENCES `tfg`.`compra` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_has_producto_producto1`
    FOREIGN KEY (`producto_id`)
    REFERENCES `tfg`.`producto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`usuario` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  `password` VARCHAR(70) NOT NULL,
  `salt` VARCHAR(70) NOT NULL,
  `ultimoAcceso` DATETIME NULL DEFAULT NULL,
  `activo` TINYINT(1) NULL DEFAULT NULL,
  `nivelAcceso` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`rProductoNodo` (
  `producto_id` INT(11) NOT NULL,
  `nodo_id` INT(11) NOT NULL,
  PRIMARY KEY (`producto_id`, `nodo_id`),
  INDEX `fk_producto_has_nodo_nodo1_idx` (`nodo_id` ASC),
  INDEX `fk_producto_has_nodo_producto1_idx` (`producto_id` ASC),
  CONSTRAINT `fk_producto_has_nodo_producto1`
    FOREIGN KEY (`producto_id`)
    REFERENCES `tfg`.`producto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_producto_has_nodo_nodo1`
    FOREIGN KEY (`nodo_id`)
    REFERENCES `tfg`.`nodo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tfg`.`registro_has_producto_has_nodo` (
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
