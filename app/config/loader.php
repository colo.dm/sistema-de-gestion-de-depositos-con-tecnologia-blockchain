<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * Register Namespaces
 */
$loader->registerNamespaces([
    'TFG\Models' => APP_PATH . '/common/models/',
    'TFG\Controllers' => APP_PATH . '/common/controllers/',
    'TFG'        => APP_PATH . '/common/library/',
    'Custom'        => APP_PATH . '/custom/models/',
]);

/**
 * Register module classes
 */
$loader->registerClasses([
    'TFG\Modules\Access\Module' => APP_PATH . '/modules/access/Module.php',
    'TFG\Modules\Data\Module' => APP_PATH . '/modules/data/Module.php',
    'TFG\Modules\Entity\Module' => APP_PATH . '/modules/entity/Module.php',
    'TFG\Modules\Frontend\Module' => APP_PATH . '/modules/frontend/Module.php',
    'TFG\Modules\Layout\Module' => APP_PATH . '/modules/layout/Module.php',
    'TFG\Modules\Menu\Module' => APP_PATH . '/modules/menu/Module.php',
    'TFG\Modules\Service\Module'      => APP_PATH . '/modules/service/Module.php',
    'TFG\Modules\Cli\Module'      => APP_PATH . '/modules/cli/Module.php',
]);

$loader->register();

/*
spl_autoload_register(function ($className) {
    if (substr( $className, 0, 10 ) === "DataEntity") {
        include APP_PATH . '/custom/models/'.$className . '.php';
        return true;
    }
});*/
