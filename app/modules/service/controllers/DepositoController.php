<?php

namespace TFG\Modules\Service\Controllers;

use TFG\Modules\Service\API\Blockchain as Blockchain;
use TFG\Modules\Service\Models\Producto as Producto;

class DepositoController extends BaseController {

    private function obtenerExistencias() {
        $blockchainAPI = new Blockchain();
        $contratos = $blockchainAPI->informacionDeCuenta();
        $existencias = [];
        foreach ($contratos as $contrato) {
            $dbProducto = Producto::findFirst("hash = '".$contrato->RFID."' and activo = 1");
            $producto = new \stdClass();
            $producto->id = (int)$dbProducto->id;
            $producto->nombre = $dbProducto->getNombre();
            $producto->nodo = $dbProducto->getNombreNodo();
            $existencias[] = $producto;
        }
        return $existencias;
    }

    public function consultarDepositoAction() {
        $existencias = $this->obtenerExistencias();
        $this->response->setJsonContent($existencias);
        return $this->response->send();
    }

    public function exportarExistenciasAction() {
        $temp = tempnam(sys_get_temp_dir(), 'exportData');
        $rs = fopen($temp, 'w');
        fputcsv($rs, ['id','producto','nodo']);
        foreach ($this->obtenerExistencias() as $producto) {
            fputcsv($rs, [$producto->id, $producto->nombre, $producto->nodo]);
        }
        fclose($temp);
        return $this->sendCsv($temp);
    }

}

