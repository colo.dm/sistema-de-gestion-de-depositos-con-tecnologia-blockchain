<?php

namespace TFG\Modules\Service\Controllers;

use TFG\Modules\Service\Models\Compra as Compra;

class PedidoCompraController extends BaseController {

    public function ingresarPedidoCompraAction($id) {
        $pedidoCompra = Compra::findFirst("id_externo = '".$id."'");
        if ($pedidoCompra) {
            $pedidoCompra->marcarIngreso();
            $pedidoCompra->save();
            $this->sendStatus200();
        } else {
            $this->sendStatus404();
        }
    }

}

