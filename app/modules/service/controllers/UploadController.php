<?php
namespace TFG\Modules\Service\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Security\Random;
use TFG\Modules\Service\ServiceModels\Service as Service;
use TFG\Modules\Service\ServiceModels\Resource as Resource;
use TFG\Modules\Service\ServiceModels\Task as Task;
use TFG\Modules\Service\ServiceModels\TaskStatus as TaskStatus;

class ImportarController extends \Entity\Controllers\ControllerBase {

    public function nuevoPedidoCompraAction() {
        throw new Exception("Metodo aún no implementado", 1);
    }

    public function nuevoPedidoVentaAction() {
        throw new Exception("Metodo aún no implementado", 1);
    }

    public function verificarExistenciaDelProductoAction() {
        throw new Exception("Metodo aún no implementado", 1);
    }

    public function file($token) {
        $this->view->setVar('token', $token);
        return $this->view->render('upload','uploadForm');
    }

    public function uploadFile($token) {
        if ($this->request->hasFiles() == true) {
            $service = Service::findFirst("token = '$token'");
            if ($service) {
                $resource = $this->moveToPendingFiles($service, $this->request->getUploadedFiles());
                $data = new \stdClass();
                $data->fileName = $resource->name;
                $data->md5 = $resource->md5Hash;
                $data->status = (int)$resource->Task->taskStatusId;
                $data->taskToken = $resource->getResourceToken();
                $this->response->setJsonContent($data);
                return $this->response->send();
            }

        }
    }

    protected function moveToPendingFiles($service, $files) {
        foreach ($files as $file) {
            $config = $this->getDI()->get('config');
            $uploadsDir = $config['application']['uploads'];
            $newFileName = $this->getRandomFileName($uploadsDir);
            $file->moveTo($uploadsDir.$newFileName);
            $task = new Task();
            $task->serviceId = $service->id;
            $task->name = 'import file '.$file->getName();
            $task->taskStatusId = 1;
            $task->save();
            $resource = new Resource();
            $resource->name = $file->getName();
            $resource->path = $newFileName;
            $resource->size = $file->getSize();
            $resource->md5Hash = md5_file($uploadsDir.$newFileName);
            $resource->fileStatusId = 1;
            $resource->importServiceId = $service->id;
            $resource->taskId = $task->id;
            $resource->save();
            return $resource;
            /*
            fileStatusId
            1 pending
            2 start processed
            3 processing
            4 finishing
            5 finished
            6 cancelled by user
            7 cancelled by system
            8 error
            */
        }
    }

    protected function getRandomFileName($uploadsDir) {
        $dir = chr(random_int(97, 122)).chr(random_int(97, 122));
        if (!is_dir($uploadsDir.$dir)) {
            mkdir($uploadsDir.$dir);
        }

        $random = new Random();
        do {
            $fileName = $dir.'/'.$random->uuid().'.'.$random->base58(7);
        } while (file_exists($fileName));

        return $fileName;
    }

}
