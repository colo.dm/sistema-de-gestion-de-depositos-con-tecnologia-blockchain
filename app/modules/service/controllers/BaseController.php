<?php
namespace TFG\Modules\Service\Controllers;

use Phalcon\Mvc\Controller;

class BaseController extends \TFG\Controllers\ControllerBase {

    private $usuario;
    private $id;
    private $nivelDeAcceso;

    protected function getResource($resourceName, $replacers = []) {
        $filePath = __DIR__ . '/../resources/js/'.$resourceName.'.json';
        if (file_exists($filePath)) {
            $rs = file_get_contents($filePath);
            foreach ($replacers as $key => $value) {
                $rs = str_replace('{{'.$key.'}}', $value, $rs);
            }
            return json_decode($rs);
        } else {
            return false;
        }
    }

    public function rsAction($resourceName) {
        $layout = $this->getResource($resourceName);
        if ($layout) {
            $this->response->setJsonContent($layout);
            return $this->response->send();
        } else {
            return $this->sendEntityStatus404();
        }
    }

    function setHeader() {
        $this->response->setHeader('Access-Control-Allow-Origin', '*');
        $this->response->setContentType('application/json', 'UTF-8');
    }

    function sendStatus200($message = 'Ok', $layoutUrl=null) {
        $this->setHeader();
        $this->response->setStatusCode(200, $message);
        $this->response->setJsonContent([
            'msg' => $message,
            'layoutUrl' => $layoutUrl
        ]);
        return $this->response->send();
    }

    function sendStatus404($message = 'Not found', $details = null) {
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setJsonContent(['msg' => $message.(isset($details) ? ": $details" : "")]);
        $this->response->setStatusCode(404, $message);
        return $this->response->send();
    }

    function sendStatus501($entity = null, $message = 'Internal error') {
        $this->response->setContentType('application/json', 'UTF-8');
        if (isset($entity)) {
            foreach ($entity->getMessages() as $errorMessage) {
                $msg .= $errorMessage.'. ';
            }
        }
        $this->response->setJsonContent(['msg' => $message.(isset($msg) ? ": $msg" : '')]);
        $this->response->setStatusCode(501, $message);
        return $this->response->send();
    }

    function sendCsv($archivo) {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $response = $this->response;
        $filetype = filetype($archivo);
        $filesize = filesize($archivo);
        $response->setHeader("Cache-Control", 'must-revalidate, post-check=0, pre-check=0');
        $response->setHeader("Content-Description", 'descargar archivo');
        $response->setHeader("Content-Type", $filetype);
        $response->setHeader("Content-Length", $filesize);
        $response->setContentType('text/csv', 'UTF-8');
        $response->setFileToSend($archivo, str_replace(" ","-",basename($archivo)), true);
        return $response;
    }
}
