<?php
namespace TFG\Modules\Service;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();

        $loader->registerNamespaces([
            'TFG\Modules\Service\Controllers' => __DIR__ . '/controllers/',
            'TFG\Modules\Service\API' => __DIR__ . '/API/',
            'TFG\Modules\Service\Models' => __DIR__ . '/models/',
            'TFG\Modules\Service\ServiceModels' => APP_PATH . '/models/serviceModels/',
            'TFG\Modules\Data\Models' => APP_PATH . '/modules/data/models/',
        ]);

        $loader->register();
    }

    /**
     * Registers services related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        /**
         * Setting up the view component
         */
        $di->set('view', function () {
            $view = new View();
            $view->setDI($this);
            $view->setViewsDir(__DIR__ . '/views/');

            $view->registerEngines([
                '.volt'  => 'voltShared',
                '.phtml' => PhpEngine::class
            ]);

            return $view;
        });
    }
}
