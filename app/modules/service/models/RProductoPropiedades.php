<?php

namespace TFG\Modules\Service\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class RProductoPropiedades extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $producto_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $propiedades_id;

    /**
     * Method to set the value of field producto_id
     *
     * @param integer $producto_id
     * @return $this
     */
    public function setProductoId($producto_id)
    {
        $this->producto_id = $producto_id;

        return $this;
    }

    /**
     * Method to set the value of field propiedades_id
     *
     * @param integer $propiedades_id
     * @return $this
     */
    public function setPropiedadesId($propiedades_id)
    {
        $this->propiedades_id = $propiedades_id;

        return $this;
    }

    /**
     * Returns the value of field producto_id
     *
     * @return integer
     */
    public function getProductoId()
    {
        return $this->producto_id;
    }

    /**
     * Returns the value of field propiedades_id
     *
     * @return integer
     */
    public function getPropiedadesId()
    {
        return $this->propiedades_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfg");
        $this->belongsTo('producto_id', '\Producto', 'id', ['alias' => 'Producto']);
        $this->belongsTo('propiedades_id', '\Propiedades', 'id', ['alias' => 'Propiedades']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rProductoPropiedades';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RProductoPropiedades[]|RProductoPropiedades
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RProductoPropiedades
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
