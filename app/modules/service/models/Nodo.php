<?php

namespace TFG\Modules\Service\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class Nodo extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    protected $nombre;

    /**
     *
     * @var string
     * @Column(type="string", length=15, nullable=true)
     */
    protected $ip;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $hash;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $credito;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=true)
     */
    protected $activo;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $tipo_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field nombre
     *
     * @param string $nombre
     * @return $this
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Method to set the value of field ip
     *
     * @param string $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Method to set the value of field hash
     *
     * @param string $hash
     * @return $this
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Method to set the value of field credito
     *
     * @param string $credito
     * @return $this
     */
    public function setCredito($credito)
    {
        $this->credito = $credito;

        return $this;
    }

    /**
     * Method to set the value of field activo
     *
     * @param integer $activo
     * @return $this
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Method to set the value of field tipo_id
     *
     * @param integer $tipo_id
     * @return $this
     */
    public function setTipoId($tipo_id)
    {
        $this->tipo_id = $tipo_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Returns the value of field ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Returns the value of field hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Returns the value of field credito
     *
     * @return string
     */
    public function getCredito()
    {
        return $this->credito;
    }

    /**
     * Returns the value of field activo
     *
     * @return integer
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Returns the value of field tipo_id
     *
     * @return integer
     */
    public function getTipoId()
    {
        return $this->tipo_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfg");
        $this->hasMany('id', 'RProductoNodo', 'nodo_id', ['alias' => 'RProductoNodo']);
        $this->hasMany('id', 'Registro', 'nodo_id', ['alias' => 'Registro']);
        $this->belongsTo('tipo_id', '\Tipo', 'id', ['alias' => 'Tipo']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'nodo';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Nodo[]|Nodo
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Nodo
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
