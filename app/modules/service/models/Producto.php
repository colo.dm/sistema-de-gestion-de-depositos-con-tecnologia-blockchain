<?php

namespace TFG\Modules\Service\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class Producto extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    protected $nombre;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $hash;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=true)
     */
    protected $activo;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    protected $id_externo;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field nombre
     *
     * @param string $nombre
     * @return $this
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Method to set the value of field hash
     *
     * @param string $hash
     * @return $this
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Method to set the value of field activo
     *
     * @param integer $activo
     * @return $this
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Method to set the value of field id_externo
     *
     * @param string $id_externo
     * @return $this
     */
    public function setIdExterno($id_externo)
    {
        $this->id_externo = $id_externo;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Returns the value of field hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Returns the value of field activo
     *
     * @return integer
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Returns the value of field id_externo
     *
     * @return string
     */
    public function getIdExterno()
    {
        return $this->id_externo;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfg");
        $this->hasMany('id', 'RCompraProducto', 'producto_id', ['alias' => 'RCompraProducto']);
        $this->hasMany('id', 'RProductoNodo', 'producto_id', ['alias' => 'RProductoNodo']);
        $this->hasMany('id', 'RProductoPropiedades', 'producto_id', ['alias' => 'RProductoPropiedades']);
        $this->hasMany('id', 'RVentaProducto', 'producto_id', ['alias' => 'RVentaProducto']);
        $this->hasMany('id', 'Registro', 'producto_id', ['alias' => 'Registro']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'producto';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Producto[]|Producto
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Producto
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getNombreNodo() {
        return count($this->RProductoNodo->Nodo) > 0 ? $this->RProductoNodo->Nodo[0]->getNombre() : null;
    }

}
