<?php

namespace TFG\Modules\Service\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class RVentaProducto extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $venta_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $producto_id;

    /**
     * Method to set the value of field venta_id
     *
     * @param integer $venta_id
     * @return $this
     */
    public function setVentaId($venta_id)
    {
        $this->venta_id = $venta_id;

        return $this;
    }

    /**
     * Method to set the value of field producto_id
     *
     * @param integer $producto_id
     * @return $this
     */
    public function setProductoId($producto_id)
    {
        $this->producto_id = $producto_id;

        return $this;
    }

    /**
     * Returns the value of field venta_id
     *
     * @return integer
     */
    public function getVentaId()
    {
        return $this->venta_id;
    }

    /**
     * Returns the value of field producto_id
     *
     * @return integer
     */
    public function getProductoId()
    {
        return $this->producto_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfg");
        $this->belongsTo('producto_id', '\Producto', 'id', ['alias' => 'Producto']);
        $this->belongsTo('venta_id', '\Venta', 'id', ['alias' => 'Venta']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rVentaProducto';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RVentaProducto[]|RVentaProducto
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RVentaProducto
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
