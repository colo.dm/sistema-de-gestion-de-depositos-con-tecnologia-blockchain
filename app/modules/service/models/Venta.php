<?php

namespace TFG\Modules\Service\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class Venta extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    protected $id_externo;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $estado_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field id_externo
     *
     * @param string $id_externo
     * @return $this
     */
    public function setIdExterno($id_externo)
    {
        $this->id_externo = $id_externo;

        return $this;
    }

    /**
     * Method to set the value of field estado_id
     *
     * @param integer $estado_id
     * @return $this
     */
    public function setEstadoId($estado_id)
    {
        $this->estado_id = $estado_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field id_externo
     *
     * @return string
     */
    public function getIdExterno()
    {
        return $this->id_externo;
    }

    /**
     * Returns the value of field estado_id
     *
     * @return integer
     */
    public function getEstadoId()
    {
        return $this->estado_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfg");
        $this->hasMany('id', 'RVentaProducto', 'venta_id', ['alias' => 'RVentaProducto']);
        $this->belongsTo('estado_id', '\Estado', 'id', ['alias' => 'Estado']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'venta';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Venta[]|Venta
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Venta
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
