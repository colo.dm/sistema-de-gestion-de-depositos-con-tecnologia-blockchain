<?php

namespace TFG\Modules\Service\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class RCompraProducto extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $compra_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $producto_id;

    /**
     * Method to set the value of field compra_id
     *
     * @param integer $compra_id
     * @return $this
     */
    public function setCompraId($compra_id)
    {
        $this->compra_id = $compra_id;

        return $this;
    }

    /**
     * Method to set the value of field producto_id
     *
     * @param integer $producto_id
     * @return $this
     */
    public function setProductoId($producto_id)
    {
        $this->producto_id = $producto_id;

        return $this;
    }

    /**
     * Returns the value of field compra_id
     *
     * @return integer
     */
    public function getCompraId()
    {
        return $this->compra_id;
    }

    /**
     * Returns the value of field producto_id
     *
     * @return integer
     */
    public function getProductoId()
    {
        return $this->producto_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfg");
        $this->belongsTo('compra_id', '\Compra', 'id', ['alias' => 'Compra']);
        $this->belongsTo('producto_id', '\Producto', 'id', ['alias' => 'Producto']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rCompraProducto';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RCompraProducto[]|RCompraProducto
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RCompraProducto
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
