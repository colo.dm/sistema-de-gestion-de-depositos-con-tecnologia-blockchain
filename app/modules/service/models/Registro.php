<?php

namespace TFG\Modules\Service\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class Registro extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    protected $hash;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $fecha;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $evento_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $producto_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $nodo_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field hash
     *
     * @param string $hash
     * @return $this
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Method to set the value of field fecha
     *
     * @param string $fecha
     * @return $this
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Method to set the value of field evento_id
     *
     * @param integer $evento_id
     * @return $this
     */
    public function setEventoId($evento_id)
    {
        $this->evento_id = $evento_id;

        return $this;
    }

    /**
     * Method to set the value of field producto_id
     *
     * @param integer $producto_id
     * @return $this
     */
    public function setProductoId($producto_id)
    {
        $this->producto_id = $producto_id;

        return $this;
    }

    /**
     * Method to set the value of field nodo_id
     *
     * @param integer $nodo_id
     * @return $this
     */
    public function setNodoId($nodo_id)
    {
        $this->nodo_id = $nodo_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Returns the value of field fecha
     *
     * @return string
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Returns the value of field evento_id
     *
     * @return integer
     */
    public function getEventoId()
    {
        return $this->evento_id;
    }

    /**
     * Returns the value of field producto_id
     *
     * @return integer
     */
    public function getProductoId()
    {
        return $this->producto_id;
    }

    /**
     * Returns the value of field nodo_id
     *
     * @return integer
     */
    public function getNodoId()
    {
        return $this->nodo_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfg");
        $this->belongsTo('evento_id', '\Evento', 'id', ['alias' => 'Evento']);
        $this->belongsTo('nodo_id', '\Nodo', 'id', ['alias' => 'Nodo']);
        $this->belongsTo('producto_id', '\Producto', 'id', ['alias' => 'Producto']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'registro';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Registro[]|Registro
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Registro
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
