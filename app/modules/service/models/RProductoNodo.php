<?php

namespace TFG\Modules\Service\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class RProductoNodo extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $producto_id;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $nodo_id;

    /**
     * Method to set the value of field producto_id
     *
     * @param integer $producto_id
     * @return $this
     */
    public function setProductoId($producto_id)
    {
        $this->producto_id = $producto_id;

        return $this;
    }

    /**
     * Method to set the value of field nodo_id
     *
     * @param integer $nodo_id
     * @return $this
     */
    public function setNodoId($nodo_id)
    {
        $this->nodo_id = $nodo_id;

        return $this;
    }

    /**
     * Returns the value of field producto_id
     *
     * @return integer
     */
    public function getProductoId()
    {
        return $this->producto_id;
    }

    /**
     * Returns the value of field nodo_id
     *
     * @return integer
     */
    public function getNodoId()
    {
        return $this->nodo_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tfg");
        $this->belongsTo('nodo_id', '\Nodo', 'id', ['alias' => 'Nodo']);
        $this->belongsTo('producto_id', '\Producto', 'id', ['alias' => 'Producto']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rProductoNodo';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return RProductoNodo[]|RProductoNodo
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return RProductoNodo
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
