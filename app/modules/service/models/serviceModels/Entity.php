<?php

class Entity extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $lastUpdate;

    /**
     *
     * @var string
     * @Column(type="string", length=190, nullable=true)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=true)
     */
    public $entityHash;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $ownerId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $entityTypeId;

    /**
     *
     * @var string
     * @Column(type="string", length=120, nullable=true)
     */
    public $format;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $layoutId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'DataEntity', 'entityId', ['alias' => 'DataEntity']);
        $this->hasMany('id', 'DataSelector', 'entityId', ['alias' => 'DataSelector']);
        $this->hasMany('id', 'Field', 'entityId', ['alias' => 'Field']);
        $this->hasMany('id', 'Field', 'entityIdReference', ['alias' => 'Field']);
        $this->hasMany('id', 'REntityAccessGroup', 'entityId', ['alias' => 'REntityAccessGroup']);
        $this->hasMany('id', 'Service', 'entityId', ['alias' => 'Service']);
        $this->belongsTo('entityTypeId', '\EntityType', 'id', ['alias' => 'EntityType']);
        $this->belongsTo('layoutId', '\Layout', 'id', ['alias' => 'Layout']);
        $this->belongsTo('ownerId', '\Owner', 'id', ['alias' => 'Owner']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'entity';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Entity[]|Entity
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Entity
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
