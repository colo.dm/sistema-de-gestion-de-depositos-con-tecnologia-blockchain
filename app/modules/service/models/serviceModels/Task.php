<?php

namespace TFG\Modules\Service\ServiceModels;

use TFG\Modules\Service\ServiceModels\Resource as Resource;
use TFG\Modules\Service\ServiceModels\Service as Service;
use TFG\Modules\Service\ServiceModels\TaskStatus as TaskStatus;

class Task extends \Entity\Models\TableExtended {
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $serviceId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $taskStatusId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'Entity\Modules\Service\Models\Resource', 'taskId', ['alias' => 'Resource']);
        $this->belongsTo('serviceId', 'Entity\Modules\Service\Models\Service', 'id', ['alias' => 'Service']);
        $this->belongsTo('taskStatusId', 'Entity\Modules\Service\Models\TaskStatus', 'id', ['alias' => 'TaskStatus']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'task';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Task[]|Task
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Task
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}
