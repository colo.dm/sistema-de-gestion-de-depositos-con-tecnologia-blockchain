<?php

namespace TFG\Modules\Service\ServiceModels;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use TFG\Modules\Service\ServiceModels\Source as Source;

class SourceType extends \Entity\Models\TableExtended
{
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'Entity\Modules\Service\Models\Source', 'sourceTypeId', ['alias' => 'Source']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sourceType';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Sourcetype[]|Sourcetype
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Sourcetype
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
