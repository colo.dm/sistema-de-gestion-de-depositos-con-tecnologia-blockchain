<?php
namespace TFG\Modules\Service\ServiceModels;

use TFG\Modules\Service\ServiceModels\Task as Task;

class Resource extends \Entity\Models\TableExtended {
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $taskId;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $path;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->belongsTo('taskId', 'Entity\Modules\Service\Models\Task', 'id', ['alias' => 'Task']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'resource';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Resource[]|Resource
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Resource
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getResourceToken() {
        return dechex($this->id).substr(md5($this->path), 0, 20).substr(md5($this->taskId),0,15);
    }
    /*
    "19 849f18cfb7ca9ab221b1 37693cfc748049e"
    "1a 7da5034bf28a6b2e9c83 1ff1de774005f8d"
    */

}
