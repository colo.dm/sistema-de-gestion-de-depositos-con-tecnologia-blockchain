<?php
namespace TFG\Modules\Service\ServiceModels;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use Phalcon\Security\Random;
use TFG\Modules\Entity\Models\Entity as Entity;
use TFG\Modules\Layout\Models\Layout as Layout;
use TFG\Modules\Layout\Models\LayoutRow as LayoutRow;
use TFG\Modules\Layout\Models\LayoutColumn as LayoutColumn;
use TFG\Modules\Layout\Models\Spec as Spec;
use TFG\Modules\Data\Models\DataSelector as DataSelector;
use TFG\Modules\Service\ServiceModels\ServiceType as ServiceType;
use TFG\Modules\Service\ServiceModels\Source as Source;

class Service extends \Entity\Models\TableExtended {
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $serviceTypeId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $sourceId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $entityId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $layoutId;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $metadata;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $token;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=false)
     */
    public $isEnabled;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'Entity\Modules\Service\Models\Task', 'serviceId', ['alias' => 'Task']);
        $this->belongsTo('layoutId', 'Entity\Modules\Layout\Models\Layout', 'id', ['alias' => 'Layout']);
        $this->belongsTo('serviceTypeId', 'Entity\Modules\Service\Models\ServiceType', 'id', ['alias' => 'ServiceType']);
        $this->belongsTo('sourceId', 'Entity\Modules\Service\Models\Source', 'id', ['alias' => 'Source']);
        $this->belongsTo('entityId', 'Entity\Modules\Entity\Models\Entity', 'id', ['alias' => 'Entity']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'service';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Service[]|Service
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Service
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    protected function beforeValidationOnCreate() {
        $layout = new Layout();
        $layout->name = $this->Entity->entityHash.'-service-tmp';
        $layout->layoutTypeId = 1;
        $layout->save();

        $this->layoutId = $layout->id;
        $random = new Random();
        $this->token = $random->base64Safe(30);
    }

    protected function afterCreate() {
        $this->Layout->name = $this->Entity->entityHash.'-service-'.$this->id;
        $this->Layout->save();

        $row = new LayoutRow();
        $row->Layout = $this->Layout;
        $row->save();
        $column = new LayoutColumn();
        $column->LayoutRow = $row;
        $column->xs = 24;
        $column->sm = 24;
        $column->md = 24;
        $column->lg = 24;
        $column->save();
        $column->addSpec($this, $this->Layout->name);
    }

    protected function afterDelete()
    {
        if ($this->Layout) {
            $this->Layout->delete();
        }
    }
}
