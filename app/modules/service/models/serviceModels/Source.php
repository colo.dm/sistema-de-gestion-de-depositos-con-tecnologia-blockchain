<?php

namespace TFG\Modules\Service\ServiceModels;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use TFG\Modules\Service\ServiceModels\ImportService as ImportService;
use TFG\Modules\Service\ServiceModels\SourceType as SourceType;

class Source extends \Entity\Models\TableExtended
{
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $sourceTypeId;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $metadataConnection;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'Entity\Modules\Service\Models\ImportService', 'sourceId', ['alias' => 'ImportService']);
        $this->belongsTo('sourceTypeId', 'Entity\Modules\Service\Models\SourceType', 'id', ['alias' => 'SourceType']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'source';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Source[]|Source
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Source
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
