<?php
namespace TFG\Modules\Service\ServiceModels;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use TFG\Modules\Service\ServiceModels\Service as Service;

class ServiceType extends \Entity\Models\TableExtended {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $lastUpdate;

    /**
     *
     * @var string
     * @Column(type="string", length=190, nullable=true)
     */
    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'Entity\Modules\Service\Models\Service', 'serviceTypeId', ['alias' => 'Service']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'serviceType';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Servicetype[]|Servicetype
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Servicetype
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
