<?php

namespace Entity\Modules\Import\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use Entity\Modules\Entity\Models\Entity as Entity;
use Entity\Modules\Layout\Models\Layout as Layout;
use Entity\Modules\Layout\Models\LayoutRow as LayoutRow;
use Entity\Modules\Layout\Models\LayoutColumn as LayoutColumn;
use Entity\Modules\Layout\Models\Spec as Spec;
use Entity\Modules\Data\Models\DataSelector as DataSelector;

class ImportService extends \Entity\Models\TableExtended
{
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $dataSelectorIdSaver;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $sourceId;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $metadataColumns;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $token;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=false)
     */
    public $isEnabled;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->belongsTo('dataSelectorIdSaver', 'Entity\Modules\Data\Models\DataSelector', 'id', ['alias' => 'DataSelector']);
        $this->belongsTo('sourceId', 'Entity\Modules\Service\Models\Source', 'id', ['alias' => 'Source']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'importService';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Importservice[]|Importservice
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Importservice
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    protected function afterCreate() {
        //create default layout
        $dataSelectorIdSaver = DataSelector::findFirstById($this->dataSelectorIdSaver);
        $entity = Entity::findFirstById($dataSelectorIdSaver->entityId);

        $layout = new Layout();
        $layout->name = $entity->entityHash.'-importService-'.$this->id;
        $layout->layoutTypeId = 1;
        $layout->save();

        $row = new LayoutRow();
        $row->Layout = $layout;
        $row->save();

        $column = new LayoutColumn();
        $column->LayoutRow = $row;
        $column->xs = 24;
        $column->sm = 24;
        $column->md = 24;
        $column->lg = 24;
        $column->save();

        $spec = new Spec();
        $spec->name = $layout->name;
        $spec->titleColor = '#0d695a';
        $spec->label = $this->name;
        $spec->viewTypeId = 1;
        $spec->LayoutColumn = $column;
        $spec->dataSelector = $this->dataSelectorIdSaver;
        $spec->save();
    }

    protected function afterDelete()
    {
        $dataSelectorIdSaver = DataSelector::findFirstById($this->dataSelectorIdSaver);
        $entity = Entity::findFirstById($dataSelectorIdSaver->entityId);

        $layout = Layout::findFirst(array(
            "name = '$entity->entityHash-importService-$this->id'"
        ));

        if ($layout) {
            $layout->delete();
        }

        $dataSelectorIdSaver2 = DataSelector::findFirstById($this->dataSelectorIdSaver);

        if ($dataSelectorIdSaver2) {
            $dataSelectorIdSaver2->delete();
        }
    }
}
