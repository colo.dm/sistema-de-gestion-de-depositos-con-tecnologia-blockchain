<?php

namespace TFG\Modules\Service\ServiceModels;

use TFG\Modules\Service\ServiceModels\Task as Task;

class TaskStatus extends \Entity\Models\TableExtended {
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'Entity\Modules\Service\Models\Task', 'taskStatusId', ['alias' => 'Task']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'taskStatus';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Taskstatus[]|Taskstatus
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Taskstatus
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
