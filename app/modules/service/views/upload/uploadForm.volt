<form action='../uploadFile/{{ token }}' name='uploadFileForm' method='post' class='form-horizontal' enctype='multipart/form-data' >
<h4>Import File</h4>
<table style="margin-bottom: 20px;">
    <tr>
        <td style="padding:5px;padding-right:30px;">
            <label>File: </label>
        </td>
        <td>
            <input type="file" name="files[]" multiple>
        </td>
    </tr>
</table>
<?php echo $this->tag->submitButton(array( "Confirm" , "class" => "btn btn-primary" ) ); ?>
</form>