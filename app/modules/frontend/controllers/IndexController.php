<?php

namespace TFG\Modules\Frontend\Controllers;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $this->assets->addCss("css/font-awesome-4.7.0/css/font-awesome.min.css");
        //$this->assets->addJs("js/common.js");
        //$this->assets->addJs("js/axios.js");
        //$this->assets->addJs("js/lodash.js");
        //$this->assets->addJs("js/vue.js");
        //$this->assets->addJs("js/ui.js");
        //$this->assets->addJs("js/main.js");
    }

    public function loginAction()
    {
        $this->assets->addCss("css/login.css");
    }

}

