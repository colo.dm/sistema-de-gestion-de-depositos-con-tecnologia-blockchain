<?php
namespace TFG\Modules\Cli\Tasks;

use TFG\Modules\Service\Models\Service as Service;
use TFG\Modules\Service\Models\Task as Task;
use TFG\Modules\Service\Models\Resource as Resource;
use TFG\Modules\Entity\Models\Field as Field;
use TFG\Modules\Data\Models\MultipleEntityRelated as MultipleEntityRelated;

class ImportcsvTask extends \Phalcon\Cli\Task
{
    private $importService;
    private $entities = [];
    private $fields = [];
    private $fieldTempTable = [];
    private $mainEntityHash = '';
    private $maxIndexColumn = -1;
    private $connection;
    private $processId;

    public function mainAction() {
        //~/Proyectos/Phalcon/entity> php app/bootstrap_cli.php Importcsv
        echo 'Use actions [pending | specificFile]'.PHP_EOL;
    }

    public function pendingAction() {
        $parameters["conditions"] = 'taskStatusId = 1
            AND serviceId in (select service.id from [Entity\Modules\Service\Models\Service] as service where serviceTypeId =1
            AND sourceId = 1)';
        $parameters["order"] = "[Entity\Modules\Service\Models\Task].id";
        $task = Task::findFirst($parameters);
        if ($task) {
            $this->loadFields($task->Service);
            $this->importFiles($task);
        }
    }

    protected function createTemporaryFormatTable($tableQuery) {
        $tableName = $this->processId.'_'.md5($tableQuery);
        $sql = "CREATE TEMPORARY TABLE $tableName
                (id INT UNSIGNED NOT NULL PRIMARY KEY,
                format CHAR(32),
                INDEX my_index_name (format)) ".$tableQuery;
        $data = $this->connection->query($sql);
        return $tableName;
    }

    protected function loadFields($service) {
        $this->entities = [];
        $this->fields = [];
        $this->mainEntityHash = $service->Entity->entityHash;
        $this->connection = $this->db;
        $this->processId = 'T'.$service->id.'_'.uniqid();

        foreach ($service->Layout->LayoutRow as $row) {
            foreach ($row->LayoutColumn as $column) {
                foreach ($column->spec as $spec) {
                    $entity = $spec->DataSelector->Entity;
                    $this->entities[$entity->entityHash] = $entity;

                    foreach ($spec->DataSelector->RDataSelectorField as $rFieldIndex => $rField) {
                        $this->fields[] = $rField->Field;
                        if (!empty($rField->Field->getEntityIdReference())) {
                            $this->fieldTempTable[$rFieldIndex] = $this->createTemporaryFormatTable($rField->getFieldFormatQuery());
                        }
                    }
                }
            }
        }

        $this->maxIndexColumn = count($this->fields);
    }

    protected function importFiles(Task $task) {
        $config = $this->getDI()->get('config');
        $uploadsDir = $config['application']['uploads'];
        foreach ($task->Resource as $resource) {
            $handle = fopen($uploadsDir.$resource->path, 'r');
            while ($line = fgetcsv($handle)) {
                $this->importLine($line);
            }
        }
    }

    protected function importLine($line) {
        $relations = [];
        $save = false;
        $msg = '';
        $dataEntities = [];
        foreach ($this->entities as $hash => $entity) {
            $dataEntities[$hash] = $entity->getDataInstance();
        }
        //var_dump(array_keys($this->fields)); die();
        foreach ($line as $index => $value) {
            if ($index >= $this->maxIndexColumn) {
                break;
            }
            if ($value != null){
                $field = $this->fields[$index];
                $fieldName = $field->getColumnName();
                if (!empty($field->getEntityIdReference()) && ($field->getFieldTypeId() != 17)) {
                    $sql = 'SELECT id from '.$this->fieldTempTable[$index]." where format = '".md5($value)."' LIMIT 1";
                    $data = $this->connection->query($sql);
                    $data->setFetchMode(\Phalcon\Db::FETCH_ASSOC);
                    $results = $data->fetchAll();
                    if (count($results)>0) {
                        $value = (int) $results[0]['id'];
                    } else {
                        continue;
                    }
                }
                $dataEntities[$field->Entity->entityHash]->$fieldName = $value;
                $save = true;
            }
        }

        if($save){
            //die('fin del test');
            $mainRecordId = null;
            if (!$dataEntities[$this->mainEntityHash]->save()) {
              foreach ($dataEntity->getMessages() as $message) {
                $msg.= $message.'\n';
              }
            } else {
                $mainRecordId = $dataEntities[$this->mainEntityHash]->id;
                foreach ($dataEntities as $entityHash => $entity) {
                    if ($entityHash != $this->mainEntityHash) {
                        if (!$dataEntities[$entityHash]->save()) {
                          foreach ($dataEntities[$entityHash]->getMessages() as $message) {
                            $msg.= $message.'\n';
                          }
                        } else {
                            $parameters["columns"] = ["id"];
                            $parameters["conditions"] = 'entityId = '.$this->entities[$this->mainEntityHash]->id.' AND entityIdReference = '.$this->entities[$entityHash]->id;
                            $parameters["limit"] = 1;
                            $field = Field::findFirst($parameters);

                            $this->entities[$this->mainEntityHash];
                            $relation = new MultipleEntityRelated();
                            $relation->dataEntityIdParent = $mainRecordId;
                            $relation->dataEntityIdChild = $dataEntities[$entityHash]->id;
                            $relation->fieldId = $field->id;

                            var_dump(json_encode($relation));
                            $relation->save();
                            if (!$relation->save()) {
                              foreach ($relation->getMessages() as $message) {
                                $msg.= $message.'\n';
                              }
                            }
                        }
                    }
                }
            }

            /* else {
                foreach ($dataEntities as $entityHash => $relation) {
                    if ($entityHash != $this->mainEntityHash) {
                        $msg = 'Entidad guardada';
                        $parentId = $dataEntities[$this->mainEntityHash]->id;
                        $currentValues = [];
                        $data = MultipleEntityRelated::findByParentId($parentId, $relation['fieldId']);
                        foreach ($data as $record) {
                            if (!in_array($record->getDataEntityIdChild(), $relation['values'])){
                                $record->delete();
                            } else {
                                $currentValues[] = $record->getDataEntityIdChild();
                            }
                        }
                        foreach ($relation['values'] as $relationToSave) {
                            if (!in_array($relationToSave, $currentValues)){
                                $record = new MultipleEntityRelated();
                                $record->setFieldId($relation['fieldId']);
                                $record->setDataEntityIdParent($parentId);
                                $record->setDataEntityIdChild($relationToSave);
                                if (!$record->save()) {
                                    $msg = 'Error al guardar relaciones';
                                }
                            }
                        }
                    }
                }
            }*/
        }
        echo $msg.PHP_EOL;

    }

}
