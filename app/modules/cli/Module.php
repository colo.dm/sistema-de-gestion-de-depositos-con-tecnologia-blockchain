<?php
namespace TFG\Modules\Cli;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{
    /**
     * Registers an autoloader related to the module
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new Loader();

        $loader->registerNamespaces([
            'TFG\Modules\Cli\Tasks' => __DIR__ . '/tasks/',
            'TFG\Modules\Service\Models' => APP_PATH . '/modules/service/models/',
            'TFG\Modules\Entity\Models' => APP_PATH . '/modules/entity/models/',
            'TFG\Modules\Layout\Models' => APP_PATH . '/modules/layout/models/',
            'TFG\Modules\Data\Models' => APP_PATH . '/modules/data/models/',
        ]);

        $loader->register();
    }

    /**
     * Registers services related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
    }
}
