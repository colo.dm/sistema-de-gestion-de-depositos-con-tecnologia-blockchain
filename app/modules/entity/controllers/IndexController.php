<?php

namespace TFG\Modules\Entity\Controllers;

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use TFG\Modules\Entity\Models\Entity as Entity;
use TFG\Modules\Entity\Models\EntityType as EntityType;
use TFG\Modules\Entity\Models\Field as Field;
use TFG\Modules\Entity\Models\FieldType as FieldType;

class IndexController extends ControllerBase
{
    function setHeader() {
        $this->response->setHeader('Access-Control-Allow-Origin', '*');
        $this->response->setContentType('application/json', 'UTF-8');
    }

    function sendEntityStatus200($message = 'Ok', $layoutUrl) {
        $this->setHeader();
        $this->response->setStatusCode(200, $message);
        $this->response->setJsonContent([
            'msg' => $message,
            'layoutUrl' => $layoutUrl
        ]);
        return $this->response->send();
    }

    function sendEntityStatus404($message = 'Not found', $details = null) {
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setJsonContent(['msg' => "$message: $details"]);
        $this->response->setStatusCode(404, $message);
        return $this->response->send();
    }

    function sendEntityStatus501($entity = null, $message = 'Internal error') {
        $this->response->setContentType('application/json', 'UTF-8');
        if (isset($entity)) {
            foreach ($entity->getMessages() as $errorMessage) {
                $msg .= $errorMessage.'. ';
            }
        }
        $this->response->setJsonContent(['msg' => $message.(isset($msg) ? ": $msg" : '')]);
        $this->response->setStatusCode(501, $message);
        return $this->response->send();
    }

    public function rsAction($resourceName) {
        $rs = $this->getResource($resourceName);
        if ($rs) {
            $this->response->setJsonContent($rs);
            return $this->response->send();
        } else {
            return $this->sendEntityStatus404();
        }
    }

    /**
     * Index action
     */
    public function editAction($filters)
    {
        $data = json_decode($filters);

        $entity = Entity::findFirst(array(
            "entityHash = '$data->entityHash'"
        ));

        if (!$entity) {
            return $this->sendEntityStatus404("Entity not found",$filters);
        }

        $data = new \stdClass();
        $data->specHash = 'mainEntity';
        $data->entities = array();
        $dataEntity = new \stdClass();
        $dataEntity->id = $entity->id;
        $dataEntity->name = $entity->name;
        $dataEntity->pruralName = $entity->pruralName;
        $dataEntity->entityRelatedChild = [];
        foreach ($entity->getMultipleEntityChild() as $child) {
            $dataEntity->entityRelatedChild[] = (int)$child->entityIdReference;
        }
        $dataEntity->format = $entity->getFormat();
        $data->entities[] = $dataEntity;
        $this->response->setJsonContent($data);
        return $this->response->send();
    }

    private function searchAsOption($entity, $conditions){
        $parameters["columns"] = ["id", "name"];
        $parameters["conditions"] = $conditions;
        $parameters["order"] = "upper(name)";
        $parameters["limit"] = 20;
        $entities = $entity::find($parameters);
        return $entities;
    }

    public function relationListAction($filter=0)
    {
        $data = [];

        if (is_numeric($filter)) {
            $filter = (int)$filter;
            if ($filter > 0) {
                $conditions = "entityTypeId = 2 AND id = " . $filter;
                $entities = $this->searchAsOption(new Entity(), $conditions);
                $data = $this->mergeAsOption($data, $entities);
            } else {
                $filter *= -1;
            }

            $conditions = "entityTypeId = 2 AND id != " . $filter;
            $entities = $this->searchAsOption(new Entity(), $conditions);
            $data = $this->mergeAsOption($data, $entities);

        } else {
            $conditions = "entityTypeId = 2 AND name LIKE '%" . $filter . "%'";
            $entities = $this->searchAsOption(new Entity(), $conditions);
            $data = $this->mergeAsOption($data, $entities);
        }

        $this->response->setJsonContent($data);
        return $this->response->send();
    }

    public function fieldTypeAction($filter)
    {
        $data = [];

        if (is_numeric($filter)) {
            $filter = (int)$filter;
            $conditions = "id = " . $filter;
            $entities = $this->searchAsOption(new FieldType(), $conditions);
            $data = $this->mergeAsOption($data, $entities);
            $conditions = "entityTypeId >= 2 AND id = " . $filter;
            $entities = $this->searchAsOption(new Entity(), $conditions);
            $data = $this->mergeAsOption($data, $entities);

            $conditions = "id != " . $filter." and id not in (1,16,17,18,19)";
            $entities = $this->searchAsOption(new FieldType(), $conditions);
            $data = $this->mergeAsOption($data, $entities);
            $conditions = "entityTypeId = 2 AND id != " . $filter;
            $entities = $this->searchAsOption(new Entity(), $conditions);
            $data = $this->mergeAsOption($data, $entities);

        } else {
            $conditions = "name LIKE '%" . $filter . "%' and id not in (1,16,17,18,19)";
            $entities = $this->searchAsOption(new FieldType(), $conditions);
            $data = $this->mergeAsOption($data, $entities);
            $conditions = "entityTypeId >= 2 AND name LIKE '%" . $filter . "%'";
            $entities = $this->searchAsOption(new Entity(), $conditions);
            $data = $this->mergeAsOption($data, $entities);
        }

        $this->response->setJsonContent($data);
        return $this->response->send();
    }

    /**
     * Searches for entity
     */
    public function searchAction($type = 0, $page = 0)
    {
        $limit = 100;
        $numberPage = 0;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Entity\Modules\Entity\Models\Entity', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $page; //$this->request->getQuery("page", "int");
        }
        $numberPage = $numberPage < 1 ? 0 : $numberPage-1;

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        if (((int)$type) > 0) {
            $parameters[] = "entityTypeId = ".((int) $type);
        }
        $parameters["columns"] = ["id", "name", "entityHash"];
        $parameters["order"] = "upper(name)";
        $parameters["limit"] = $limit;
        $parameters["offset"] = $numberPage*$limit;

        $entities = Entity::find($parameters);
        if (count($entities) == 0) {
            return $this->sendEntityStatus404("Empty search","The search did not find any entity");
        }

        $data = new \stdClass();
        $data->specHash = 'listEntities';
        $data->entities = $entities;
        $this->response->setJsonContent($data);
        return $this->response->send();
    }

    public function typeAction() {
        $data = [];

        $conditions = "id > 1";
        $entities = $this->searchAsOption(new EntityType(), $conditions);
        $data = $this->mergeAsOption($data, $entities);

        $this->response->setJsonContent($data);
        return $this->response->send();
    }

    /**
     * Creates a new entity
     */
    public function createAction()
    {
        if ($this->request->isPost()) {
            $data = $this->request->getJsonRawBody();//$this->request->getPost();
        } else {
            return $this->sendEntityStatus501($entity, 'Entity creation error');
        }

        $entity = new Entity();
        $entity->setName($data->entities[0]->name);
        $entity->setOwnerId(1); //$this->request->getPost("ownerId");
        $entity->setEntityTypeId($data->entities[0]->entityType); //$this->request->getPost("entityTypeId");
        //$entity->setEntityHash(md5($data->name.uniqid('', true)));

        if (!$entity->save()) {
            return $this->sendEntityStatus501($entity, 'Entity creation error');
        }

        createDataEntity($entity->id, '');
        return $this->sendEntityStatus200('Entity created: '.$data->entities[0]->name,'layout/editEntity/'.$entity->entityHash);
    }

    public function saveAction()
    {
        if ($this->request->isPost()) {
            $data = $this->request->getJsonRawBody();//$this->request->getPost();
        } else {
            return $this->sendEntityStatus501($entity, 'Request error');
        }

        foreach ($data->entities as $requestEntity) {
            $entity = Entity::findFirstByid((int)$requestEntity->id);

            if (!$entity) {
                return $this->sendEntityStatus404("Entity not found",$requestEntity->id);
            }

            $unsavedEntity = $this->getEntityFromRequest($requestEntity);
            if (!empty($unsavedEntity->name)) {
                $entity->setName($unsavedEntity->name);
            }

            if ($unsavedEntity->getFormat() !== null){
                $entity->setFormat($unsavedEntity->getFormat());
            }

            if ($unsavedEntity->getPruralName() !== null){
                $entity->setPruralName($unsavedEntity->getPruralName());
            }

            $oldRelations = [];
            foreach ($entity->getMultipleEntityChild() as $child) {
                if (!in_array($child->entityIdReference, $requestEntity->entityRelatedChild)){
                    $child->delete();
                } else {
                    $oldRelations[] = $child->entityIdReference;
                }
            }
            foreach ($requestEntity->entityRelatedChild as $childId) {
                if (!in_array($childId, $oldRelations)) {
                    $newRelation = new Field();
                    $newRelation->entityId = $entity->id;
                    $newRelation->setFieldTypeId(17);
                    $newRelation->setIsMultiple(true);
                    $newRelation->setRequired(false);
                    $newRelation->setName('multipleRelation');
                    $newRelation->setEntityIdReference($childId);

                    if (!$newRelation->save()){
                        return $this->sendEntityStatus501($entity, 'Entity multiple relation save error');
                    }
                }
            }


            if (!$entity->save()) {
                return $this->sendEntityStatus501($entity, 'Entity save error');
            }

            $this->updateEntityClass($entity, $entity->format);
        }

        return $this->sendEntityStatus200('Entity saved', null);
    }

    /**
     * Deletes a entity
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        if ($this->request->isPost()) {
            $data = $this->request->getJsonRawBody();//$this->request->getPost();
        } else {
            $data = json_decode($id);
        }

        $entity = Entity::findFirst(array(
            "entityHash = '$data->entityHash'"
        ));

        if (!$entity) {
            return $this->sendEntityStatus404("Entity not found",$data->specHash);
        }

        if (!$entity->delete()) {
            return $this->sendEntityStatus501($entity, 'Entity deletion error');
        }

        return $this->sendEntityStatus200('Entity deleted', null);
    }

    /**
     * Saves a entity edited
     *
     */
    public function saveFieldsAction($id=null)
    {
        if ($this->request->isPost()) {
            $data = $this->request->getJsonRawBody();//$this->request->getPost();
        } else {
            $data = json_decode($id);
        }

        $entity = Entity::findFirst(array(
            "entityHash = '$data->specHash'"
        ));

        if (!$entity) {
            return $this->sendEntityStatus404("Entity not found",$data->specHash);
        }

        foreach ($data->entities as $field) {
            $fieldId = (int)$field->id;
            if ($field->id === null) {
                $requestField = $this->getFieldFromRequest($field);
                $requestField->setEntityId($entity->getId());
                if (!$requestField->save()) {
                    return $this->sendEntityStatus501($field, 'Field creation error');
                }
            } elseif ($fieldId < 0) {
                $fieldToDelete = Field::findFirstByid(abs($fieldId));
                if ($fieldToDelete->delete() === false) {
                    return $this->sendEntityStatus501($fieldToDelete, 'Field delete error');
                }
            } else {
                $requestField = $this->getFieldFromRequest($field);
                $unsavedField = Field::findFirstByid($fieldId);
                if (!empty($requestField->getName())) {
                    $unsavedField->setName($requestField->getName());
                }
                if ($requestField->getDescription() != null) {
                    $unsavedField->setDescription($requestField->getDescription());
                }
                if ($requestField->getRequired() !== null) {
                    $unsavedField->setRequired($requestField->getRequired());
                }
                if (!$unsavedField->save()) {
                    return $this->sendEntityStatus501($field, 'Field update error');}
            }
        }
        $this->updateEntityClass($entity, $entity->format);
        return $this->sendEntityStatus200('Fields saved', null);
    }

    public function fieldsAction($filters)
    {
        $data = json_decode($filters);

        $entity = Entity::findFirst(array(
            "entityHash = '$data->entityHash'"
        ));

        if (!$entity) {
            return $this->sendEntityStatus404("Entity not found",$filters);
        }

        $data = new \stdClass();
        $data->specHash = $entity->entityHash;
        $data->entities = array();
        foreach ($entity->Field as $field) {
            if ($field->getFieldTypeId() != 17) {
                $f = new \stdClass();
                $f->id = $field->id;
                $f->name = $field->name;
                if ($field->fieldTypeId == 1 || $field->fieldTypeId == 18){
                    $f->type = (int)$field->entityIdReference;
                    $f->multiple = $field->getIsMultiple();
                } else {
                    $f->type = (int)$field->fieldTypeId;
                }
                $f->desc = $field->description;
                $f->req = $field->getRequired();
                $data->entities[] = $f;
            }
        }

        $this->response->setJsonContent($data);
        return $this->response->send();
        //return $this->sendEntityStatus200($field, 'Fields saved');
    }

    public function fieldsListAction($filters)
    {
        $dataFilter = json_decode($filters);

        $entity = Entity::findFirst(array(
            "entityHash = '$dataFilter->entityHash'"
        ));

        if (!$entity) {
            return $this->sendEntityStatus404("Entity not found",$filters);
        }

        $data = new \stdClass();
        $data->specHash = $entity->entityHash;
        $data->list = array();
        foreach ($entity->Field as $field) {
            $entity = new \stdClass();
            $entity->value = $field->id;
            $entity->label = $field->name;
            $data->list[] = $entity;
        }

        if (isset($dataFilter->forSelector)) {
            $data = $data->list;
        }

        $this->response->setJsonContent($data);
        return $this->response->send();
        //return $this->sendEntityStatus200($field, 'Fields saved');
    }



}
