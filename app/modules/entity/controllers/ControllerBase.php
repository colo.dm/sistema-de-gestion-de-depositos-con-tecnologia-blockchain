<?php
namespace TFG\Modules\Entity\Controllers;

use Phalcon\Mvc\Controller;
use TFG\Modules\Entity\Models\Entity as Entity;
use TFG\Modules\Entity\Models\Field as Field;

class ControllerBase extends Controller
{
    protected function getResource($resourceName) {
        $filePath = __DIR__ . '/../resources/js/'.$resourceName.'.json';
        if (file_exists($filePath)) {
            return json_decode(file_get_contents($filePath));
        } else {
            return false;
        }
    }

    protected function mergeAsOption($options, $addOptions) {
        foreach ($addOptions as $optionItem) {
            $option = new \stdClass();
            $option->value = (int)$optionItem->id;
            $option->label = $optionItem->name;
            $options[] = $option;
        }
        return $options;
    }

    protected function getEntityFromRequest($data){
        $entity = new Entity();
        $entity->setName(isset($data->name) ? trim((string)$data->name) : null);
        $entity->setPruralName(isset($data->pruralName) ? $data->pruralName : null);
        $entity->setFormat(isset($data->format) ? $data->format : null);
        return $entity;
    }

    protected function getFieldFromRequest($data){
        $field = new Field();
        $field->setName($data->name);
        $field->setFieldTypeId($data->type);
        $field->setRequired($data->req);
        $field->setDescription($data->desc ?? '');
        $field->setIsMultiple($data->multiple ?? false);
        return $field;
    }

    protected function updateEntityClass(Entity $entity, $oldFormat) {
        $format = $oldFormat;
            $fixedFormat = $format;
            $regex = '/\{[a-zA-Z0-9]+\}/m';
            preg_match_all($regex, $format, $matches, PREG_SET_ORDER, 0);
            foreach ($matches as $expr) {
                $fieldId = str_replace(['{','}'],['', ''], $expr[0]);
                $field = Field::findFirstByid($fieldId);
                if ($field) {
                    $getValueText = '$this->'.$field->getColumnName();
                    if ($field->fieldTypeId == 1) {
                        $dataEntity = 'DataEntity'.$field->entityIdReference;
                        $getValueText = '".$this->get'.$field->getColumnName().'()."';
                    }
                    $format = str_ireplace('{'.$fieldId.'}', $getValueText, $format);
                } else {
                    $format = str_ireplace('{'.$fieldId.'}', '', $format);
                    $fixedFormat = str_ireplace('{'.$fieldId.'}', '', $fixedFormat);
                }
            }
            $getters = '';
            foreach ($entity->Field as $field) {
                if ($field->fieldTypeId == 1) {
                    $dataEntity = 'DataEntity'.$field->entityIdReference;
                    $getters.= "\n    public function get".$field->getColumnName().'(){
        $field = '.$dataEntity.'::findFirstById($this->'.$field->getColumnName().');
        return $field ? $field->getLabel() : null;
    }';
                    $getValueText = '".$this->get'.$field->getColumnName().'()."';
                }
            }
            if ($fixedFormat != $oldFormat) {
                $entity->setFormat($fixedFormat);
                $entity->save();
            }
            createDataEntity($entity->id, $format, $getters);
    }
}
