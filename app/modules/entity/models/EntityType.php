<?php

namespace TFG\Modules\Entity\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class EntityType extends \Entity\Models\TableExtended
{
    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasMany('id', 'Entity', 'entityTypeId', ['alias' => 'Entity']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'entityType';
    }

}
