<?php

namespace TFG\Modules\Entity\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class DataEntity extends \Entity\Models\TableId
{
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    private $entityId;

    /**
     * Method to set the value of field entityId
     *
     * @param integer $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Returns the value of field entityId
     *
     * @return integer
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent->initialize();
        $this->belongsTo('entityId', '\Entity', 'id', ['alias' => 'Entity']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'dataEntity';
    }

}
