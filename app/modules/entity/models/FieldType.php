<?php

namespace TFG\Modules\Entity\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use Phalcon\Db\Column as Column;

class FieldType extends \Entity\Models\TableExtended
{

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasMany('id', 'Entity\Modules\Entity\Models\Field', 'fieldTypeId', ['alias' => 'Field']);
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'fieldType';
    }

    public function getDbFieldType() {
        switch ($this->id) {
            case 1:
            case 2:
            case 3:
            case 16:
                return Column::TYPE_INTEGER;
                break;
            case 4:
                return Column::TYPE_DECIMAL;
                break;
            case 5:
            case 9:
            case 12:
            case 13:
            case 14:
            case 15:
                return Column::TYPE_VARCHAR;
                break;
            case 6:
            case 7:
            case 11:
                return Column::TYPE_TEXT;
                break;
            case 8:
                return Column::TYPE_DATE;
                break;
            case 10:
                return Column::TYPE_DATETIME;
                break;
        }
    }

    public function getDbFieldSize() {
        switch ($this->id) {
            case 1:
            case 3:
            case 11:
            case 16:
                return 10;
                break;
            case 2:
                return 4;
                break;
            case 4:
                return 12;
                break;
            case 5:
            case 12:
            case 13:
            case 14:
            case 15:
                return 255;
                break;
            case 9:
                return 5;
                break;
            default:
                return 0;
        }
    }

    public function getDbFieldScale() {
        switch ($this->id) {
            case 4:
                return 3;
                break;
            default:
                return 0;
        }
    }

    public function getDbFieldIsUnsigned() {
        switch ($this->id) {
            case 1:
            case 11:
            case 16:
                return true;
                break;
            default:
                return false;
        }
    }

}
