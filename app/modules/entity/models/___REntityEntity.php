<?php

namespace Entity\Modules\Entity\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use Entity\Modules\Entity\Models\Entity as Entity;

class REntityEntity extends \Entity\Models\TableId
{
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $entityIdParent;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $entityIdChild;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->belongsTo('entityIdParent', 'Entity\Modules\Entity\Models\Entity', 'id', ['alias' => 'EntityParent']);
        $this->belongsTo('entityIdChild', 'Entity\Modules\Entity\Models\Entity', 'id', ['alias' => 'EntityChild']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rEntityEntity';
    }

}
