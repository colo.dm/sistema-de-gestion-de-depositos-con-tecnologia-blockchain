<?php

namespace TFG\Modules\Entity\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use Phalcon\Db\Column as Column;
use Phalcon\Db\Reference as Reference;
use Phalcon\Db\Index as Index;
use TFG\Modules\Data\Models\MultipleEntityRelated as MultipleEntityRelated;

class Field extends \Entity\Models\TableExtended
{
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $entityId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $fieldTypeId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $size;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $scale;

    /**
     *
     * @var boolean
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $required;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $isUnsigned;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=true)
     */
    protected $isMultiple;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=true)
     */
    protected $entityIdReference;

    /**
     * Method to set the value of field entityId
     *
     * @param integer $entityId
     * @return $this
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Method to set the value of field fieldTypeId
     *
     * @param integer $fieldTypeId
     * @return $this
     */
    public function setFieldTypeId($fieldTypeId)
    {
        $this->fieldTypeId = $fieldTypeId;

        return $this;
    }


    /**
     * Method to set the value of field size
     *
     * @param integer $size
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Method to set the value of field size
     *
     * @param integer $size
     * @return $this
     */
    public function setScale($scale)
    {
        $this->scale = $scale;

        return $this;
    }

    /**
     * Method to set the value of field size
     *
     * @param integer $size
     * @return $this
     */
    public function setRequired($required)
    {
        $this->required = (int) ((bool) $required);
        return $this;
    }

    /**
     * Method to set the value of field size
     *
     * @param integer $size
     * @return $this
     */
    public function setIsMultiple($isMultiple)
    {
        $this->isMultiple = (int) ((bool) $isMultiple);
        return $this;
    }


    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Method to set the value of field size
     *
     * @param integer $size
     * @return $this
     */
    public function setIsUnsigned($isUnsigned)
    {
        $this->isUnsigned = (int) ((bool) $isUnsigned);
        return $this;
    }

    /**
     * Method to set the value of field entityIdReference
     *
     * @param integer $entityId
     * @return $this
     */
    public function setEntityIdReference($entityIdReference)
    {
        $this->entityIdReference = $entityIdReference;

        return $this;
    }

    /**
     * Returns the value of field entityId
     *
     * @return integer
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Returns the value of field fieldTypeId
     *
     * @return integer
     */
    public function getFieldTypeId()
    {
        return (int)$this->fieldTypeId;
    }


    /**
     * Returns the value of field size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Returns the value of field size
     *
     * @return integer
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * Returns the value of field required
     *
     * @return bool
     */
    public function getRequired()
    {
        return $this->required == 1 ? true : false;
    }

    /**
     * Returns the value of field required
     *
     * @return bool
     */
    public function getIsMultiple()
    {
        return $this->isMultiple == 1 ? true : false;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Returns the value of field required
     *
     * @return bool
     */
    public function getIsUnsigned()
    {
        return $this->isUnsigned == 1 ? true : false;
    }

    /**
     * Returns the value of field entityIdReference
     *
     * @return integer
     */
    public function getEntityIdReference()
    {
        return $this->entityIdReference;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasMany('id', 'RFieldAccessGroup', 'fieldId', ['alias' => 'RFieldAccessGroup']);
        $this->belongsTo('entityId', 'Entity\Modules\Entity\Models\Entity', 'id', ['alias' => 'Entity']);
        $this->belongsTo('fieldTypeId', 'Entity\Modules\Entity\Models\FieldType', 'id', ['alias' => 'FieldType']);
        $this->belongsTo('entityIdReference', 'Entity\Modules\Entity\Models\Entity', 'id', ['alias' => 'EntityReference']);
        $this->hasMany('id', 'Entity\Modules\Data\Models\RDataSelectorField', 'fieldId', ['alias' => 'RDataSelectorField']);
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'field';
    }

    public function getTableName()
    {
        if (isset($this->entityId)){
            return 'dataEntity'.$this->entityId;
        } else {
            return null;
        }
    }

    public function getColumnName() {
        switch ($this->fieldTypeId) {
            case 1:
                return 'entity'.$this->id;
                break;
            case 16:
                return 'user'.$this->id;
                break;
            case 11:
                return 'expr'.$this->id;
                break;
            case 6:
            case 7:
                return 'text'.$this->id;
            default:
                return 'field'.$this->id;
                break;
        }
    }

    protected function beforeCreate()
    {
        parent::beforeCreate();
        if ($this->fieldTypeId > 20) { //>20 then is an Entity
            $this->entityIdReference = $this->fieldTypeId;
            $this->fieldTypeId = $this->isMultiple ? 18 : 1;
        }
        $this->setIsUnsigned($this->FieldType->getDbFieldIsUnsigned());
        $this->setSize($this->FieldType->getDbFieldSize());
        $this->setScale($this->FieldType->getDbFieldScale());
    }

    protected function afterCreate()
    {
        $params = [
            'type'      => $this->FieldType->getDbFieldType(),
            'notNull'   => $this->getRequired()
        ];
        if ($this->getSize() > 0){
            $params['size'] = $this->getSize();
        }
        if ($this->getScale() > 0){
            $params['scale'] = $this->getScale();
        }
        if ($this->getIsUnsigned() > 0){
            $params['unsigned'] = $this->getIsUnsigned();
        }

        $connection = $this->getDI()->get('db');
        if (!$this->isMultiple) {
            $connection->addColumn(
                $this->getTableName(),
                null,
                new Column(
                    $this->getColumnName(),
                    $params
                )
            );

            if ($this->entityIdReference > 0){
                $connection->addForeignKey(
                    $this->getTableName(),
                    null, /*self::getDbName(), */
                    new Reference('FK_'.$this->entityId.'_'.$this->id.'__'.$this->entityIdReference, array(
                        'referencedTable' => 'dataEntity'.$this->entityIdReference,
                        'referencedColumns' => array('id'),
                        'columns' => array($this->getColumnName()),
                        'onUpdate' => 'RESTRICT',
                        'onDelete' => 'RESTRICT')
                    )
                );
            } else {
                $newColumnIndex = new Index(
                    'IDX_F'.$this->id,
                    [
                        $this->getColumnName()
                    ]
                );
                $connection->addIndex($this->getTableName(), null, $newColumnIndex);
            }
        }
    }

    protected function beforeUpdate()
    {
        parent::beforeUpdate();
        $params = [
            'type'      => $this->FieldType->getDbFieldType(),
            'notNull'   => $this->getRequired()
        ];
        if ($this->getSize() > 0){
            $params['size'] = $this->getSize();
        }
        if ($this->getScale() > 0){
            $params['scale'] = $this->getScale();
        }
        if ($this->getIsUnsigned() > 0){
            $params['unsigned'] = $this->getIsUnsigned();
        }

        if (!$this->getIsMultiple()) {
            $connection = $this->getDI()->get('db');
            $connection->modifyColumn(
                $this->getTableName(),
                null,
                new Column(
                    $this->getColumnName(),
                    $params
                )
            );
        }

        /*$connection->modifyColumn(
            $this->getTableName(),
            null,
            new Column(
                'name',
                [
                    'type'    => Column::TYPE_VARCHAR,
                    'size'    => 40,
                    'notNull' => true,
                ]
            )
        );*/
    }

    protected function beforeDelete()
    {
        // Deleting the column 'name'
        $connection = $this->getDI()->get('db');
        foreach ($this->RDataSelectorField as $relation) {
            $relation->delete();
        }

        if (!$this->isMultiple) {
            if ($this->entityIdReference > 0){
                $connection->dropForeignKey(
                    $this->getTableName(),
                    null,
                    'FK_'.$this->entityId.'_'.$this->id.'__'.$this->entityIdReference
                );
            }
            $connection->dropColumn(
                $this->getTableName(),
                null,
                $this->getColumnName()
            );
        } else {
            $dataRelated = MultipleEntityRelated::findByFieldId($this->id);
            foreach ($dataRelated as $record) {
                $record->delete();
            }
        }
    }
}
