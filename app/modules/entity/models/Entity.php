<?php

namespace TFG\Modules\Entity\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use Phalcon\Db\Column as Column;
use Phalcon\Db\Index as Index;
use Phalcon\Db\Reference as Reference;
use TFG\Modules\Layout\Models\Layout as Layout;
use TFG\Modules\Layout\Models\LayoutRow as LayoutRow;
use TFG\Modules\Layout\Models\LayoutColumn as LayoutColumn;
use TFG\Modules\Layout\Models\Spec as Spec;
use TFG\Modules\Data\Models\DataSelector as DataSelector;
use TFG\Modules\Service\Models\Service as Service;
use TFG\Modules\Entity\Models\Field as Field;
use TFG\Models\DataEntity as DataEntity;

class Entity extends \Entity\Models\TableExtended
{
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $ownerId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $entityTypeId;

    /**
     *
     * @var string
     * @Column(type="string", length=45, nullable=false)
     */
    protected $entityHash;

    /**
     *
     * @var string
     * @Column(type="string", length=120, nullable=true)
     */
    protected $format;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $layoutId;

    /**
     *
     * @var string
     * @Column(type="string", length=190, nullable=true)
     */
    protected $pruralName;

    /**
     * Method to set the value of field ownerId
     *
     * @param integer $ownerId
     * @return $this
     */
    public function setOwnerId($ownerId)
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    /**
     * Method to set the value of field entityTypeId
     *
     * @param integer $entityTypeId
     * @return $this
     */
    public function setEntityTypeId($entityTypeId)
    {
        $this->entityTypeId = $entityTypeId;

        return $this;
    }

    /**
     * Method to set the value of field entityTypeId
     *
     * @param string $entityHash
     * @return $this
     */
    private function setEntityHash($entityHash)
    {
        $this->entityHash = $entityHash;

        return $this;
    }

    /**
     * Method to set the value of field entityTypeId
     *
     * @param string $format
     * @return $this
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }


    /**
     * Returns the value of field ownerId
     *
     * @return integer
     */
    public function getOwnerId()
    {
        return $this->ownerId;
    }

    /**
     * Returns the value of field entityTypeId
     *
     * @return integer
     */
    public function getEntityTypeId()
    {
        return $this->entityTypeId;
    }

    /**
     * Method to set the value of field entityTypeId
     *
     * @return string
     */
    public function getEntityHash()
    {
        return $this->entityHash;
    }

    /**
     * Method to set the value of field entityTypeId
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setPruralName($pruralName)
    {
        $this->pruralName = $pruralName;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getPruralName()
    {
        return $this->pruralName;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        parent::initialize();
        $this->setSchema("entity");
        $this->hasMany('id', 'Entity\Models\DataEntity', 'entityId', ['alias' => 'DataEntity']);
        $this->hasMany('id', 'Entity\Modules\Entity\Models\Field', 'entityId', ['alias' => 'Field']);
        $this->hasMany('id', 'Entity\Modules\Entity\Models\Field', 'entityIdReference', ['alias' => 'FieldEntity']);
        $this->hasMany('id', 'Entity\Modules\Data\Models\DataSelector', 'entityId', ['alias' => 'DataSelector']);
        $this->hasMany('id', 'Entity\Modules\Access\Models\REntityAccessGroup', 'entityId', ['alias' => 'REntityAccessGroup']);
        $this->hasMany('id', 'Entity\Modules\Service\Models\Service', 'entityId', ['alias' => 'Service']);
        $this->belongsTo('entityTypeId', 'Entity\Modules\Entity\Models\\EntityType', 'id', ['alias' => 'EntityType']);
        $this->belongsTo('ownerId', 'Entity\Modules\Access\Models\Owner', 'id', ['alias' => 'Owner']);
        $this->belongsTo('layoutId', 'Entity\Modules\Layout\Models\Layout', 'id', ['alias' => 'Layout']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Entity[]|Entity
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Entity
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'entity';
    }

    public function getTableName()
    {
        if (isset($this->id)){
            return 'dataEntity'.$this->id;
        } else {
            return null;
        }
    }

    public function getDataInstance() {
        $tableName = $this->getTableName();
        $tableName[0] = strtoupper($tableName[0]);
        $class = '\\Custom\\' . $tableName;
        return new $class();
    }

    public function getMultipleEntityChild() {
        return Field::find("fieldTypeId = 17 AND entityId =". $this->id);
    }

    protected function beforeValidationOnCreate() {
        $layout = new Layout();
        $layout->name = $this->entityHash.'-default';
        $layout->layoutTypeId = 1;
        $layout->save();

        $this->layoutId = $layout->id;
    }

    protected function afterCreate() {
        // Set the creation date
        $connection = $this->getDI()->get('db');
        $connection->createTable(
            $this->getTableName(),
            null,
            [
               'columns' => [
                    new Column(
                        'id',
                        [
                            'type'          => Column::TYPE_INTEGER,
                            'size'          => 10,
                            'notNull'       => true,
                            'unsigned'      => true,
                            'autoIncrement' => false,
                            'primary'       => true,
                        ]
                    ),
                    new Column(
                        'created',
                        [
                            'type'    => Column::TYPE_TIMESTAMP,
                            'notNull' => true
                        ]
                    ),
                    new Column(
                        'lastUpdate',
                        [
                            'type'    => Column::TYPE_TIMESTAMP,
                            'notNull' => true
                        ]
                    )
                ]
            ]
        );

        new Reference('FK_DataID_PK_'.$this->id, array(
            'referencedTable' => 'dataEntity',
            'referencedColumns' => array('id'),
            'columns' => array('id'),
            'onUpdate' => 'RESTRICT',
            'onDelete' => 'RESTRICT')
        );

        $indexCreated = new Index(
            'created',
            [
                'created'
            ]
        );

        $indexlastUpdate = new Index(
            'lastUpdate',
            [
                'lastUpdate'
            ]
        );

        // Add index to existing table
        $connection->addIndex($this->getTableName(), null, $indexCreated);
        $connection->addIndex($this->getTableName(), null, $indexlastUpdate);

        $this->Layout->name = $this->entityHash.'-default';
        $this->Layout->save();

        $row = new LayoutRow();
        $row->Layout = $this->Layout;
        $row->save();
        $column = new LayoutColumn();
        $column->LayoutRow = $row;
        $column->xs = 24;
        $column->sm = 24;
        $column->md = 24;
        $column->lg = 24;
        $column->save();
        $column->addSpec($this, 'default');
    }

    protected function beforeCreate() {
        parent::beforeCreate();
        $this->setEntityHash(md5(uniqid('', true)));
    }

    protected function beforeDelete() {
        foreach ($this->Field as $child) {
            $child->delete();
        }

        foreach ($this->FieldEntity as $child) {
            $child->delete();
        }

        foreach ($this->Service as $child) {
            $child->delete();
        }

        foreach ($this->DataSelector as $child) {
            $child->delete();
        }

        $connection = $this->getDI()->get('db');
        $connection->execute("DELETE FROM dataEntity where entityId = ".$this->id);

        $connection->dropTable('dataEntity'.$this->id);
    }

    protected function afterDelete() {
        if ($this->Layout) {
            $this->Layout->delete();
        }
    }

}

//http://localhost/entity/entity/c/spec/create/{"name":"firstEntity"}
//http://localhost/entity/entity/c/spec/delete/{"specHash":"6e9af63fbdbbf24dad4903bb1dd60d62"}
/*
$index_unique = new \Phalcon\Db\Index(
    'column_UNIQUE',
    [
        'column',
        'column'
    ],
    'UNIQUE'
);

// Define new primary index
$index_primary = new \Phalcon\Db\Index(
    'PRIMARY',
    [
        'column'
    ]
);

// Add index to existing table
$connection->addIndex("robots", null, $index_unique);
$connection->addIndex("robots", null, $index_primary);
*/