<div class="page-header">
    <h1>Schema</h1>
</div>

<div id="schema">
	<h1 @click="addItem">{{"{{ message }}"}} </h1>
	<label @click="reverseMessage">Name</label>	<input name="name"></input>

	<ul >
	  <li v-for="(item, index) in items">
	    {{"{{ item.name }}{{ index }}"}}
	  </li>
	</ul>

	<my-components></my-components>
	<my-component></my-component>

	<p>{{"{{ total }}"}}</p>
    <button-counter v-on:increment="incrementTotal"></button-counter>
    <button-counter v-on:increment="incrementTotal"></button-counter>


</div>


<div id="app">
	<my-component></my-component>	
</div>