<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>

<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous"><?php echo $this->tag->linkTo(["entity", "Go Back"]) ?></li>
        </ul>
    </nav>
</div>



<div id="app">

    <!--entity-form url="../js/data4.json" ></entity-form -->
    <entity-form url="../../js/data4.json" ></entity-form>

    <spec-entity-form url="../../js/data_2.json" ></spec-entity-form>

<!--b-alert variant="danger" dismissible :show="true" @dismissed="showDismissibleAlert=false">
    Dismissible Alert!
</b-alert-->

</div>