<?php

namespace TFG\Modules\Data\Controllers;
use TFG\Modules\Data\Models\DataSelector as DataSelector;
use TFG\Modules\Layout\Models\Spec as Spec;
use TFG\Modules\Entity\Models\Entity as Entity;
use TFG\Modules\Entity\Models\Field as Field;
use TFG\Modules\Data\Models\MultipleEntityRelated as MultipleEntityRelated;
use TFG\Models\DataEntity as DataEntity;
use Phalcon\Mvc\Model\Query;

class IndexController extends ControllerBase {
    function setHeader() {
        $this->response->setHeader('Access-Control-Allow-Origin', '*');
        $this->response->setContentType('application/json', 'UTF-8');
    }

    function sendEntityStatus200($message = 'Ok', $layoutUrl=null) {
        $this->setHeader();
        $this->response->setStatusCode(200, $message);
        $this->response->setJsonContent([
            'msg' => $message,
            'layoutUrl' => $layoutUrl
        ]);
        return $this->response->send();
    }

    function sendEntityStatus404($message = 'Not found', $details = null) {
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setJsonContent(['msg' => $message.(isset($details) ? ": $details" : "")]);
        $this->response->setStatusCode(404, $message);
        return $this->response->send();
    }

    function sendEntityStatus501($entity = null, $message = 'Internal error') {
        $this->response->setContentType('application/json', 'UTF-8');
        if (isset($entity)) {
            foreach ($entity->getMessages() as $errorMessage) {
                $msg .= $errorMessage.'. ';
            }
        }
        $this->response->setJsonContent(['msg' => $message.(isset($msg) ? ": $msg" : '')]);
        $this->response->setStatusCode(501, $message);
        return $this->response->send();
    }

    public function indexAction()
    {

    }

    public function optionAction($fieldId, $dataEntityId = null) {
        $field = Field::findFirstById((int)$fieldId);
        $options = [];
        if($field && $field->getEntityIdReference() !== null) {
            $dataTable = $field->EntityReference->getDataInstance();
            $parameters = [];
            if ($dataEntityId === null) {
                //$parameters["conditions"] = $conditions;
            }

            //$parameters["columns"] = ["id", "name"];
            //$parameters["conditions"] = $conditions;
            //$parameters["order"] = "upper(name)";
            $parameters["limit"] = 20;
            foreach ($dataTable::find($parameters) as $entityOption) {
                $option = new \stdClass();
                $option->value = (int)$entityOption->id;
                $option->label = $entityOption->getLabel();
                $options[] = $option;
            }
        }
        $this->response->setJsonContent($options);
        return $this->response->send();
    }

    public function specAction($specId,$filters) {
        $spec = Spec::findFirstById($specId);
        $data = new \stdClass();
        $data->specHash = $spec->hash;
        $data->entities = [];
        $parameters = [];
        $filters = json_decode(rawurldecode($filters));
        $sendData = true;
        $mainDataEntityId = 0;
        if (isset($filters->dataEntityId)) {
            $mainDataEntityId = (int) $filters->dataEntityId;
            $filters->dataEntityId = $mainDataEntityId;
            if ($filters->dataEntityId > 0) {
                $parameters[] = 'id = '.($filters->dataEntityId);
            } elseif ($filters->dataEntityId < 0) {
                $sendData = false;
            }
        }
        if ($sendData) {
            $entityId = $mainDataEntityId > 0 ? DataEntity::findFirstById($mainDataEntityId)->entityId : $spec->dataSelector->Entity->getId();
            $dataTable = $spec->dataSelector->Entity->getDataInstance();
            $addLabel = true;
            if ($entityId != $spec->dataSelector->Entity->getId()){
                $addLabel = false;
                $field = Field::findFirst("entityId = ".$entityId. " AND fieldTypeId = 17 AND entityIdReference = ".$spec->dataSelector->Entity->getId());
                $childs = MultipleEntityRelated::findByParentId($mainDataEntityId, $field->getId());
                $dataValues = [];
                foreach ($childs as $child) {
                    $dataValues[] = $dataTable::findFirstById($child->dataEntityIdChild);
                }
                //error_log(json_encode($dataValues), 0);
            } else {
                $dataValues = $dataTable::find($parameters);
            }
            foreach ($dataValues as $dataValue) {
                $entity = new \stdClass();
                $entity->id = (int)$dataValue->id;
                foreach ($spec->dataSelector->rDataSelectorField as $rField) {
                    $fieldName = $rField->Field->getColumnName();
                    $fieldId = (string)$rField->Field->id;
                    if ($rField->Field->getIsMultiple()) {
                        $entity->$fieldId = [];
                        $dataRelated = MultipleEntityRelated::findByParentId($filters->dataEntityId, $fieldId);
                        foreach ($dataRelated as $record) {
                            $entity->$fieldId[] = $record->getDataEntityIdChild();
                        }
                    }
                    else {
                        if (empty($rField->Field->getEntityIdReference())) {
                            $entity->$fieldId = $dataValue->$fieldName;
                        } else {
                            $getValueFunction = 'get'.$fieldName;
                            $entity->$fieldId = $dataValue->$getValueFunction();
                            //$entity->$fieldId = $dataValue->$fieldName;

                        }
                    }
                }
                if ($addLabel) {
                    $entity->defaultLabel = $dataValue->getLabel();
                }
                $data->entities[] = $entity;
            }
        } else {
            $newEntity = new \stdClass();
            $newEntity->id = null;
            $data->entities = [$newEntity];
        }
        $this->response->setJsonContent($data);
        return $this->response->send();
    }

    public function saveAction() {
        if ($this->request->isPost()) {
            $data = rawurldecode($this->request->getRawBody());
            $data = json_decode($data);
            $spec = Spec::findFirst("hash = '$data->specHash'");
            $entity = $spec->DataSelector->Entity;
            $parentDataId = null;
            if (property_exists($data->filters, 'dataEntityId')){
                $parentDataId = (int)$data->filters->dataEntityId;
            }
            if ($entity) {
                foreach ($data->entities as $requestEntity) {
                    $dataTable = $entity->getDataInstance();
                    if ($requestEntity->id > 0) {
                        $dataTable = $dataTable::findFirstById($requestEntity->id);
                    } elseif ($requestEntity->id < 0) {
                        $dataTable = $dataTable::findFirstById($requestEntity->id*(-1));
                        if ($dataTable) {
                            $this->deleteRelation($parentDataId, $dataTable);
                        }
                    }
                    unset($requestEntity->id);
                    $msg = $this->saveDataEntity($dataTable, $requestEntity, $entity, $parentDataId);
                }
                return $this->sendEntityStatus200($msg);
            } else {
                return $this->sendEntityStatus404();
            }
        }
    }

    public function deleteAction() {
        if ($this->request->isPost()) {
            $data = rawurldecode($this->request->getRawBody());
            $data = json_decode($data);
            $spec = Spec::findFirst("hash = '$data->specHash'");
            $entity = $spec->DataSelector->Entity;
            if ($entity) {
                $data = $entity->getDataInstance()::findFirstById($data->id);
                if ($data) {
                    if (!$data->delete()){
                        $this->sendEntityStatus501();
                    } else {
                        return $this->sendEntityStatus200('Entity Deleted');
                    }
                } else {
                    return $this->sendEntityStatus404('Entity not found');
                }
            } else {
                return $this->sendEntityStatus404('Spec not fount');
            }
        }
    }
}

