<?php
namespace TFG\Modules\Data\Controllers;

use Phalcon\Mvc\Controller;
use TFG\Modules\Entity\Models\Field as Field;
use TFG\Modules\Data\Models\MultipleEntityRelated as MultipleEntityRelated;
use TFG\Models\DataEntity as DataEntity;

class ControllerBase extends Controller {

    protected function saveDataEntity($dataTable, $requestEntity, $mainEntity = null, $relatedDataEntityId = null){
        $relations = [];
        $save = false;
        $msg = 'Entidad sin cambios';
        foreach ($requestEntity as $id => $value) {
            if ($value != null){
                $field = Field::findFirstById((int)$id);
                if ($field->getIsMultiple()) {
                    $relations[] = [
                        'fieldId' => $field->getId(),
                        'values' => $value
                    ];
                } else {
                    $fieldName = $field->getColumnName();
                    $dataTable->$fieldName = $value;
                }
                $save = true;
            }
        }
        if($save){
            if (!$dataTable->save()) {
              foreach ($dataTable->getMessages() as $message) {
                $msg.= $message.'\n';
              }
            } else {
                if ($relatedDataEntityId > 0){
                    $entityId = DataEntity::findFirstById($relatedDataEntityId)->entityId;
                    $field = Field::findFirst("entityId = ".$entityId. " AND fieldTypeId = 17 AND entityIdReference = ".$mainEntity->getId());
                    $data = MultipleEntityRelated::findByParentId($relatedDataEntityId, $field->getId(), $dataTable->id);
                    if (count($data) == 0){
                        $record = new MultipleEntityRelated();
                        $record->setFieldId($field->getId());
                        $record->setDataEntityIdParent($relatedDataEntityId);
                        $record->setDataEntityIdChild($dataTable->id);
                        if (!$record->save()) {
                            $msg = 'Error al guardar relaciones';
                            /*foreach ($record->getMessages() as $message) {
                                $msg.= $message.'\n';
                            }
                            $msg.=json_encode($relation);*/
                        }
                    }
                }
                foreach ($relations as $relation) {
                    $msg = 'Entidad guardada';
                    $parentId = $dataTable->id;
                    $currentValues = [];
                    $data = MultipleEntityRelated::findByParentId($parentId, $relation['fieldId']);
                    foreach ($data as $record) {
                        if (!in_array($record->getDataEntityIdChild(), $relation['values'])){
                            $record->delete();
                        } else {
                            $currentValues[] = $record->getDataEntityIdChild();
                        }
                    }
                    foreach ($relation['values'] as $relationToSave) {
                        if (!in_array($relationToSave, $currentValues)){
                            $record = new MultipleEntityRelated();
                            $record->setFieldId($relation['fieldId']);
                            $record->setDataEntityIdParent($parentId);
                            $record->setDataEntityIdChild($relationToSave);
                            if (!$record->save()) {
                                $msg = 'Error al guardar relaciones';
                                /*foreach ($record->getMessages() as $message) {
                                    $msg.= $message.'\n';
                                }
                                $msg.=json_encode($relation);*/
                            }
                        }
                    }
                }
            }
        }
        return $msg;
    }

    protected function deleteRelation($parentDataId, $dataTable) {
        foreach (MultipleEntityRelated::findByParentId($parentDataId, null, $dataTable->id) as $data) {
            $data->delete();
        }
        $dataTable->delete();
    }
}
