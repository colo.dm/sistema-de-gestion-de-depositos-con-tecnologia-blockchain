<?php

namespace TFG\Modules\Data\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class Rrdataselectorfieldfilter extends \Entity\Models\Table
{
    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $rDataSelectorFieldDataSelectorId;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $rDataSelectorFieldFieldId;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $filterId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->belongsTo('rDataSelectorFieldDataSelectorId', '\RDataSelectorField', 'dataSelectorId', ['alias' => 'RDataSelectorField']);
        $this->belongsTo('filterId', '\Filter', 'id', ['alias' => 'Filter']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rRDataSelectorFieldFilter';
    }
}
