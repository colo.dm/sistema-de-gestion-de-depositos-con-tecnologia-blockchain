<?php

namespace TFG\Modules\Data\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class Filter extends TableExtended
{
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $conditionId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $filterTypeId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'RFilterValues', 'filterId', ['alias' => 'RFilterValues']);
        $this->hasMany('id', 'Entity\Modules\Data\Models\RrDataSelectorFieldFilter', 'filterId', ['alias' => 'RRDataSelectorFieldFilter']);
        $this->belongsTo('conditionId', '\Condition', 'id', ['alias' => 'Condition']);
        $this->belongsTo('filterTypeId', '\FilterType', 'id', ['alias' => 'FilterType']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'filter';
    }
}
