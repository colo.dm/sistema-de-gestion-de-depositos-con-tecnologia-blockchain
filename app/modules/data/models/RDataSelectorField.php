<?php

namespace TFG\Modules\Data\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;
use TFG\Modules\Entity\Models\Field as Field;

class RDataSelectorField extends \Entity\Models\Table
{
    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $dataSelectorId;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $fieldId;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $position;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $fieldFormat;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('dataSelectorId', 'Entity\Modules\Data\Models\RrDataSelectorFieldFilter', 'rDataSelectorFieldDataSelectorId', ['alias' => 'RRDataSelectorFieldFilter']);
        $this->belongsTo('dataSelectorId', 'Entity\Modules\Data\Models\DataSelector', 'id', ['alias' => 'DataSelector']);
        $this->belongsTo('fieldId', 'Entity\Modules\Entity\Models\Field', 'id', ['alias' => 'Field']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rDataSelectorField';
    }

    public function getFieldFormatQuery() {
        if ($this->Field->fieldTypeId == 1) {
            $mainTable = $this->Field->EntityReference->getTableName();
            $params = json_decode($this->fieldFormat);
            //var_dump(substr($this->fieldFormat,0));
            $columns = 'SELECT '.$mainTable.'.id, md5(CONCAT(';
            $separator = '';
            $join = '';
            foreach ($params as $paramIndex => $param) {
                $previousField = $this->Field;
                $columns .= $separator;
                $tableAlias = $mainTable;
                $param = str_replace($this->Field->id, '', $param);
                if ((strpos($param, '{') === 0) && (strrpos($param, '}') === strlen($param)-1)) {
                    $fieldParams = explode('.', $param);
                    $fieldCount = count($fieldParams);
                    $related = $fieldCount > 1;
                    foreach ($fieldParams as $index => $fieldParam) {
                        $fieldId = (int)substr($fieldParam, 1,-1);
                        $field = Field::findFirstById($fieldId);
                        if (($field->getTableName() != $mainTable) && (($index) < $fieldCount)) {
                            $previusTableAlias = ($index == 1) ? $mainTable : 'e'.$paramIndex.'_'.($index-1);
                            $tableAlias = 'e'.$paramIndex.'_'.$index;
                            $newJoin = PHP_EOL.' LEFT JOIN '.$field->getTableName().
                                     ' AS '.$tableAlias.' ON '.$tableAlias.'.id = '.$previusTableAlias.'.'.$previousField->getColumnName();
                            if (strpos($join, $newJoin) === FALSE){
                                $join .= $newJoin;
                            }
                        }
                        if (($index+1) == $fieldCount) {
                            $columns .= 'coalesce('.$tableAlias.'.'.$field->getColumnName().",' ')";
                        }
                        $previousField = $field;
                    }
                } else {
                    $columns .= "'$param'";
                }
                $separator = ', ';
            }
            $columns .= ')) as format FROM '.$mainTable.$join;
            return $columns;
            //die($columns.PHP_EOL);
            /*
            foreach ($this->Field->EntityReference->Field as $field) {
                foreach ($params as $param) {
                    if (!is_string($param) && (($field->id)==$param)) {
                        if ($field->fieldTypeId == 1) {
                            //
                        }
                    }
                }
            }*/

        }
    }
}
