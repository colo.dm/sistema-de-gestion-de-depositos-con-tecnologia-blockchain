<?php

namespace TFG\Modules\Data\Models;

class MultipleEntityRelated extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    private $dataEntityIdParent;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    private $dataEntityIdChild;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    private $fieldId;

    public function setFieldId($fieldId){
        $this->fieldId = (int) $fieldId;
    }

    public function setDataEntityIdParent($dataEntityIdParent){
        $this->dataEntityIdParent = (int) $dataEntityIdParent;
    }

    public function setDataEntityIdChild($dataEntityIdChild){
        $this->dataEntityIdChild = (int) $dataEntityIdChild;
    }

    public function getDataEntityIdChild(){
        return (int) $this->dataEntityIdChild;
    }

    public function getDataEntityIdParent(){
        return (int) $this->dataEntityIdParent;
    }

    public function getFieldId(){
        return (int) $this->fieldId;
    }


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->belongsTo('dataEntityIdChild', 'Entity\Models\DataEntity', 'id', ['alias' => 'DataEntityChild']);
        $this->belongsTo('dataEntityIdParent', 'Entity\Models\DataEntity', 'id', ['alias' => 'DataEntityParent']);
        $this->belongsTo('fieldId', '\Field', 'id', ['alias' => 'Field']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'multipleEntityRelated';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Multipleentityrelated[]|Multipleentityrelated
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Multipleentityrelated
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Multipleentityrelated
     */
    public static function findByParentId($parentId, $fieldId, $childId = null)
    {
        $extraFilter = isset($childId) ? ' AND dataEntityIdChild = '.((int)$childId) : '';
        if (($fieldId == null) && ($childId != null)) {
            return parent::find("dataEntityIdParent = ".$parentId.$extraFilter);
        }
        $parameters = [];
        $parameters["conditions"] = "dataEntityIdParent = ".$parentId.$extraFilter." AND fieldId = ".$fieldId;
        $parameters["order"] = "dataEntityIdChild desc";
        return parent::find($parameters);
    }

    public static function findByFieldId($fieldId)
    {
        return parent::find("fieldId = ".$fieldId);
    }
}
