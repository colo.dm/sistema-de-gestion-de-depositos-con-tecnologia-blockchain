<?php

namespace TFG\Modules\Data\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class DataSelector extends \Entity\Models\TableExtended
{
    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $entityId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'Entity\Modules\Data\Models\RDataSelectorField', 'dataSelectorId',
            [
                'alias' => 'RDataSelectorField',
                'params' => [
                    'order' => 'Entity\Modules\Data\Models\RDataSelectorField.position'
                ]
            ]);
        $this->hasMany('id', 'Entity\Modules\Layout\Models\Spec', 'dataSelectorId', ['alias' => 'Spec']);
        $this->belongsTo('entityId', 'Entity\Modules\Entity\Models\Entity', 'id', ['alias' => 'Entity']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'dataSelector';
    }

    protected function beforeDelete() {
        foreach ($this->Spec as $parent) {
            $parent->delete();
        }
    }
}
