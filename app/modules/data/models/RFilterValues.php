<?php

namespace TFG\Modules\Data\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;

class RFilterValues extends Table
{
    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $filterId;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $valuesId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->belongsTo('filterId', '\Filter', 'id', ['alias' => 'Filter']);
        $this->belongsTo('valuesId', '\Values', 'id', ['alias' => 'Values']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'rFilterValues';
    }
}
