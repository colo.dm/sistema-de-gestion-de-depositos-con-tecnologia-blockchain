<?php

namespace TFG\Modules\Data\Models;

use InvalidArgumentException;
use Phalcon\Mvc\Model;


class Values extends TableId
{
    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $value;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'RFilterValues', 'valuesId', ['alias' => 'RFilterValues']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'values';
    }
}
