<?php
namespace Entity\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller {
    function setHeader($response) {
        $response->setHeader('Access-Control-Allow-Origin', '*');
        $response->setContentType('application/json', 'UTF-8');
        return $response;
    }

    function sendEntityStatus200($response, $message = 'Ok', $layoutUrl=null) {
        $this->setHeader($response);
        $response->setStatusCode(200, $message);
        $response->setJsonContent([
            'msg' => $message,
            'layoutUrl' => $layoutUrl
        ]);
        return $response->send();
    }

    function sendEntityStatus404($response, $message = 'Not found', $details = null) {
        $response->setContentType('application/json', 'UTF-8');
        $response->setJsonContent(['msg' => $message.(isset($details) ? ": $details" : "")]);
        $response->setStatusCode(404, $message);
        return $response->send();
    }

    function sendEntityStatus501($response, $entity = null, $message = 'Internal error') {
        $response->setContentType('application/json', 'UTF-8');
        if (isset($entity)) {
            foreach ($entity->getMessages() as $errorMessage) {
                $msg .= $errorMessage.'. ';
            }
        }
        $response->setJsonContent(['msg' => $message.(isset($msg) ? ": $msg" : '')]);
        $response->setStatusCode(501, $message);
        return $response->send();
    }
}
