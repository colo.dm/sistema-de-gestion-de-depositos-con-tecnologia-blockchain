<?php

namespace Entity\Models;

class TableWithTimeStamps extends TableId
{
    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $lastUpdate;

    /**
     * Returns the value of field created
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Returns the value of field lastUpdate
     *
     * @return string
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    protected function beforeCreate()
    {
        // Set the creation date
        $this->created = date("Y-m-d H:i:s");
        $this->lastUpdate = date("Y-m-d H:i:s");
    }

    protected function beforeUpdate()
    {
        // Set the modification date
        $this->lastUpdate = date("Y-m-d H:i:s");
    }

}
