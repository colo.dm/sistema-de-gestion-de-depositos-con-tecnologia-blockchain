<?php

namespace Entity\Models;

class Table extends \Phalcon\Mvc\Model
{
    /**
     * Initialize method for model.
     */
    protected function initialize()
    {
        $this->setSchema("entity");
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return null;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Base[]|Base
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Base
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
