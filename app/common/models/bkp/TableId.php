<?php

namespace Entity\Models;

class TableId extends Table
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    protected $id;    
    

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Initialize method for model.
     */
    protected function initialize()
    {
        parent::initialize();
    }

}
