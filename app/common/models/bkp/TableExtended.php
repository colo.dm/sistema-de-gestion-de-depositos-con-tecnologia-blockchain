<?php

namespace Entity\Models;

class TableExtended extends TableId
{
    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $created;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $lastUpdate;

    /**
     *
     * @var string
     * @Column(type="string", length=190, nullable=true)
     */
    protected $name;

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }


    /**
     * Returns the value of field created
     *
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Returns the value of field lastUpdate
     *
     * @return string
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    protected function beforeCreate()
    {
        // Set the creation date
        $this->created = date("Y-m-d H:i:s");
        $this->lastUpdate = date("Y-m-d H:i:s");
    }

    protected function beforeUpdate()
    {
        // Set the modification date
        $this->lastUpdate = date("Y-m-d H:i:s");
    }

}
