<?php

class MultipleEntityRelated extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $dataEntityIdParent;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $dataEntityIdChild;

    /**
     *
     * @var integer
     * @Primary
     * @Column(type="integer", length=11, nullable=false)
     */
    public $fieldId;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->belongsTo('dataEntityIdChild', '\DataEntity', 'id', ['alias' => 'DataEntity']);
        $this->belongsTo('dataEntityIdParent', '\DataEntity', 'id', ['alias' => 'DataEntity']);
        $this->belongsTo('fieldId', '\Field', 'id', ['alias' => 'Field']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'multipleEntityRelated';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Multipleentityrelated[]|Multipleentityrelated
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Multipleentityrelated
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
