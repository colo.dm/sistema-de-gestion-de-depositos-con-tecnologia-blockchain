<?php

class Owner extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $created;

    /**
     *
     * @var string
     * @Column(type="string", nullable=true)
     */
    public $lastUpdate;

    /**
     *
     * @var string
     * @Column(type="string", length=190, nullable=true)
     */
    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("entity");
        $this->hasMany('id', 'AppGroup', 'ownerId', ['alias' => 'AppGroup']);
        $this->hasMany('id', 'AppUser', 'ownerId', ['alias' => 'AppUser']);
        $this->hasMany('id', 'Entity', 'ownerId', ['alias' => 'Entity']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'owner';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Owner[]|Owner
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Owner
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
