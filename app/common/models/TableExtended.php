<?php

namespace Entity\Models;

class TableExtended extends TableWithTimeStamps
{

    /**
     *
     * @var string
     * @Column(type="string", length=190, nullable=true)
     */
    protected $name;

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
