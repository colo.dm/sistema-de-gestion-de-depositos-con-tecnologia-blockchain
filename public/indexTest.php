<?php
require '../app/bootstrap_web.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Entity</title>
  <link rel="stylesheet" type="text/css" href="css/font-awesome-4.7.0/css/font-awesome.min.css">
</head>

<body>
<div id="main">
    </div>
    <script type="text/javascript" src="js/common.js"></script>
    <script type="text/javascript" src="js/axios.js"></script>
    <script type="text/javascript" src="js/lodash.js"></script>
    <script type="text/javascript" src="js/vue.js"></script>
    <script type="text/javascript" src="js/ui.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</body>

</html>