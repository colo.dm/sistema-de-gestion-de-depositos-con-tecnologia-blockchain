Vue.component('text-field', {
  template: '\
  <div class="text-field">\
    <el-input :type="subtype.text" :placeholder="field.placeholder" v-model="data.value"/>\
  </div>',
  props: ["url","field","data"],
  data: function () {
    return {
      subtype: { 
        text: 'text',
        textArea: 'textArea',
        title: 'title' 
      }
    }
  }
});

Vue.component('number-field', {
  template: '\
    <div class="entity-field">\
      <el-input-number v-if="field.subtype === subtype.int" class="form-control" :controls="controls" :placeholder="field.placeholder"\
          v-model="data.value"/>\
      <el-input-number v-if="field.subtype === subtype.decimal" class="form-control" :controls="controls" :placeholder="field.placeholder"\
          v-model="data.value"/>\
    </div>',
  props: ["url","field","data"],
  data: function () {
    return {
      controls: false,
      subtype: { 
        int: 'int',
        decimal: 'decimal'
      }
    }
  }
});

Vue.component('select-field', {
  template: '\
    <div class="entity-field">\
      <el-select v-model="data.value" :options="field.options" :multiple="field.multiple" :placeholder="field.placeholder">\
        <el-option-group v-if="field.subtype === subtype.grouping"\
          v-for="group in field.groups"\
          :key="group.label"\
          :label="group.label">\
          <el-option\
            v-for="item in group.options"\
            :key="item.value"\
            :label="item.label"\
            :value="item.value">\
          </el-option>\
        </el-option-group>\
        <el-option v-if="field.subtype === subtype.single"\
          v-for="item in field.options"\
          :key="item.value"\
          :label="item.label"\
          :value="item.value">\
        </el-option>\
      </el-select>\
      <el-form label-position="right" class="subOptions">\
        <div v-for="(field, fieldIndex) in getSubOptions(data.value)">\
          <el-form-item :label="field.label">\
            <text-field   v-if="field.type === type.text"   :field="field" :data="getFieldData(data.subOptions,field.id)"/>\
            <number-field v-if="field.type === type.number" :field="field" :data="getFieldData(data.subOptions,field.id)"/>\
            <select-field v-if="field.type === type.select" :field="field" :data="getFieldData(data.subOptions,field.id)"/>\
            <check-field  v-if="field.type === type.check"  :field="field" :data="getFieldData(data.subOptions,field.id)"/>\
            <switch-field  v-if="field.type === type.switch"  :field="field" :data="getFieldData(data.subOptions,field.id)"/>\
          </el-form-item>\
        </div>\
      </el-form>\
    </div>',
  props: ["url","field","data"],
  data: function () {
    return {
      subtype: { 
        single: 'single',
        grouping: 'grouping'
      },
      type: {
        text: 'text',
        number: 'number',
        select: 'select',
        check: 'check',
        switch: 'switch'
      }
    }
  },
  methods:{
    getSubOptions: function(optionId) {
      for (var g = 0; g < this.field.groups.length; g++) {
        for (var i = 0; i < this.field.groups[g].options.length; i++) {
          if (this.field.groups[g].options[i].value == this.data.value) {
            return this.field.groups[g].optionsSpec;
          }
        }
      }
    },
    getFieldData: function(values, fieldId){  
      if(fieldId > 0){    
        for (var j = 0; j < values.length; j++) {
          if(values[j].id == fieldId){
            return values[j];
          }
        }
      }
      return {
        'id': fieldId,
        'value': null
      };
    }
  }
});

Vue.component('check-field', {
  template: '\
    <div class="entity-field">\
      <b-form-checkbox v-model="field.value" value="true" unchecked-value="false">{{field.label ? field.label : text}}\
      </b-form-checkbox>\
    </div>',
  props: ["url","field","text"],
  data: function () {
    return {
    }
  }
});

Vue.component('switch-field', {
  template: '\
  <div class="entity-field">\
    <el-switch\
      v-model="data.value"\
      on-text=""\
      off-text=""\
      on-value="1"\
      off-value="0">\
    </el-switch>\
  </div>',
  props: ["url","field","data"],
  data: function () {
    return {
    }
  }
});

Vue.component('view-type-form', {
  template: '\
    <div>\
      <div class="entity-form" v-for="(entity, entityIndex) in entities" :key="entity.id">\
        <el-card  class="box-card specHeader">\
          <div slot="header" class="clearfix">\
            <div v-for="(field, fieldIndex) in spec.fields" class="specHeaderOptions">\
              <div :label="getFieldLabel(field)">\
                <text-field   v-if="field.type === type.text"   :field="field" :data="getFieldData(entity,field.id)"/>\
                <number-field v-if="field.type === type.number" :field="field" :data="getFieldData(entity,field.id)"/>\
                <select-field v-if="field.type === type.select" :field="field" :data="getFieldData(entity,field.id)"/>\
                <check-field  v-if="field.type === type.check"  :field="field" :data="getFieldData(entity,field.id)"/>\
                <switch-field  v-if="field.type === type.switch"  :field="field" :data="getFieldData(entity,field.id)"/>\
              </div>\
            </div>\
            <!--el-button class="fa fa-sliders actions-icon" aria-hidden="true"></el-button-->\
          </div>\
        <el-form label-position="right">\
          <div v-for="(field, fieldIndex) in spec.fields">\
            <el-form-item :label="getFieldLabel(field)">\
              <text-field   v-if="field.type === type.text"   :field="field" :data="getFieldData(entity,field.id)"/>\
              <number-field v-if="field.type === type.number" :field="field" :data="getFieldData(entity,field.id)"/>\
              <select-field v-if="field.type === type.select" :field="field" :data="getFieldData(entity,field.id)"/>\
              <check-field  v-if="field.type === type.check"  :field="field" :data="getFieldData(entity,field.id)"/>\
              <switch-field  v-if="field.type === type.switch"  :field="field" :data="getFieldData(entity,field.id)"/>\
            </el-form-item>\
          </div>\
        </el-form></el-card>\
      </div>\
    </div>',
  props: ["url","spec","entities"],
  data: function () {
    return {
      type: {
        text: 'text',
        number: 'number',
        select: 'select',
        check: 'check',
        switch: 'switch'
      }
    }
  },
  methods:{
    getFieldLabel: function(field){
        return field.label+':';
      },
    getFieldData: function(entity, fieldId){  
      if(fieldId > 0){    
        for (var j = 0; j < entity.values.length; j++) {
          if(entity.values[j].id == fieldId){
            return entity.values[j];
          }
        }
      }
      return {
        'id': fieldId,
        'value': null
      };
    }
  }
});

Vue.component('view-type-form-collapse', {
  template: '\
    <div>\
      <el-collapse v-model="activeName" accordion v-for="(entity, entityIndex) in entities" :key="entity.id">\
        <el-collapse-item :title="getFieldData(entity,1).value" :name="entity.id">\
          <el-form label-position="right">\
            <div v-for="(field, fieldIndex) in spec.fields">\
              <el-form-item :label="getFieldLabel(field)">\
                <text-field   v-if="field.type === type.text"   :field="field" :data="getFieldData(entity,field.id)"/>\
                <number-field v-if="field.type === type.number" :field="field" :data="getFieldData(entity,field.id)"/>\
                <select-field v-if="field.type === type.select" :field="field" :data="getFieldData(entity,field.id)"/>\
                <check-field  v-if="field.type === type.check"  :field="field" :data="getFieldData(entity,field.id)"/>\
                <switch-field  v-if="field.type === type.switch"  :field="field" :data="getFieldData(entity,field.id)"/>\
              </el-form-item>\
            </div>\
          </el-form>\
        </el-collapse-item>\
      </el-collapse>\
    </div>',
  props: ["url","spec","entities"],
  data: function () {
    return {
      type: {
        text: 'text',
        number: 'number',
        select: 'select',
        check: 'check',
        switch: 'switch'
      },
      activeName: 1
    }
  },
  methods:{
    getFieldLabel: function(field){
        return field.label+':';
      },
    getFieldData: function(entity, fieldId){  
      if(fieldId > 0){    
        for (var j = 0; j < entity.values.length; j++) {
          if(entity.values[j].id == fieldId){
            return entity.values[j];
          }
        }
      }
      return {
        'id': fieldId,
        'value': null
      };
    }
  }
});

Vue.component('view-type-table', {
  template: '\
  <div>\
    <el-table\
        :data="arrayData"\
        style="width: 100%">\
        <!--el-table-column type="expand">\
          <template scope="props">\
          </template>\
        </el-table-column-->\
        <el-table-column v-for="(field, fieldIndex) in spec.fields" :key="field.id"\
          :label="field.label"\
          :prop="field.label">\
        </el-table-column>\
    </el-table>\
  </div>',
  props: ["url","spec","entities"],
  data: function () {
    return {
    }
  },
  watch: {
    arrayData: function(){

    }
  },
  computed: {
    arrayData: function() {
      var data = [];    
      for (var e = 0; e < this.entities.length; e++) {
        var entity = {};
        for (var f = 0; f < this.spec.fields.length; f++) {  
          Object.defineProperty(entity, this.spec.fields[f].label, {
            value : this.getFieldData(this.entities[e], this.spec.fields[f].id),
            writable : true,
            enumerable : true,
            configurable : true});   
        }
        data.push(entity);
      }
      return data;        
    }
  },
  methods:{
    getFieldLabel: function(field){
        return field.label+':';
      },
    getFieldData: function(entity, fieldId){   
      if(fieldId > 0){    
        for (var j = 0; j < entity.values.length; j++) {
          if(entity.values[j].id == fieldId){
            return entity.values[j].value;
          }
        }
      }
      return {
        'id': fieldId,
        'value': null
      };
    }
  }
});

Vue.component('spec-settings', {
  template: '\
    <div>\
      <div class="el-col el-col-2">&nbsp;</div>\
      <el-card class="el-col el-col-24 el-col-xs-24 el-col-sm-23 el-col-md-20 el-col-lg-20">\
        <div slot="header" class="clearfix">\
          <span>Entity</span>\
        </div>\
        <el-tabs activeName="default">\
          <el-tab-pane v-for="(tab, tabIndex) in entities.layout.tabs" :key="tab.name" :label="tab.label" :name="tab.name">\
            <el-row :gutter="5" v-for="(row, rowIndex) in tab.rows" :key="row.id">\
              <el-col v-for="(column, columnIndex) in row.columns" :key="column.id" :xs="column.xs" :sm="column.sm" :md="column.md" :lg="column.lg">\
                <div v-for="(specDef, specIndex) in column.specs">\
                  <view-type-form-collapse :spec="getSpec(specDef.hash)" :entities="getSpecData(specDef.hash)"/>\
                </div>\
              </el-col>\
            </el-row>\
          </el-tab-pane>\
        </el-tabs>\
        <div class="col-sm-offset-2 col-sm-10">\
          <el-button type="primary" v-on:click="save()">Save</el-button>\
          <el-button type="text" v-on:click="getUrlData()">Reload</el-button>\
        </div>\
      </el-card>\
    </div>',
    props: ["url"],
    data: function () {
      return {
        originalValues: {
          specs:[],
          data:[]
        },
        entityList: {
          specs:[],
          layout:{
            tabs:[
              {
                rows:[
                  {
                    columns:[]
                  }
                ]
              }
            ]
          },
          data:[]
        }
      }
    },
    created: function () {
      this.getUrlData();
    },
    watch: {
      entities: function (value) {
        this.entityList = value;        
      }
    },
    computed: {
      entities: function() {
        return this.entityList;        
      },
      label: function(){
        var format = this.entityList.label.format;
        var output = "";
        for (var i = 0; i < format.length; i++) {
          if (typeof format[i] === 'string'){
            output += format[i]; 
          } else {
            var selObj = this.entityList.fields.filter(function( obj ) {
              return obj.id == format[i];
            });
            output += selObj[0].value;
          }
        }
        return output;
      }
    },
    methods: {
      clone: function( obj ){
        return JSON.parse(JSON.stringify(obj));
      },
      getUrlData: _.debounce(
        function () {
          var instance = this;  
          var now = new Date().toLocaleString();
          axios.get(instance.url +'?'+now)
            .then(function (response) {
              instance.entityList = response.data;
              instance.originalValues = instance.clone(instance.entityList); 
            })
            .catch(function (error) {
              //instance.answer = 'Error! Could not reach the API. ' + error
            })          
        },
        500
      ),
      getSpec: function(hash){
        for (var i = 0; i < this.entityList.specs.length; i++) {
          if(this.entityList.specs[i].hash == hash){
            return this.entityList.specs[i];
          }
        }
      },
      getSpecData: function(spec){  
        for (var j = 0; j < this.entityList.data.length; j++) {
          if(this.entityList.data[j].specHash == spec){            
            return this.entityList.data[j].entities;
          } 
        }
        return [];
      },
      getSpecEntityLabel: function(hash, entity){
        var label = '';
        var value = '';
        var spec = this.getSpec(hash);
        for (var i = 0; i < spec.label.format.length; i++) {
          if (Number.isInteger(spec.label.format[i])) {
            value = this.getFieldData(entity, spec.label.format[i]).value;
            if (value != null) {
              label += value;
            }
          } else {
            label += spec.label.format[i];
          }
        }
        return label;
      },
      save: function() {
        var data = [];
        for (var i = 0; i < this.entityList.data.length; i++) {
          var newData = {
            specHash: this.entityList.data[i].specHash,
            entities: []
          };
          for (var e = 0; e < this.entityList.data[i].entities.length; e++) {
            var newEntity = {
              id: this.entityList.data[i].entities[e].id,
              values: []
            };
            for (var j = 0; j < this.entityList.data[i].entities[e].values.length; j++) {              
              if (JSON.stringify(this.entityList.data[i].entities[e].values[j]) != JSON.stringify(this.originalValues.data[i].entities[e].values[j])){
                var field = this.entityList.data[i].entities[e].values[j];
                newEntity.values.push(field);
              } 
            }

            if(newEntity.values.length > 0) {
              newData.entities.push(newEntity);
            }
          }
          if(newData.entities.length > 0) {
            data.push(newData);
          }
        }
        //valores para actualizar
        console.log(JSON.stringify(data));
      }
    }
});