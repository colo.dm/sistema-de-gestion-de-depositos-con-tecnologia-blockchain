Vue.component('main-menu', {
  template: '\
  <el-menu :default-active="activeIndex" class="el-menu-demo" mode="horizontal" @select="handleSelect">\
    <div v-for="(menuItem, menuItemIndex) in menu.items" :key="menuItem.id">\
      <el-menu-item v-if="menuItem.type === itemType.item" :index="menuItem.url">\
        <i v-bind:class="getItemIconClass(menuItem.icon)" aria-hidden="true"/>{{ menuItem.label }}</i>\
      </el-menu-item>\
      <el-submenu v-if="menuItem.type === itemType.subItem" :index="menuItem.url">\
        <template slot="title">\
          <i v-bind:class="getItemIconClass(menuItem.icon)" aria-hidden="true"/>{{ menuItem.label }}</i>\
        </template>\
        <div v-for="(subItem, subItemIndex) in menuItem.subItems" :key="subItem.id">\
          <el-menu-item :index="subItem.url">\
          <i v-bind:class="getItemIconClass(subItem.icon)" aria-hidden="true"/>{{ subItem.label }}\
          </el-menu-item>\
        </div>\
      </el-submenu>\
    </div>\
    <el-menu-item class="menuSearch" index="search">\
      <el-select\
        v-model="searchValue"\
        filterable\
        remote\
        placeholder="Buscar"\
        :remote-method="remoteSearch"\
        :change="searchSelect()"\
        :loading="loading">\
        <el-option\
          v-for="item in searchResult"\
          :key="item.value"\
          :label="item.label"\
          :value="item.value">\
        </el-option>\
      </el-select>\
    </el-menu-item>\
  </el-menu>',
  props: ["url"],
  data: function () {
    return {
      searchValue: null,
      searchResultValues: [],
      loading: false,
      activeIndex: '1',
      itemType:{
        item: 'item',
        subItem: 'subItem',
        icon: 'icon'
      },
      mainMenu:{
        items:[
          {
            type:''
          }
        ]
      }
    }
  },
  created: function () {
    this.getUrlData();
  },
  watch: {
    menu: function (value) {
      this.mainMenu = value;        
    },
    searchResult: function(values) {
      this.searchResultValues = values;
    }
  },
  computed: {
    menu: function() {
      return this.mainMenu;        
    },
    searchResult: function() {
      return this.searchResultValues;
    }
  },
  methods: {    
    getUrlData: _.debounce(
      function () {
        var instance = this;  
        var now = new Date().toLocaleString();
        axios.get(instance.url +'?'+now)
          .then(function (response) {
            instance.mainMenu = response.data;
          })
          .catch(function (error) {
            //instance.answer = 'Error! Could not reach the API. ' + error
          })          
      },
      500
    ),
    handleSelect(key, keyPath) {
      console.log(key, keyPath);
    },
    getItemIconClass(icon) {
      return "sfa fa fa-"+icon;
    },
    remoteSearch: _.debounce(
      function () {
        var instance = this;  
        var now = new Date().toLocaleString();
        axios.get('http://localhost/entity/js/data/searchItems.json?'+now)
          .then(function (response) {
            instance.searchResultValues = response.data;
          })
          .catch(function (error) {
            //instance.answer = 'Error! Could not reach the API. ' + error
          })          
      },
      500
    ),
    searchSelect: function() {
      this.searchValue = null;
    }
  }
});

var app = new Vue({
  el: '#app',
  data: function() {
  	return {
      message: '',
      showNav: true,      
      desktopMode: false
  	}
  },
  created: function () {
    this.resizeApp();
  },
  watch: {
	  showMenu: function (value) {
	    this.showNav = value;    
	  },
    collapse: function (value) {
      this.onlyIcons = value;    
    }
	},
  computed: {
	  showMenu: function() {
	    return this.showNav;        
	  },
    collapse: function() {
      return this.onlyIcons;        
    }
	},
  methods: {
    toogleMenu: function() {    	  
      this.showNav = !(this.showNav);
    },
    forceShowMenu: function() {
    	this.showNav = true;
      this.resizeApp();
    },
    forceHideMenu: function() {
    	this.showNav = false;
      this.resizeApp();
    },
    menuStyle: function(){
  		var title = document.getElementById('mainMenu').getBoundingClientRect();
  		return 'height:'+(window.innerHeight-title.height-4).toString()+'px';
    },   
    setHeight: function(elementId, size){
      document.getElementById(elementId).style.height = size.toString()+'px';
    },
    resizeApp: function(){
      var appContentSize = 0;
      if (window.innerWidth < 768) {
        this.desktopMode= false;
        var mainMenu = document.getElementById('mainMenu').getBoundingClientRect();        
        appContentSize = window.innerHeight-mainMenu.height;
      } else {
        this.desktopMode= true;
        appContentSize = window.innerHeight-30;
      }  
      //this.setHeight('appContent', appContentSize);
      //this.setHeight('sidebarItems', window.innerHeight);
      this.setHeight('app', window.innerHeight);
    }
  }
});


function Resize(){
  if (window.innerWidth < 768) {
    app.forceHideMenu();
  } else {
    app.forceShowMenu();
  }
}                            
    
window.onresize=Resize;
window.onload=Resize;