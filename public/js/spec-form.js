Vue.component('text-field', {
  template: '\
  <div class="text-field">\
    <el-input :type="subtype.text" :placeholder="field.placeholder" v-model="data.value"/>\
  </div>',
  props: ["url","field","data"],
  data: function () {
    return {
      subtype: { 
        text: 'text',
        textArea: 'textArea',
        title: 'title' 
      }
    }
  }
});

Vue.component('number-field', {
  template: '\
    <div class="entity-field">\
      <el-input-number v-if="field.subtype === subtype.int" class="form-control" :controls="controls" :placeholder="field.placeholder"\
          v-model="data.value"/>\
      <el-input-number v-if="field.subtype === subtype.decimal" class="form-control" :controls="controls" :placeholder="field.placeholder"\
          v-model="data.value"/>\
    </div>',
  props: ["url","field","data"],
  data: function () {
    return {
      controls: false,
      subtype: { 
        int: 'int',
        decimal: 'decimal'
      }
    }
  }
});

Vue.component('select-field', {
  template: '\
    <div class="entity-field">\
      <el-select v-model="data.value" :options="field.options" :multiple="field.multiple" :placeholder="field.placeholder">\
        <el-option-group v-if="field.subtype === subtype.grouping"\
          v-for="group in field.groups"\
          :key="group.label"\
          :label="group.label">\
          <el-option\
            v-for="item in group.options"\
            :key="item.value"\
            :label="item.label"\
            :value="item.value">\
          </el-option>\
        </el-option-group>\
        <el-option v-if="field.subtype === subtype.single"\
          v-for="item in field.options"\
          :key="item.value"\
          :label="item.label"\
          :value="item.value">\
        </el-option>\
      </el-select>\
    </div>',
  props: ["url","field","data"],
  data: function () {
    return {
      subtype: { 
        single: 'single',
        grouping: 'grouping'
      }
    }
  }
});

Vue.component('check-field', {
  template: '\
    <div class="entity-field">\
      <b-form-checkbox v-model="field.value" value="true" unchecked-value="false">{{field.label ? field.label : text}}\
      </b-form-checkbox>\
    </div>',
  props: ["url","field","text"],
  data: function () {
    return {
    }
  }
});

Vue.component('spec-form', {
  template: '\
    <div>\
      <el-row :gutter="5" v-for="(row, rowIndex) in entities.layout.rows" :key="row.id">\
        <el-col v-for="(column, columnIndex) in row.columns" :key="column.id" :xs="column.xs" :sm="column.sm" :md="column.md" :lg="column.lg">\
          <div class="entity-form" v-for="(data, dataIndex) in getSpecData(column.specs)">\
            <el-card v-for="(entity, entityIndex) in data.entities" :key="entity.id" class="box-card">\
              <div slot="header" class="clearfix">\
                <span style="line-height: 36px;">{{getSpecEntityLabel(data.specHash,entity)}}</span>\
                <el-button class="fa fa-sliders actions-icon" aria-hidden="true"></el-button>\
              </div>\
            <el-form label-position="right">\
              <div v-for="(field, fieldIndex) in getSpec(data.specHash).fields">\
                <el-form-item :label="getFieldLabel(field)">\
                  <text-field   v-if="field.type === type.text"   :field="field" :data="getFieldData(entity,field.id)"/>\
                  <number-field v-if="field.type === type.number" :field="field" :data="getFieldData(entity,field.id)"/>\
                  <select-field v-if="field.type === type.select" :field="field" :data="getFieldData(entity,field.id)"/>\
                  <check-field  v-if="field.type === type.check"  :field="field" :data="getFieldData(entity,field.id)"/>\
                </el-form-item>\
              </div>\
            </el-form></el-card>\
          </div>\
        </el-col>\
      </el-row>\
      <div class="col-sm-offset-2 col-sm-10">\
        <el-button type="primary" v-on:click="save()">Save</el-button>\
        <el-button type="text" v-on:click="getUrlData()">Reload</el-button>\
      </div>\
    </div>',
    props: ["url"],
    data: function () {
      return {
        originalValues: {
          specs:[],
          data:[]
        },
        entityList: {
          specs:[],
          layout:{
            rows:[
              {
                columns:[]
              }
            ]
          },
          data:[]
        },
        type: {
          text: 'text',
          number: 'number',
          select: 'select',
          check: 'check'
        }
      }
    },
    created: function () {
      this.getUrlData();
    },
    watch: {
      entities: function (value) {
        this.entityList = value;        
      }
    },
    computed: {
      entities: function() {
        return this.entityList;        
      },
      label: function(){
        var format = this.entityList.label.format;
        var output = "";
        for (var i = 0; i < format.length; i++) {
          if (typeof format[i] === 'string'){
            output += format[i]; 
          } else {
            var selObj = this.entityList.fields.filter(function( obj ) {
              return obj.id == format[i];
            });
            output += selObj[0].value;
          }
        }
        return output;
      }
    },
    methods: {
      clone: function( obj ){
        return JSON.parse(JSON.stringify(obj));
      },
      getUrlData: _.debounce(
        function () {
          var instance = this;  
          var now = new Date().toLocaleString();
          axios.get(instance.url +'?'+now)
            .then(function (response) {
              instance.entityList = response.data;
              instance.originalValues = instance.clone(instance.entityList); 
            })
            .catch(function (error) {
              //instance.answer = 'Error! Could not reach the API. ' + error
            })          
        },
        500
      ),
      getSpec: function(hash){
        for (var i = 0; i < this.entityList.specs.length; i++) {
          if(this.entityList.specs[i].hash == hash){
            return this.entityList.specs[i];
          }
        }
      },
      getSpecData: function(specs){  
        var data = [];
        for (var i = 0; i < specs.length; i++) {
          for (var j = 0; j < this.entityList.data.length; j++) {
            if(this.entityList.data[j].specHash == specs[i]){
              console.log(this.entityList.data[j].entities);
              data.push(this.entityList.data[j]);
            }
          }
        }
        return data;
      },
      getSpecEntityLabel: function(hash, entity){
        var label = '';
        var value = '';
        var spec = this.getSpec(hash);
        for (var i = 0; i < spec.label.format.length; i++) {
          if (Number.isInteger(spec.label.format[i])) {
            value = this.getFieldData(entity, spec.label.format[i]).value;
            if (value != null) {
              label += value;
            }
          } else {
            label += spec.label.format[i];
          }
        }
        return label;
      },
      getFieldLabel: function(field){
        return field.label+':';
      },
      getFieldData: function(entity, fieldId){    
        if(fieldId > 0){    
          for (var j = 0; j < entity.values.length; j++) {
            if(entity.values[j].id == fieldId){
              return entity.values[j];
            }
          }
        }
        return {
          'id': fieldId,
          'value': null
        };
      },
      save: function() {
        var data = [];
        for (var i = 0; i < this.entityList.data.length; i++) {
          var newData = {
            specHash: this.entityList.data[i].specHash,
            entities: []
          };
          for (var e = 0; e < this.entityList.data[i].entities.length; e++) {
            var newEntity = {
              id: this.entityList.data[i].entities[e].id,
              values: []
            };
            for (var j = 0; j < this.entityList.data[i].entities[e].values.length; j++) {
              if (this.entityList.data[i].entities[e].values[j].value != this.originalValues.data[i].entities[e].values[j].value){
                var field = this.entityList.data[i].entities[e].values[j];
                newEntity.values.push(field);
              }
            }
            if(newEntity.values.length > 0) {
              newData.entities.push(newEntity);
            }
          }
          if(newData.entities.length > 0) {
            data.push(newData);
          }
        }
        //valores para actualizar
        console.log(JSON.stringify(data));
      }
    }
});