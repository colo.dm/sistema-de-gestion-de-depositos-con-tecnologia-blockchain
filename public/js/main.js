webpackJsonp([1],[
/* 0 */,
/* 1 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 2 */,
/* 3 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),
/* 4 */,
/* 5 */,
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(process) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Store", function() { return Store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "install", function() { return install; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapState", function() { return mapState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapMutations", function() { return mapMutations; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapGetters", function() { return mapGetters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mapActions", function() { return mapActions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createNamespacedHelpers", function() { return createNamespacedHelpers; });
/**
 * vuex v3.0.1
 * (c) 2017 Evan You
 * @license MIT
 */
var applyMixin = function (Vue) {
  var version = Number(Vue.version.split('.')[0]);

  if (version >= 2) {
    Vue.mixin({ beforeCreate: vuexInit });
  } else {
    // override init and inject vuex init procedure
    // for 1.x backwards compatibility.
    var _init = Vue.prototype._init;
    Vue.prototype._init = function (options) {
      if ( options === void 0 ) options = {};

      options.init = options.init
        ? [vuexInit].concat(options.init)
        : vuexInit;
      _init.call(this, options);
    };
  }

  /**
   * Vuex init hook, injected into each instances init hooks list.
   */

  function vuexInit () {
    var options = this.$options;
    // store injection
    if (options.store) {
      this.$store = typeof options.store === 'function'
        ? options.store()
        : options.store;
    } else if (options.parent && options.parent.$store) {
      this.$store = options.parent.$store;
    }
  }
};

var devtoolHook =
  typeof window !== 'undefined' &&
  window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

function devtoolPlugin (store) {
  if (!devtoolHook) { return }

  store._devtoolHook = devtoolHook;

  devtoolHook.emit('vuex:init', store);

  devtoolHook.on('vuex:travel-to-state', function (targetState) {
    store.replaceState(targetState);
  });

  store.subscribe(function (mutation, state) {
    devtoolHook.emit('vuex:mutation', mutation, state);
  });
}

/**
 * Get the first item that pass the test
 * by second argument function
 *
 * @param {Array} list
 * @param {Function} f
 * @return {*}
 */
/**
 * Deep copy the given object considering circular structure.
 * This function caches all nested objects and its copies.
 * If it detects circular structure, use cached copy to avoid infinite loop.
 *
 * @param {*} obj
 * @param {Array<Object>} cache
 * @return {*}
 */


/**
 * forEach for object
 */
function forEachValue (obj, fn) {
  Object.keys(obj).forEach(function (key) { return fn(obj[key], key); });
}

function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

function isPromise (val) {
  return val && typeof val.then === 'function'
}

function assert (condition, msg) {
  if (!condition) { throw new Error(("[vuex] " + msg)) }
}

var Module = function Module (rawModule, runtime) {
  this.runtime = runtime;
  this._children = Object.create(null);
  this._rawModule = rawModule;
  var rawState = rawModule.state;
  this.state = (typeof rawState === 'function' ? rawState() : rawState) || {};
};

var prototypeAccessors$1 = { namespaced: { configurable: true } };

prototypeAccessors$1.namespaced.get = function () {
  return !!this._rawModule.namespaced
};

Module.prototype.addChild = function addChild (key, module) {
  this._children[key] = module;
};

Module.prototype.removeChild = function removeChild (key) {
  delete this._children[key];
};

Module.prototype.getChild = function getChild (key) {
  return this._children[key]
};

Module.prototype.update = function update (rawModule) {
  this._rawModule.namespaced = rawModule.namespaced;
  if (rawModule.actions) {
    this._rawModule.actions = rawModule.actions;
  }
  if (rawModule.mutations) {
    this._rawModule.mutations = rawModule.mutations;
  }
  if (rawModule.getters) {
    this._rawModule.getters = rawModule.getters;
  }
};

Module.prototype.forEachChild = function forEachChild (fn) {
  forEachValue(this._children, fn);
};

Module.prototype.forEachGetter = function forEachGetter (fn) {
  if (this._rawModule.getters) {
    forEachValue(this._rawModule.getters, fn);
  }
};

Module.prototype.forEachAction = function forEachAction (fn) {
  if (this._rawModule.actions) {
    forEachValue(this._rawModule.actions, fn);
  }
};

Module.prototype.forEachMutation = function forEachMutation (fn) {
  if (this._rawModule.mutations) {
    forEachValue(this._rawModule.mutations, fn);
  }
};

Object.defineProperties( Module.prototype, prototypeAccessors$1 );

var ModuleCollection = function ModuleCollection (rawRootModule) {
  // register root module (Vuex.Store options)
  this.register([], rawRootModule, false);
};

ModuleCollection.prototype.get = function get (path) {
  return path.reduce(function (module, key) {
    return module.getChild(key)
  }, this.root)
};

ModuleCollection.prototype.getNamespace = function getNamespace (path) {
  var module = this.root;
  return path.reduce(function (namespace, key) {
    module = module.getChild(key);
    return namespace + (module.namespaced ? key + '/' : '')
  }, '')
};

ModuleCollection.prototype.update = function update$1 (rawRootModule) {
  update([], this.root, rawRootModule);
};

ModuleCollection.prototype.register = function register (path, rawModule, runtime) {
    var this$1 = this;
    if ( runtime === void 0 ) runtime = true;

  if (process.env.NODE_ENV !== 'production') {
    assertRawModule(path, rawModule);
  }

  var newModule = new Module(rawModule, runtime);
  if (path.length === 0) {
    this.root = newModule;
  } else {
    var parent = this.get(path.slice(0, -1));
    parent.addChild(path[path.length - 1], newModule);
  }

  // register nested modules
  if (rawModule.modules) {
    forEachValue(rawModule.modules, function (rawChildModule, key) {
      this$1.register(path.concat(key), rawChildModule, runtime);
    });
  }
};

ModuleCollection.prototype.unregister = function unregister (path) {
  var parent = this.get(path.slice(0, -1));
  var key = path[path.length - 1];
  if (!parent.getChild(key).runtime) { return }

  parent.removeChild(key);
};

function update (path, targetModule, newModule) {
  if (process.env.NODE_ENV !== 'production') {
    assertRawModule(path, newModule);
  }

  // update target module
  targetModule.update(newModule);

  // update nested modules
  if (newModule.modules) {
    for (var key in newModule.modules) {
      if (!targetModule.getChild(key)) {
        if (process.env.NODE_ENV !== 'production') {
          console.warn(
            "[vuex] trying to add a new module '" + key + "' on hot reloading, " +
            'manual reload is needed'
          );
        }
        return
      }
      update(
        path.concat(key),
        targetModule.getChild(key),
        newModule.modules[key]
      );
    }
  }
}

var functionAssert = {
  assert: function (value) { return typeof value === 'function'; },
  expected: 'function'
};

var objectAssert = {
  assert: function (value) { return typeof value === 'function' ||
    (typeof value === 'object' && typeof value.handler === 'function'); },
  expected: 'function or object with "handler" function'
};

var assertTypes = {
  getters: functionAssert,
  mutations: functionAssert,
  actions: objectAssert
};

function assertRawModule (path, rawModule) {
  Object.keys(assertTypes).forEach(function (key) {
    if (!rawModule[key]) { return }

    var assertOptions = assertTypes[key];

    forEachValue(rawModule[key], function (value, type) {
      assert(
        assertOptions.assert(value),
        makeAssertionMessage(path, key, type, value, assertOptions.expected)
      );
    });
  });
}

function makeAssertionMessage (path, key, type, value, expected) {
  var buf = key + " should be " + expected + " but \"" + key + "." + type + "\"";
  if (path.length > 0) {
    buf += " in module \"" + (path.join('.')) + "\"";
  }
  buf += " is " + (JSON.stringify(value)) + ".";
  return buf
}

var Vue; // bind on install

var Store = function Store (options) {
  var this$1 = this;
  if ( options === void 0 ) options = {};

  // Auto install if it is not done yet and `window` has `Vue`.
  // To allow users to avoid auto-installation in some cases,
  // this code should be placed here. See #731
  if (!Vue && typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
  }

  if (process.env.NODE_ENV !== 'production') {
    assert(Vue, "must call Vue.use(Vuex) before creating a store instance.");
    assert(typeof Promise !== 'undefined', "vuex requires a Promise polyfill in this browser.");
    assert(this instanceof Store, "Store must be called with the new operator.");
  }

  var plugins = options.plugins; if ( plugins === void 0 ) plugins = [];
  var strict = options.strict; if ( strict === void 0 ) strict = false;

  var state = options.state; if ( state === void 0 ) state = {};
  if (typeof state === 'function') {
    state = state() || {};
  }

  // store internal state
  this._committing = false;
  this._actions = Object.create(null);
  this._actionSubscribers = [];
  this._mutations = Object.create(null);
  this._wrappedGetters = Object.create(null);
  this._modules = new ModuleCollection(options);
  this._modulesNamespaceMap = Object.create(null);
  this._subscribers = [];
  this._watcherVM = new Vue();

  // bind commit and dispatch to self
  var store = this;
  var ref = this;
  var dispatch = ref.dispatch;
  var commit = ref.commit;
  this.dispatch = function boundDispatch (type, payload) {
    return dispatch.call(store, type, payload)
  };
  this.commit = function boundCommit (type, payload, options) {
    return commit.call(store, type, payload, options)
  };

  // strict mode
  this.strict = strict;

  // init root module.
  // this also recursively registers all sub-modules
  // and collects all module getters inside this._wrappedGetters
  installModule(this, state, [], this._modules.root);

  // initialize the store vm, which is responsible for the reactivity
  // (also registers _wrappedGetters as computed properties)
  resetStoreVM(this, state);

  // apply plugins
  plugins.forEach(function (plugin) { return plugin(this$1); });

  if (Vue.config.devtools) {
    devtoolPlugin(this);
  }
};

var prototypeAccessors = { state: { configurable: true } };

prototypeAccessors.state.get = function () {
  return this._vm._data.$$state
};

prototypeAccessors.state.set = function (v) {
  if (process.env.NODE_ENV !== 'production') {
    assert(false, "Use store.replaceState() to explicit replace store state.");
  }
};

Store.prototype.commit = function commit (_type, _payload, _options) {
    var this$1 = this;

  // check object-style commit
  var ref = unifyObjectStyle(_type, _payload, _options);
    var type = ref.type;
    var payload = ref.payload;
    var options = ref.options;

  var mutation = { type: type, payload: payload };
  var entry = this._mutations[type];
  if (!entry) {
    if (process.env.NODE_ENV !== 'production') {
      console.error(("[vuex] unknown mutation type: " + type));
    }
    return
  }
  this._withCommit(function () {
    entry.forEach(function commitIterator (handler) {
      handler(payload);
    });
  });
  this._subscribers.forEach(function (sub) { return sub(mutation, this$1.state); });

  if (
    process.env.NODE_ENV !== 'production' &&
    options && options.silent
  ) {
    console.warn(
      "[vuex] mutation type: " + type + ". Silent option has been removed. " +
      'Use the filter functionality in the vue-devtools'
    );
  }
};

Store.prototype.dispatch = function dispatch (_type, _payload) {
    var this$1 = this;

  // check object-style dispatch
  var ref = unifyObjectStyle(_type, _payload);
    var type = ref.type;
    var payload = ref.payload;

  var action = { type: type, payload: payload };
  var entry = this._actions[type];
  if (!entry) {
    if (process.env.NODE_ENV !== 'production') {
      console.error(("[vuex] unknown action type: " + type));
    }
    return
  }

  this._actionSubscribers.forEach(function (sub) { return sub(action, this$1.state); });

  return entry.length > 1
    ? Promise.all(entry.map(function (handler) { return handler(payload); }))
    : entry[0](payload)
};

Store.prototype.subscribe = function subscribe (fn) {
  return genericSubscribe(fn, this._subscribers)
};

Store.prototype.subscribeAction = function subscribeAction (fn) {
  return genericSubscribe(fn, this._actionSubscribers)
};

Store.prototype.watch = function watch (getter, cb, options) {
    var this$1 = this;

  if (process.env.NODE_ENV !== 'production') {
    assert(typeof getter === 'function', "store.watch only accepts a function.");
  }
  return this._watcherVM.$watch(function () { return getter(this$1.state, this$1.getters); }, cb, options)
};

Store.prototype.replaceState = function replaceState (state) {
    var this$1 = this;

  this._withCommit(function () {
    this$1._vm._data.$$state = state;
  });
};

Store.prototype.registerModule = function registerModule (path, rawModule, options) {
    if ( options === void 0 ) options = {};

  if (typeof path === 'string') { path = [path]; }

  if (process.env.NODE_ENV !== 'production') {
    assert(Array.isArray(path), "module path must be a string or an Array.");
    assert(path.length > 0, 'cannot register the root module by using registerModule.');
  }

  this._modules.register(path, rawModule);
  installModule(this, this.state, path, this._modules.get(path), options.preserveState);
  // reset store to update getters...
  resetStoreVM(this, this.state);
};

Store.prototype.unregisterModule = function unregisterModule (path) {
    var this$1 = this;

  if (typeof path === 'string') { path = [path]; }

  if (process.env.NODE_ENV !== 'production') {
    assert(Array.isArray(path), "module path must be a string or an Array.");
  }

  this._modules.unregister(path);
  this._withCommit(function () {
    var parentState = getNestedState(this$1.state, path.slice(0, -1));
    Vue.delete(parentState, path[path.length - 1]);
  });
  resetStore(this);
};

Store.prototype.hotUpdate = function hotUpdate (newOptions) {
  this._modules.update(newOptions);
  resetStore(this, true);
};

Store.prototype._withCommit = function _withCommit (fn) {
  var committing = this._committing;
  this._committing = true;
  fn();
  this._committing = committing;
};

Object.defineProperties( Store.prototype, prototypeAccessors );

function genericSubscribe (fn, subs) {
  if (subs.indexOf(fn) < 0) {
    subs.push(fn);
  }
  return function () {
    var i = subs.indexOf(fn);
    if (i > -1) {
      subs.splice(i, 1);
    }
  }
}

function resetStore (store, hot) {
  store._actions = Object.create(null);
  store._mutations = Object.create(null);
  store._wrappedGetters = Object.create(null);
  store._modulesNamespaceMap = Object.create(null);
  var state = store.state;
  // init all modules
  installModule(store, state, [], store._modules.root, true);
  // reset vm
  resetStoreVM(store, state, hot);
}

function resetStoreVM (store, state, hot) {
  var oldVm = store._vm;

  // bind store public getters
  store.getters = {};
  var wrappedGetters = store._wrappedGetters;
  var computed = {};
  forEachValue(wrappedGetters, function (fn, key) {
    // use computed to leverage its lazy-caching mechanism
    computed[key] = function () { return fn(store); };
    Object.defineProperty(store.getters, key, {
      get: function () { return store._vm[key]; },
      enumerable: true // for local getters
    });
  });

  // use a Vue instance to store the state tree
  // suppress warnings just in case the user has added
  // some funky global mixins
  var silent = Vue.config.silent;
  Vue.config.silent = true;
  store._vm = new Vue({
    data: {
      $$state: state
    },
    computed: computed
  });
  Vue.config.silent = silent;

  // enable strict mode for new vm
  if (store.strict) {
    enableStrictMode(store);
  }

  if (oldVm) {
    if (hot) {
      // dispatch changes in all subscribed watchers
      // to force getter re-evaluation for hot reloading.
      store._withCommit(function () {
        oldVm._data.$$state = null;
      });
    }
    Vue.nextTick(function () { return oldVm.$destroy(); });
  }
}

function installModule (store, rootState, path, module, hot) {
  var isRoot = !path.length;
  var namespace = store._modules.getNamespace(path);

  // register in namespace map
  if (module.namespaced) {
    store._modulesNamespaceMap[namespace] = module;
  }

  // set state
  if (!isRoot && !hot) {
    var parentState = getNestedState(rootState, path.slice(0, -1));
    var moduleName = path[path.length - 1];
    store._withCommit(function () {
      Vue.set(parentState, moduleName, module.state);
    });
  }

  var local = module.context = makeLocalContext(store, namespace, path);

  module.forEachMutation(function (mutation, key) {
    var namespacedType = namespace + key;
    registerMutation(store, namespacedType, mutation, local);
  });

  module.forEachAction(function (action, key) {
    var type = action.root ? key : namespace + key;
    var handler = action.handler || action;
    registerAction(store, type, handler, local);
  });

  module.forEachGetter(function (getter, key) {
    var namespacedType = namespace + key;
    registerGetter(store, namespacedType, getter, local);
  });

  module.forEachChild(function (child, key) {
    installModule(store, rootState, path.concat(key), child, hot);
  });
}

/**
 * make localized dispatch, commit, getters and state
 * if there is no namespace, just use root ones
 */
function makeLocalContext (store, namespace, path) {
  var noNamespace = namespace === '';

  var local = {
    dispatch: noNamespace ? store.dispatch : function (_type, _payload, _options) {
      var args = unifyObjectStyle(_type, _payload, _options);
      var payload = args.payload;
      var options = args.options;
      var type = args.type;

      if (!options || !options.root) {
        type = namespace + type;
        if (process.env.NODE_ENV !== 'production' && !store._actions[type]) {
          console.error(("[vuex] unknown local action type: " + (args.type) + ", global type: " + type));
          return
        }
      }

      return store.dispatch(type, payload)
    },

    commit: noNamespace ? store.commit : function (_type, _payload, _options) {
      var args = unifyObjectStyle(_type, _payload, _options);
      var payload = args.payload;
      var options = args.options;
      var type = args.type;

      if (!options || !options.root) {
        type = namespace + type;
        if (process.env.NODE_ENV !== 'production' && !store._mutations[type]) {
          console.error(("[vuex] unknown local mutation type: " + (args.type) + ", global type: " + type));
          return
        }
      }

      store.commit(type, payload, options);
    }
  };

  // getters and state object must be gotten lazily
  // because they will be changed by vm update
  Object.defineProperties(local, {
    getters: {
      get: noNamespace
        ? function () { return store.getters; }
        : function () { return makeLocalGetters(store, namespace); }
    },
    state: {
      get: function () { return getNestedState(store.state, path); }
    }
  });

  return local
}

function makeLocalGetters (store, namespace) {
  var gettersProxy = {};

  var splitPos = namespace.length;
  Object.keys(store.getters).forEach(function (type) {
    // skip if the target getter is not match this namespace
    if (type.slice(0, splitPos) !== namespace) { return }

    // extract local getter type
    var localType = type.slice(splitPos);

    // Add a port to the getters proxy.
    // Define as getter property because
    // we do not want to evaluate the getters in this time.
    Object.defineProperty(gettersProxy, localType, {
      get: function () { return store.getters[type]; },
      enumerable: true
    });
  });

  return gettersProxy
}

function registerMutation (store, type, handler, local) {
  var entry = store._mutations[type] || (store._mutations[type] = []);
  entry.push(function wrappedMutationHandler (payload) {
    handler.call(store, local.state, payload);
  });
}

function registerAction (store, type, handler, local) {
  var entry = store._actions[type] || (store._actions[type] = []);
  entry.push(function wrappedActionHandler (payload, cb) {
    var res = handler.call(store, {
      dispatch: local.dispatch,
      commit: local.commit,
      getters: local.getters,
      state: local.state,
      rootGetters: store.getters,
      rootState: store.state
    }, payload, cb);
    if (!isPromise(res)) {
      res = Promise.resolve(res);
    }
    if (store._devtoolHook) {
      return res.catch(function (err) {
        store._devtoolHook.emit('vuex:error', err);
        throw err
      })
    } else {
      return res
    }
  });
}

function registerGetter (store, type, rawGetter, local) {
  if (store._wrappedGetters[type]) {
    if (process.env.NODE_ENV !== 'production') {
      console.error(("[vuex] duplicate getter key: " + type));
    }
    return
  }
  store._wrappedGetters[type] = function wrappedGetter (store) {
    return rawGetter(
      local.state, // local state
      local.getters, // local getters
      store.state, // root state
      store.getters // root getters
    )
  };
}

function enableStrictMode (store) {
  store._vm.$watch(function () { return this._data.$$state }, function () {
    if (process.env.NODE_ENV !== 'production') {
      assert(store._committing, "Do not mutate vuex store state outside mutation handlers.");
    }
  }, { deep: true, sync: true });
}

function getNestedState (state, path) {
  return path.length
    ? path.reduce(function (state, key) { return state[key]; }, state)
    : state
}

function unifyObjectStyle (type, payload, options) {
  if (isObject(type) && type.type) {
    options = payload;
    payload = type;
    type = type.type;
  }

  if (process.env.NODE_ENV !== 'production') {
    assert(typeof type === 'string', ("Expects string as the type, but found " + (typeof type) + "."));
  }

  return { type: type, payload: payload, options: options }
}

function install (_Vue) {
  if (Vue && _Vue === Vue) {
    if (process.env.NODE_ENV !== 'production') {
      console.error(
        '[vuex] already installed. Vue.use(Vuex) should be called only once.'
      );
    }
    return
  }
  Vue = _Vue;
  applyMixin(Vue);
}

var mapState = normalizeNamespace(function (namespace, states) {
  var res = {};
  normalizeMap(states).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedState () {
      var state = this.$store.state;
      var getters = this.$store.getters;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapState', namespace);
        if (!module) {
          return
        }
        state = module.context.state;
        getters = module.context.getters;
      }
      return typeof val === 'function'
        ? val.call(this, state, getters)
        : state[val]
    };
    // mark vuex getter for devtools
    res[key].vuex = true;
  });
  return res
});

var mapMutations = normalizeNamespace(function (namespace, mutations) {
  var res = {};
  normalizeMap(mutations).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedMutation () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      var commit = this.$store.commit;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapMutations', namespace);
        if (!module) {
          return
        }
        commit = module.context.commit;
      }
      return typeof val === 'function'
        ? val.apply(this, [commit].concat(args))
        : commit.apply(this.$store, [val].concat(args))
    };
  });
  return res
});

var mapGetters = normalizeNamespace(function (namespace, getters) {
  var res = {};
  normalizeMap(getters).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    val = namespace + val;
    res[key] = function mappedGetter () {
      if (namespace && !getModuleByNamespace(this.$store, 'mapGetters', namespace)) {
        return
      }
      if (process.env.NODE_ENV !== 'production' && !(val in this.$store.getters)) {
        console.error(("[vuex] unknown getter: " + val));
        return
      }
      return this.$store.getters[val]
    };
    // mark vuex getter for devtools
    res[key].vuex = true;
  });
  return res
});

var mapActions = normalizeNamespace(function (namespace, actions) {
  var res = {};
  normalizeMap(actions).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedAction () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      var dispatch = this.$store.dispatch;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapActions', namespace);
        if (!module) {
          return
        }
        dispatch = module.context.dispatch;
      }
      return typeof val === 'function'
        ? val.apply(this, [dispatch].concat(args))
        : dispatch.apply(this.$store, [val].concat(args))
    };
  });
  return res
});

var createNamespacedHelpers = function (namespace) { return ({
  mapState: mapState.bind(null, namespace),
  mapGetters: mapGetters.bind(null, namespace),
  mapMutations: mapMutations.bind(null, namespace),
  mapActions: mapActions.bind(null, namespace)
}); };

function normalizeMap (map) {
  return Array.isArray(map)
    ? map.map(function (key) { return ({ key: key, val: key }); })
    : Object.keys(map).map(function (key) { return ({ key: key, val: map[key] }); })
}

function normalizeNamespace (fn) {
  return function (namespace, map) {
    if (typeof namespace !== 'string') {
      map = namespace;
      namespace = '';
    } else if (namespace.charAt(namespace.length - 1) !== '/') {
      namespace += '/';
    }
    return fn(namespace, map)
  }
}

function getModuleByNamespace (store, helper, namespace) {
  var module = store._modulesNamespaceMap[namespace];
  if (process.env.NODE_ENV !== 'production' && !module) {
    console.error(("[vuex] module namespace not found in " + helper + "(): " + namespace));
  }
  return module
}

var index_esm = {
  Store: Store,
  install: install,
  version: '3.0.1',
  mapState: mapState,
  mapMutations: mapMutations,
  mapGetters: mapGetters,
  mapActions: mapActions,
  createNamespacedHelpers: createNamespacedHelpers
};


/* harmony default export */ __webpack_exports__["default"] = (index_esm);

/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(3)))

/***/ }),
/* 7 */,
/* 8 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 9 */,
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getElement = (function (fn) {
	var memo = {};

	return function(selector) {
		if (typeof memo[selector] === "undefined") {
			memo[selector] = fn.call(this, selector);
		}

		return memo[selector]
	};
})(function (target) {
	return document.querySelector(target)
});

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(33);

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton) options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
	if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else {
		throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	options.attrs.type = "text/css";

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	options.attrs.type = "text/css";
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */,
/* 121 */,
/* 122 */,
/* 123 */,
/* 124 */,
/* 125 */,
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */,
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */,
/* 134 */,
/* 135 */,
/* 136 */,
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */,
/* 141 */,
/* 142 */,
/* 143 */,
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */,
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */,
/* 187 */,
/* 188 */,
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */,
/* 194 */,
/* 195 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__state__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_entity_src_assets_app_css__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_entity_src_assets_app_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_entity_src_assets_app_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_entity_src_assets_menu_css__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_entity_src_assets_menu_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_entity_src_assets_menu_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_entity_src_assets_vbar_css__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_entity_src_assets_vbar_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_entity_src_assets_vbar_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_entity_src_assets_components_css__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_entity_src_assets_components_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_entity_src_assets_components_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__packages_app_index_js__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__packages_main_menu_index_js__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__packages_column_spec_index_js__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__packages_content_spec_index_js__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__packages_layout_row_index_js__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__packages_layout_tab_index_js__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__packages_layout_editor_index_js__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__packages_layout_service_editor_index_js__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__packages_text_field_index_js__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__packages_date_field_index_js__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__packages_number_field_index_js__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__packages_select_field_index_js__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__packages_transfer_field_index_js__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__packages_list_field_index_js__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__packages_check_field_index_js__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__packages_switch_field_index_js__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__packages_view_type_form_index_js__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__packages_view_type_form_collapse_index_js__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__packages_view_type_table_index_js__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__Main_vue__ = __webpack_require__(287);
Vue = window.Vue;
Vuex = window.Vuex;


const store = new Vuex.Store({
    state: __WEBPACK_IMPORTED_MODULE_0__state__["c" /* state */],
    mutations: __WEBPACK_IMPORTED_MODULE_0__state__["b" /* mutations */],
    getters: __WEBPACK_IMPORTED_MODULE_0__state__["a" /* getters */]
})

/*styles*/





/*components*/



















//import Diagram from '../packages/diagram/index.js';


const entityComponents = [
//  Diagram,
  __WEBPACK_IMPORTED_MODULE_13__packages_text_field_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_14__packages_date_field_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_15__packages_number_field_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_17__packages_transfer_field_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_16__packages_select_field_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_19__packages_check_field_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_20__packages_switch_field_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_18__packages_list_field_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_21__packages_view_type_form_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_22__packages_view_type_form_collapse_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_23__packages_view_type_table_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_12__packages_layout_service_editor_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_11__packages_layout_editor_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_10__packages_layout_tab_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_9__packages_layout_row_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_7__packages_column_spec_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_8__packages_content_spec_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_6__packages_main_menu_index_js__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_5__packages_app_index_js__["a" /* default */]
];

const installEntity = function(Vue) {
  entityComponents.map(function(component) {
    Vue.component(component.name, component);
  });
};


if (typeof window !== 'undefined' && window.Vue) {
  installEntity(window.Vue);
};

/**/
Vue.use(__WEBPACK_IMPORTED_MODULE_5__packages_app_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_7__packages_column_spec_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_8__packages_content_spec_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_6__packages_main_menu_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_23__packages_view_type_table_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_13__packages_text_field_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_15__packages_number_field_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_17__packages_transfer_field_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_16__packages_select_field_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_18__packages_list_field_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_19__packages_check_field_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_20__packages_switch_field_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_21__packages_view_type_form_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_22__packages_view_type_form_collapse_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_9__packages_layout_row_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_10__packages_layout_tab_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_11__packages_layout_editor_index_js__["a" /* default */]);
Vue.use(__WEBPACK_IMPORTED_MODULE_12__packages_layout_service_editor_index_js__["a" /* default */]);
//Vue.use(Diagram);



var app = new Vue({
  el: '#main',
  store: store,
  render: function (createElement) {
    return createElement(__WEBPACK_IMPORTED_MODULE_24__Main_vue__["a" /* default */], this.$slots.default);
  }
});

function Resize(){
	//var navH = document.getElementById('vastrastNav').getBoundingClientRect();
  //document.getElementById('appContent').style.height= (window.innerHeight-navH.height-navH.top).toString()+'px';
  var header = document.getElementById('globalHeader').clientHeight;
  var newSize = (window.innerHeight-header+1).toString()+'px';
  var newSizeIn = (window.innerHeight-header).toString()+'px';
  document.getElementById('globalAside').style.height= newSize;
  document.getElementById('globalContainer').style.height= newSize;

  //document.getElementById('mainMenu').style.height= newSizeIn;
  //document.getElementById('container').style.height= newSize;
  //document.getElementById('app').style.height= newSize;

  //app.forceShowMenu();
  //app.resizeApp();
}

window.onresize=Resize;
window.onload=Resize;


/***/ }),
/* 196 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//inicializamos el estado de los todos
const state = {
    content: {
      viewType: '',
      layoutUrl: [],
      icon: '',
      layoutDialogUrl: '',
      showLayoutDialog: false,
      activeLayoutUrl: ''
    },
    layoutDialog: {
      url: '',
      title: '',
      visible: false,
      preventClose: false,
      extraParams: {},
      instance: null,
      resetParams: {
        data: false,
        spec: false,
        layout: false
      }
    }
}
/* harmony export (immutable) */ __webpack_exports__["c"] = state;



const getters = {
  baseUrl: function() {
    //return 'http://localhost/entity/'
    return location.protocol + "//" + location.host + '//';
  },
  activeLayoutUrl: function() {
    return state.content.activeLayoutUrl
  },
  activeLayoutLoading: function() {
    var selObj = state.content.layoutUrl.filter(function( obj ) {
      return obj.url == state.content.activeLayoutUrl;
    });
    if (selObj.length > 0) {
      return selObj[0].loading
    }
    return false; //true
  },
  headerLabel: function() {
    var selObj = state.content.layoutUrl.filter(function( obj ) {
      return obj.url == state.content.activeLayoutUrl;
    });
    var label = ''
    if (selObj.length > 0) {
      label = selObj[0].header.label
    }
    return (label.length > 28) ? label.substring(0,25)+'...' : label;
  },
  layouts: function() {
    return state.content.layoutUrl
  }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = getters;



const mutations = {
    setLayoutUrl: function(state, url) {
      state.layoutDialog.visible = false;
      state.content.activeLayoutUrl = url;
      var selObj = state.content.layoutUrl.filter(function( obj ) {
        return obj.url == url;
      });
      if (selObj.length == 0) {
        var tab = {
          header: {
            label:''
          },
          data: null,
          url: url,
          isStandard: false,
          loading: true,
          resetParams: {
            data: false,
            spec: false,
            layout: false
          }
        }
        state.content.layoutUrl.push(tab)
      }
    },
    setLayoutUrlStandard: function(state, url) {
      state.content.activeLayoutUrl = url;
      var selObj = state.content.layoutUrl.filter(function( obj ) {
        return obj.url == url;
      });
      if (selObj.length == 0) {
        var tab = {
          header: {
            label:''
          },
          data: null,
          url: url,
          isStandard: true,
          loading: true,
          resetParams: {
            data: false,
            spec: false,
            layout: false
          }
        }
        state.content.layoutUrl.push(tab)
      } else {
        selObj[0].resetParams.data = true;
        selObj[0].resetParams.spec = true;
      }
    },
    removeLayout: function(state, index) {
      if (state.content.activeLayoutUrl == state.content.layoutUrl[index].url){
        state.content.activeLayoutUrl = '';
      }
      state.content.layoutUrl.splice(index, 1);
    },
    setLayoutHeader: function(state, tab) {
      state.content.layoutUrl[tab.index].header = tab.header;
    },
    setLayoutDataHeader: function(state, tab) {
      if (state.content.layoutUrl[tab.index].header.format) {
        var label = state.content.layoutUrl[tab.index].header.format
        for(var key in tab.data){
          label = label.replace('{'+key+'}', tab.data[key])
        }
        state.content.layoutUrl[tab.index].header.label = label
      }
    },
    setLayoutLoading: function(state, tab) {
      if (state.content.layoutUrl[tab.index].loading) {
        state.content.layoutUrl[tab.index].loading = tab.loading;
      }
    },
    setIcon: function(state, icon) {
      state.content.icon = icon
    },
    setLayoutDialogUrl: function(state, layout) {
      state.layoutDialog.url = layout.url;
      state.layoutDialog.title = layout.title;
      state.layoutDialog.preventClose = layout.preventClose;
      if ('extraParams' in layout) {
        state.layoutDialog.extraParams = layout.extraParams;
      } else {
        state.layoutDialog.extraParams = {}
      }
      if (layout.instance) {
        state.layoutDialog.instance = layout.instance
      } else {
        state.layoutDialog.instance = null
      }
      state.layoutDialog.visible = true;
    },
    setLayoutDialogVisibility: function(state, visible) {
      state.layoutDialog.visible = visible;
      if (!visible) {
        if (state.layoutDialog.extraParams.resetOnClose) {
          if (state.layoutDialog.instance) {
            state.layoutDialog.instance.resetData();
          }
        };
        state.layoutDialog.url = '';
        state.layoutDialog.title = '';
        state.layoutDialog.extraParams = {};
        state.layoutDialog.instance = null;
      }
    }
}
/* harmony export (immutable) */ __webpack_exports__["b"] = mutations;


/***/ }),
/* 197 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(198);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../css-loader/index.js!../../../postcss-loader/lib/index.js!./app.css", function() {
			var newContent = require("!!../../../css-loader/index.js!../../../postcss-loader/lib/index.js!./app.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 198 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(undefined);
// imports


// module
exports.push([module.i, ".fadeLoader-leave-active {\n  transition: opacity .1s;\n}\n.fadeLoader-enter, .fadeLoader-leave-to {\n  opacity: 0;\n}\n\n.fadeSection-enter-active, .fadeSection-leave-active {\n  transition: opacity 1.2s;\n}\n.fadeSection-enter, .fadeSection-leave-to {\n  opacity: 0;\n}\n\n.fadeSectionRefresh-enter-active {\n  transition: opacity .5s;\n}\n\n.fadeSectionRefresh-leave-active {\n  transition: opacity 0s;\n}\n\n.fadeSectionRefresh-enter, .fadeSectionRefresh-leave-to {\n  opacity: 0;\n  position: fixed;\n}\n\n.fade2-enter-to {\n}\n\nbody {\n  background-color: #dadada;\n  overflow: hidden;\n  padding: 0 !important;\n  margin: 0;\n}\n\n#menuButton{\n\tdisplay: none;\n}\n\n#app{\n  text-align: left !important;\n}\n\n.el-main {\n  z-index: 0;\n  padding: 0px;\n}\n\n.container {\n    width: 100%;\n    max-width: 100%;\n    padding-right: 0;\n    padding-left: 0;\n    padding-right: 25px;\n}\n\n\n.el-header {\n    height: 50px !important;\n    width: 100%;\n    background-color: #363b3e;\n    z-index: 1001;\n    -webkit-box-shadow: 4px 2px 18px -3px rgba(0,0,0,0.75);\n    -moz-box-shadow: 4px 2px 18px -3px rgba(0,0,0,0.75);\n    box-shadow: 4px 2px 18px -3px rgba(0,0,0,0.75);\n    color: #991d44;\n    padding: 0 5px;\n    position: relative;\n}\n\n.el-header .el-select {\n    position: absolute;\n    top: 9px;\n    left: 76px;\n    font-size: 35px !important;\n}\n\n.el-header-options .el-input__inner {\n    font-size: 24px;\n    padding-bottom: 3px;\n    height: 40px;\n}\n\n#menuIcon {\n    position: relative;\n    display: inline-block;\n    font-size: 21px;\n    width: 60px;\n    text-align: center;\n    cursor: pointer;\n    left: -25px;\n    height: 100%;\n    top: 0px;\n    -webkit-box-sizing: content-box;\n    -moz-box-sizing: content-box;\n    box-sizing: content-box;\n    float: none;\n    z-index: auto;\n    opacity: 1;\n    margin: 0;\n    padding: 0;\n    overflow: visible;\n    border: none;\n    -webkit-border-radius: 0;\n    border-radius: 0;\n    color: rgb(252, 252, 252);\n    -o-text-overflow: clip;\n    text-overflow: clip;\n    background: #a8003b;\n    -webkit-box-shadow: none;\n    box-shadow: none;\n    text-shadow: none;\n    -webkit-transition: none;\n    -moz-transition: none;\n    -o-transition: none;\n    transition: none;\n    -webkit-transform: skewX(-20deg);\n    transform: skewX(-20deg);\n    -webkit-transform-origin: 50% 50% 0;\n    transform-origin: 50% 50% 0;\n    padding-left: 16px;\n}\n\n#menuIcon i {\n  margin-top: 6px;\n  transform: skewX(20deg);\n}\n\n#headerIcon {\n  position: fixed;\n  display: flex;\n  padding-top: 0px;\n  font-size: 64px;\n  border-radius: 34px;\n  width: 24px;\n  margin-top: 0px;\n  text-align: center;\n  color: #454545;\n  padding-left: 4px;\n  z-index: -1;\n  top: -21px;\n  left: 33px;\n}\n\n#headerLabel {\n    font-size: 1.1em;\n    -webkit-box-sizing: content-box;\n    -moz-box-sizing: content-box;\n    box-sizing: content-box;\n    float: none;\n    z-index: auto;\n    position: absolute;\n    cursor: default;\n    opacity: 1;\n    margin: 0;\n    padding: 0;\n        padding-right: 0px;\n        padding-left: 0px;\n    overflow: visible;\n    border: none;\n    -webkit-border-radius: 0;\n    border-radius: 0;\n    color: rgb(240, 222, 222);\n    -o-text-overflow: clip;\n    text-overflow: clip;\n    background: #2a3231;\n    -webkit-box-shadow: none;\n    box-shadow: none;\n    text-shadow: none;\n    -webkit-transition: none;\n    -moz-transition: none;\n    -o-transition: none;\n    transition: none;\n    -webkit-transform: skewX(-20deg);\n    transform: skewX(-20deg);\n    -webkit-transform-origin: 50% 50% 0;\n    transform-origin: 50% 50% 0;\n    display: inline-block;\n    top: 0px;\n    height: 100%;\n    vertical-align: middle;\n    left: -30px;\n    padding-left: 14px;\n    padding-right: 21px;\n    float: right;\n    right: -15px;\n    left: inherit;\n}\n\n#headerLabel i{\n    margin-top: 0.6em;\n    display: block;\n    transform: skewX(30deg);\n    position: relative;\n}\n\n#appContent {\n  display: block;\n  overflow-y: hidden;\n  margin-left: inherit;\n  padding-top: 5px;\n  padding-bottom: 15px;\n  padding-left: 3px;\n  padding-right: 0px;\n  width: 100% !important;\n}\n\n.cleanerContent {\n\tclear:both;\n\tmargin-top: 15px;\n}\n\n#colAppContent {\n    padding-left: 0px;\n    padding-right: 3px;\n}\n\n#globalAside {\n  z-index: 1000;\n  -webkit-box-shadow: 4px 0px 7px 0px rgba(0,0,0,0.52);\n  -moz-box-shadow: 4px 0px 7px 0px rgba(0,0,0,0.52);\n  box-shadow: 1px 0px 7px 0px rgba(0,0,0,0.52);\n  background-color: #fdfdfd;\n}\n\n.disable-select {\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n\n.dialogLayout .el-dialog {\n  width: 50%;\n  max-height: 85vh;\n  overflow: auto;\n  margin-top: 7vh !important;\n}\n\n@media (max-width: 1200px) {\n  .dialogLayout .el-dialog {\n    width: 75%;\n  }\n}\n\n.dialogLayout .el-dialog .el-card {\n    border: none;\n    box-shadow: initial;\n}\n\n.dialogLayout .el-dialog .el-card .el-card__header {\n    display: none;\n}\n\n.dialogLayout .el-dialog .el-dialog__body {\n    padding: 5px;\n}\n\n.globalAppLoader {\n    width: 70px;\n    height: 70px;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    margin-left: -35px;\n    margin-top: -35px;\n    z-index: 900;\n}\n\n.globalAppLoaderLabel{\n  position: absolute;\n  font-size: 40px;\n  text-align: center;\n  height: 60px;\n  vertical-align: middle;\n  display: block;\n  width: 60px;\n  color: #871746;\n}\n\n.half-circle-spinner, .half-circle-spinner *\n{ box-sizing: border-box; }\n\n.half-circle-spinner {\n  width: 60px;\n  height: 60px;\n  border-radius: 100%;\n  position: relative;\n}\n.half-circle-spinner .circle {\n  content: \"\";\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  border-radius: 100%;\n  border: calc(30px / 10) solid transparent; }\n\n.half-circle-spinner .circle.circle-1 {\n  border-top-color: #a8003b;\n  animation: half-circle-spinner-animation 1s infinite;\n}\n\n.half-circle-spinner .circle.circle-2 {\n  border-bottom-color: #a8003b;\n  animation: half-circle-spinner-animation 1s infinite alternate;\n}\n\n@keyframes half-circle-spinner-animation {\n  0% { transform: rotate(0deg); }\n  100%{ transform: rotate(360deg); }\n}\n\n/* */\n@media (max-width: 768px) {\n   .bg-success {\n        display: block !important;\n   }\n\n   #appContent {\n      padding: 1px;\n      padding-right: 20px;\n   }\n\n   #colAppContent {\n      padding-left: 0px;\n      padding-right: 2px;\n  }\n\n  .dialogLayout .el-dialog {\n    width: 85%;\n  }\n}\n\n", ""]);

// exports


/***/ }),
/* 199 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(200);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../css-loader/index.js!../../../postcss-loader/lib/index.js!./menu.css", function() {
			var newContent = require("!!../../../css-loader/index.js!../../../postcss-loader/lib/index.js!./menu.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 200 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(undefined);
// imports


// module
exports.push([module.i, "#menu-row {\n  background-color: #fafafa;\n  border-right: 1px solid #ccc;\n\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n}\n\n#mainMenu{\n  width: 103%;\n  /*border-bottom: 1px solid #868f8e;*/\n  background-color: #fdfdfd;\n  height: auto;\n}\n\n.vatrastTitle {\n  font-size: 2.15rem;\n  margin: auto;\n  width: 100%;\n  display: block;\n  text-align: center;\n  z-index: 10000000;\n  color: #189c97 !important;\n  height: 60px;\n  border-bottom: 1px solid #ccc;\n  cursor: pointer;\n}\n\n#vatrastNav{\n    margin-left: -5px;\n    width: 100%;\n    background-color: #fafafa;\n    height: 60px;\n    color: #8d2065;\n    padding-left: 13px;\n    padding-right: 30px;\n    vertical-align: middle;\n    border-bottom: 1px solid #969696;\n    z-index: 100000000000000000;\n    position: fixed;\n}\n\n#sidebar {\n  padding: 0px;\n  z-index: 10000000;\n}\n\n.closeButtonItem {\n    font-size: 12px;\n    font-weight: normal;\n    position: absolute;\n    right: 10px;\n    top: 15px;\n    color: #bfbfb9;\n}\n\n.closeButtonItem:hover {\n  color: #c6380a;\n}\n\n#sidebarItems {\n  /*overflow-y: auto;*/\n  width: 100%;\n}\n\n/*Element UI Menu*/\n.el-menu {\n\tbackground-color: #223839;\n}\n\n.el-menu .el-menu-item, .el-menu .el-submenu__title {\n    color: #686662;\n    font-size: 12.5px;\n    padding-left: 10px !important;\n}\n\n.el-submenu__title {\n    color: #686662;\n}\n\n.el-menu .el-menu-item:hover,.el-menu .el-submenu__title:hover {\n\tbackground-color: transparent;\n  color: #68503c;\n  padding-left: 10px !important;\n}\n\n.el-menu .el-submenu .el-menu {\n  background-color: #f9f9f9;\n  color: #444;\n  border: 1px solid #dedede;\n}\n\n.el-submenu .el-menu .el-menu-item,.el-submenu .el-menu .el-submenu__title {\n  background-color: transparent;\n  color: #777e81;\n  padding-left: 17px !important;\n}\n\n.el-submenu .el-menu .el-menu-item:hover,.el-submenu .el-menu .el-submenu__title:hover {\n  color: #32635b;\n  background-color: #edf2f0;\n  padding-left: 17px !important;\n}\n\n.menuSearch {\n  float: right !important;\n  margin-right: 11px !important;\n  font-weight: 500;\n}\n\n.menuSearch:hover {\n  border-color: transparent !important;\n  background-color: transparent !important;\n}\n\n.sfa {\n  border-radius: 40px;\n  padding: 8px;\n  margin-right: 4px;\n}\n\n.subsfa {\n  border-radius: 40px;\n  padding: 8px;\n  color: #555;\n  margin-right: 4px;\n}\n\n.el-submenu__title .sfa {\n  font-size: 19px;\n  color: #a8003b;\n}\n\n.el-menu-item.is-active, .el-menu-item.is-active:hover, .el-menu-item.is-active i {\n  color: #600022 !important;\n  font-weight: 500;\n}\n\n.el-menu--horizontal .el-submenu > .el-menu {\n    position: absolute;\n    top: 65px;\n    left: 0;\n    border: 1px solid #d1dbe5;\n    padding: 5px 0;\n    background-color: rgb(253, 253, 253);\n    z-index: 100;\n    min-width: 100%;\n}\n\n.globalContainerFrame {\n  position: absolute;\n}\n\n.el-submenu.is-active .el-submenu__title {\n  color: #635d55;\n  font-weight: 600;\n}\n\n.el-menu-item-group__title {\n  padding-left: 17px !important;\n}\n\n.el-menu-item-group > ul > div {\n    padding-left: 20px;\n}\n\n@media (max-width: 768px) {\n\n  #sidebarItems {\n    position: absolute;\n    z-index: 10000;\n    overflow-y: auto;\n    width: 100%;\n   }\n\n  #menuButton{\n    display: inline;\n  }\n\n  .vatrastTitle {\n     text-align: left;\n     padding-left: 10px;\n     font-size: 1.35rem;\n     height: auto;\n   }\n}\n", ""]);

// exports


/***/ }),
/* 201 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(202);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../css-loader/index.js!../../../postcss-loader/lib/index.js!./vbar.css", function() {
			var newContent = require("!!../../../css-loader/index.js!../../../postcss-loader/lib/index.js!./vbar.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 202 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(undefined);
// imports


// module
exports.push([module.i, ".vb > .vb-dragger {\n    z-index: 1000000;\n    width: 12px;\n    right: -5px;\n}\n\n.vb > .vb-dragger > .vb-dragger-styler {\n    -webkit-backface-visibility: hidden;\n    backface-visibility: hidden;\n    -webkit-transform: rotate3d(0,0,0,0);\n    transform: rotate3d(0,0,0,0);\n    -webkit-transition:\n        background-color 100ms ease-out,\n        margin 100ms ease-out,\n        height 100ms ease-out;\n    transition:\n        background-color 100ms ease-out,\n        margin 100ms ease-out,\n        height 100ms ease-out;\n    /*background-color: rgba(24,156,151,.1);*/\n    margin: 5px 5px 5px 0;\n    border-radius: 10px;\n    height: calc(100% - 10px);\n    display: block;\n    z-index: 1000000000;\n}\n\n#globalAside .vb-dragger {\n    left: 3px;\n    right: 0;\n}\n\n.vb.vb-scrolling-phantom > .vb-dragger > .vb-dragger-styler {\n    background-color: rgba(24,156,151,.3);\n}\n\n.vb > .vb-dragger:hover > .vb-dragger-styler {\n    background-color: rgba(24,156,151,.5);\n    margin: 0px;\n    margin-right: 5px;\n    height: 100%;\n}\n\n.vb.vb-dragging > .vb-dragger > .vb-dragger-styler {\n    background-color: rgba(24,156,151,.5);\n    margin: 0px;\n    margin-right: 5px;\n    height: 100%;\n}\n\n.vb.vb-dragging-phantom > .vb-dragger > .vb-dragger-styler {\n    background-color: rgba(24,156,151,.5);\n}", ""]);

// exports


/***/ }),
/* 203 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(204);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../css-loader/index.js!../../../postcss-loader/lib/index.js!./components.css", function() {
			var newContent = require("!!../../../css-loader/index.js!../../../postcss-loader/lib/index.js!./components.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 204 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(undefined);
// imports


// module
exports.push([module.i, "/*CARD*/\n.el-card {\n    border: 1px solid #a5b6b7;\n    border-radius: 3px;\n    background-color: #fff;\n    overflow: hidden;\n    box-shadow: 0 2px 4px 0 rgba(0,0,0,.12),0 0 6px 0 rgba(0,0,0,.04);\n    margin-bottom: 3px;\n    padding-bottom: 5px;\n}\n\n.el-card__header {\n    padding: 10px 20px;\n    border-bottom: 1px solid #d1dbe5;\n    box-sizing: border-box;\n    color: #0d695a;\n}\n\n.el-card__header button {\n    float: right;\n}\n\n.el-card__body {\n    padding: 10px;\n}\n\n.specHeader .el-card__header {\n    background-color: #1b9c8e;\n    color: #eee;\n    font-size: 14px;\n}\n\n.specHeaderOptions .el-input__inner{\n \tcolor: #fafafa;\n}\n\n.specHeaderOptions .el-input__inner:focus {\n    background-color: transparent;\n    border: 0px !important;\n    border-bottom: 1px solid #fafafa !important;\n    color: #c1fdff;\n}\n\n.subOptions {\n    background-color: #fbfbfb !important;\n    border: 1px solid #ddd;\n}\n\n.deleteButton{\n    position: relative;\n    left: -10px;\n    padding: 5px !important;\n    color: #d8cfcf !important;\n    background-color: transparent !important;\n    margin-left: 100%;\n    font-size: 16px;\n    height: 24px;\n    position: absolute;\n    right: 24px;\n    margin-left: 98%;\n}\n\n.deleteButton:hover {\n    color: #ef640c !important;\n    /*background-color: transparent !important;*/\n}\n\n.el-form .deleteButton {\n    display: none\n}\n\n.el-form:hover .deleteButton {\n    display: inline;\n    top: -3px;\n}\n\n.formEntitySeparator {\n    border: 1px solid transparent;\n    border-bottom: 1px solid #eee;\n}\n\n.formEntitySeparator:hover {\n    border: 1px solid #dfdfdf;\n}\n/*tabs*/\n.el-tabs__item {\n    color: #316f66;\n}\n\n.el-tabs__item.is-active {\n    color: #1b8a82;\n}\n\n.el-tabs__active-bar {\n    background-color: #2ba79e;\n}\n\n.el-tabs .el-row .el-col .el-card__header {\n  display: none;\n}\n\n.serviceOptions .el-card__header {\n  display: initial !important;\n}\n\n.force_header .el-card__header {\n  display: block !important;\n}\n\n.el-tabs .el-row .el-col {\n  border: 0px;\n  box-shadow: unset;\n}\n\n.el-col {\n    padding-left: 0px !important;\n    padding-right: 4px !important;\n}\n\n/*buttons*/\n.el-button {\n    border-radius: 3px;\n}\n\n.buttonAction {\n    font-size: 15px;\n    padding-left: 0 !important;\n    padding-right: 0 !important;\n    margin-left: 0 !important;\n    margin-right: 0 !important;\n    display: initial;\n}\n\n.el-switch {\n    position: relative;\n    line-height: 14px;\n    height: 20px;\n    vertical-align: middle;\n    margin-left: 7px;\n}\n\n.el-switch.is-checked .el-switch__core {\n    color: #fff;\n    background-color: #2db1a6;\n    border-color: #65a498;\n}\n\n.el-button--primary, .el-button--primary:hover, .el-button--primary:active{\n\tcolor: #307474;\n\tbackground-color: #e6f8f6;\n\tborder-color: #afe4dc;\n}\n\n.el-button--primary:focus, .el-button--primary:hover {\n    background: #e6f8f6;\n    border-color: #2cbad1;\n    color: #158383;\n}\n\n.el-button--text, .el-button--text:active {\n    border: none;\n    color: #00799b;\n\tbackground: 0 0;\n\tpadding-left: 5px;\n\tpadding-right: 5px;\n}\n\n.el-button--text:focus, .el-button--text:hover {\n    background-color: #f5f5f5;\n    color: #00799b;\n}\n\n/*collapse*/\n.el-collapse-item__content {\n    font-size: 13px;\n    color: #1f2d3d;\n    line-height: 1.769230769230769;\n    background-color: #fcfcfc;\n}\n\n/*FORMS*/\n.el-form {\n    padding: 15px;\n    /*padding-top: 0px;*/\n    /*background-color: #f7fcfa;*/\n    margin-bottom: 3px;\n    /*border: 1px solid #c4dad7;*/\n    padding-bottom: 0;\n    position: relative;\n}\n\n.multiple-form{\n    padding: 15px;\n    /*padding-top: 0px;*/\n    background-color: #f7fcfa;\n    margin-bottom: 3px;\n    border: 1px solid #c4dad7;\n}\n\n.el-form-item__label {\n    color: #4e8c89;\n    text-align: left;\n    vertical-align: middle;\n    float: left;\n    font-size: 13px;\n    line-height: 1;\n    padding: 10px 4px 11px 0;\n    box-sizing: border-box;\n    font-size: 12px;\n}\n\n.el-form-item__content {\n    line-height: 31px;\n}\n\n.disabledForm {\n    background-color: #ffe9e9;\n    height: 10px;\n    display: flex;\n}\n\n.el-input-number {\n    line-height: 31px;\n}\n.el-input-number .el-input__inner {\n    text-align: left;\n    line-height: 31px;\n}\n\n.form-group {\n\tmargin-bottom: 10px;\n}\n\n.el-form-item {\n    margin-bottom: 5px;\n}\n\n.el-input__inner {\n\tborder-radius: 3px;\n\tborder: 1px solid transparent;\n\tpadding: 1px 1px;\n\tpadding: 2px 6px;\n\theight: 31px;\n\tborder-radius: 1px;\n    transition: none;\n    background-color: #f8f8f8;\n}\n\n.el-input__inner:hover {\n\tborder: 1px solid transparent;\n\ttransition: none;\n}\n\n.el-input__inner:focus {\n\tbackground-color: #e8fbfa;\n\tcolor: #2a2a2a;\n\tborder: 1px solid #cee5ec !important;\n\n}\n\n\n.el-textarea {\n    width: 70%;\n    font-size: 12.5px;\n}\n\n.noLabels .el-textarea {\n    width: 97%;\n}\n\n.el-textarea__inner {\n    border-radius: 3px;\n    border: 1px solid transparent;\n    padding: 1px 1px;\n    padding: 2px 6px;\n    border-radius: 1px;\n    transition: none;\n    background-color: #f8f8f8;\n    font-family: Cantarel;\n    font-size: 12.5px;\n    min-height: 80px;\n}\n\n.el-textarea__inner:hover {\n    border: 1px solid transparent;\n    transition: none;\n}\n\n.el-textarea__inner:focus {\n    background-color: #e8fbfa;\n    color: #2a2a2a;\n    border: 1px solid #cee5ec !important;\n\n}\n\n\n\n.el-select:hover .el-input__inner {\n    border: 1px solid transparent;\n    transition: none;\n}\n\n.el-select-dropdown {\n    border: 1px solid #7f938e;\n}\n\n.el-select-dropdown__item.selected {\n    color: #e74c04;\n}\n\n.el-select-dropdown__item.hover, .el-select-dropdown__item:hover {\n    background-color: #e8fbfa;\n}\n\n.el-popper[x-placement^=\"bottom\"] .popper__arrow {\n    border-bottom-color: #7f938e;\n}\n\n.el-transfer-panel .el-transfer-panel__header .el-checkbox {\n    display: block;\n    line-height: 40px;\n    width: calc(100% - 50px);\n}\n\n.el-transfer-panel .el-transfer-panel__header .el-checkbox .el-checkbox__label span {\n    position: initial;\n    right: 15px;\n    color: #909399;\n    font-size: 12px;\n    font-weight: 400;\n    display: contents;\n}\n\n.actions-icon, .actions-icon:hover {\n\tfloat: right;\n\tborder-radius: 50px;\n\theight: 32px;\n\twidth: 32px;\n\tpadding: 0;\n\tborder: 1px solid transparent;\n\tcolor: #ef7e12;\n\tfont-size: 24px !important;\n}\n\n/**/\n.el-row {\n\tmargin-bottom: 5px;\n    margin-left: 2px !important;\n    margin-right: 2px !important;\n}\n\nlabel {\n\tcolor: #5f1137;\n\twidth: 25%;\n}\n\n.el-transfer-panel__list label {\n    width: 90%;\n}\n\n.text-field .el-input {\n\twidth: 70%;\n    font-size: 12.5px;\n}\n\n.noLabels .text-field .el-input {\n    width: 97%;\n}\n\n.el-button-group {\n    right: 0;\n    position: inherit;\n    margin: 8px;\n}\n\n.el-card__header .options {\n    float: right;\n    position: relative;\n}\n\n.el-card__header .icon {\n    margin-right: 0;\n    padding-right: 4px;\n    margin-left: 0;\n    padding-left: 0;\n}\n\n/*LOADING*/\n.el-loading-parent--relative {\n    min-height: 40px;\n}\n\n.el-loading-mask {\n    position: absolute;\n    z-index: 0;\n    background-color: rgba(255,255,255,.9);\n    margin: 0;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    left: 0;\n    -webkit-transition: opacity .3s;\n    transition: opacity .3s;\n}\n\n.el-loading-spinner {\n    top: 50%;\n    margin-top: -11px;\n    width: 100%;\n    text-align: center;\n    position: absolute;\n}\n\n.el-loading-spinner .circular {\n    height: 24px;\n    width: 24px;\n    -webkit-animation: loading-rotate 1s linear infinite;\n    animation: loading-rotate 1s linear infinite;\n}\n\n.el-loading-spinner .path {\n    -webkit-animation: loading-dash 1.5s ease-in-out infinite;\n    animation: loading-dash 1.5s ease-in-out infinite;\n    stroke-dasharray: 90,150;\n    stroke-dashoffset: 0;\n    stroke-width: 2;\n    stroke: #B7073F;\n    stroke-linecap: round;\n}\n\n.el-popover {\n    position: absolute;\n    background: #fff;\n    min-width: 150px;\n    border-radius: 4px;\n    border: 1px solid #b3b3b3;\n    padding: 12px;\n    z-index: 2000;\n    color: #5a5e66;\n    line-height: 1.4;\n    text-align: justify;\n    word-break: break-all;\n    font-size: 14px;\n    box-shadow: 0 2px 20px 0 rgba(49, 59, 57, 0.8);\n}\n\n.el-popper[x-placement^=\"top\"] .popper__arrow {\n    bottom: -6px;\n    left: 50%;\n    margin-right: 3px;\n    border-top-color: #8f8f8f;\n    border-bottom-width: 0;\n}\n\n/*MEDIA QUERY*/\n@media (max-width: 768px) {\n\tlabel {\n\t\tcolor: #5f1137;\n\t\twidth: 95%;\n\t}\n\n    .el-col {\n        padding-left: 0px !important;\n        padding-right: 5px !important;\n    }\n\n\t.el-form-item__label {\n\t    text-align: left;\n\t}\n\n\t.text-field .el-input {\n\t\twidth: 95%;\n\t}\n\n\t.el-form-item {\n\t    margin-bottom: 5px;\n\t}\n\n\t.el-form-item__label {\n        padding-bottom: 7px;\n    }\n}", ""]);

// exports


/***/ }),
/* 205 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(206);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 206 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_f7863314_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(208);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_f7863314_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/app/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f7863314", Component.options)
  } else {
    hotAPI.reload("data-v-f7863314", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 207 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var _vuex = __webpack_require__(6);

exports.default = {
  name: 'App',

  props: ['urlMenu'],

  data: function data() {
    return {
      width: '190px',
      collapse: false,
      message: '',
      noMatchDataText: 'No hay coincidencias',
      showNav: true,
      desktopMode: false,
      blockContent: false,
      headerType: {
        label: 'label',
        options: 'options'
      },
      header: {}
    };
  },

  mounted: function mounted() {
    this.resizeApp();
  },

  watch: {
    showMenu: function showMenu(value) {
      this.showNav = value;
    },
    isCollapse: function isCollapse(value) {
      this.collapse = value;
    }
  },

  computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl', 'activeLayoutUrl', 'headerLabel', 'activeLayoutLoading']), {
    showMenu: function showMenu() {
      return this.showNav;
    },
    isCollapse: function isCollapse() {
      return this.collapse;
    },
    itemContent: function itemContent() {
      return this.$store.state.content;
    },
    layoutDialog: function layoutDialog() {
      return this.$store.state.layoutDialog;
    }
  }),

  methods: _extends({}, (0, _vuex.mapMutations)(['setLayoutUrl', 'setIcon', 'setLayoutDialogUrl', 'setLayoutDialogVisibility', 'setLayoutHeader', 'setLayoutDataHeader', 'setLayoutLoading']), {

    toogleMenu: function toogleMenu() {
      this.showNav = !this.showNav;
    },

    forceShowMenu: function forceShowMenu() {
      this.showNav = true;
      this.resizeApp();
    },

    forceHideMenu: function forceHideMenu() {
      this.showNav = false;
      this.resizeApp();
    },

    menuStyle: function menuStyle() {
      var title = document.getElementById('mainMenu').getBoundingClientRect();
      return 'height:' + (window.innerHeight - title.height - 4).toString() + 'px';
    },

    setHeight: function setHeight(elementId, size) {
      document.getElementById(elementId).style.height = size.toString() + 'px';
    },

    resizeApp: function resizeApp() {
      var appContentSize = 0;
      if (window.innerWidth < 768) {
        this.desktopMode = false;
      } else {
        this.desktopMode = true;
        appContentSize = window.innerHeight - 30;
      }
      this.setHeight('app', window.innerHeight);
    },

    layoutDialogBeforeClose: function layoutDialogBeforeClose() {
      var _this = this;

      if (this.layoutDialog.preventClose) {
        this.$confirm('¿Cancelar ' + this.layoutDialog.title + '?', '', {
          confirmButtonText: 'Cerrar',
          cancelButtonText: 'Cancelar',
          type: 'warning'
        }).then(function () {
          _this.setLayoutDialogVisibility(false);
          return true;
        }).catch(function () {
          return false;
        });
      }
      this.setLayoutDialogVisibility(false);
    },

    layoutDialogClose: function layoutDialogClose(layoutDialog) {
      if (layoutDialog.instance) {
        layoutDialog.instance.$emit('reset');
      }
    },
    refresh: function refresh() {},

    search: function search() {},

    deleteEntity: function deleteEntity() {}

  })
};

/***/ }),
/* 208 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": "app"
    }
  }, [_c('transition', {
    attrs: {
      "name": "fadeLoader"
    }
  }, [(_vm.activeLayoutLoading) ? _c('div', {
    staticClass: "globalAppLoader",
    attrs: {
      "id": "globalLoader"
    }
  }, [_c('div', {
    staticClass: "half-circle-spinner"
  }, [_c('div', {
    staticClass: "circle circle-1"
  }), _vm._v(" "), _c('div', {
    staticClass: "circle circle-2"
  })])]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "container",
    attrs: {
      "id": "container"
    }
  }, [_c('el-container', [_c('el-header', {
    attrs: {
      "id": "globalHeader"
    }
  }, [_c('span', {
    attrs: {
      "id": "menuIcon"
    },
    on: {
      "click": function($event) {
        _vm.showNav = !_vm.showNav
      }
    }
  }, [_c('i', {
    staticClass: "sfa fa fa-bars"
  })]), _vm._v(" "), _c('span', {
    attrs: {
      "id": "headerLabel"
    }
  }, [_c('i', [_vm._v(_vm._s(_vm.headerLabel))])])]), _vm._v(" "), _c('el-container', [_c('el-aside', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showNav),
      expression: "showNav"
    }, {
      name: "bar",
      rawName: "v-bar"
    }],
    attrs: {
      "id": "globalAside",
      "width": _vm.width
    }
  }, [_c('div', [_c('main-menu', {
    attrs: {
      "theme": "dark",
      "id": "mainMenu",
      "url": _vm.urlMenu
    }
  })], 1)]), _vm._v(" "), _c('el-container', {
    directives: [{
      name: "bar",
      rawName: "v-bar"
    }],
    attrs: {
      "id": "globalContainer"
    }
  }, [_c('div', {
    staticClass: "globalContainerFrame"
  }, [_c('el-main', [_c('el-row', {
    attrs: {
      "gutter": 10
    }
  }, [_c('el-col', {
    attrs: {
      "xs": 24,
      "sm": 24,
      "md": 24,
      "lg": 24
    }
  }, [_c('div', {
    staticClass: "grid-content",
    attrs: {
      "id": "colAppContent"
    }
  }, [_c('div', {
    attrs: {
      "id": "appContent"
    }
  }, _vm._l((_vm.itemContent.layoutUrl), function(tab, tabIndex) {
    return _c('content-spec', {
      directives: [{
        name: "show",
        rawName: "v-show",
        value: (tab.url == _vm.activeLayoutUrl),
        expression: "tab.url == activeLayoutUrl"
      }],
      key: tabIndex,
      staticClass: "contentSpec",
      attrs: {
        "layoutUrl": tab.url,
        "resetParams": tab.resetParams
      },
      on: {
        "getHeader": function($event) {
          _vm.setLayoutHeader({
            index: tabIndex,
            header: $event
          })
        },
        "getData": function($event) {
          _vm.setLayoutDataHeader({
            index: tabIndex,
            data: $event
          })
        },
        "sectionLoading": function($event) {
          _vm.setLayoutLoading({
            index: tabIndex,
            loading: $event
          })
        }
      }
    })
  }))])])], 1)], 1)], 1)])], 1)], 1)], 1), _vm._v(" "), _c('el-dialog', {
    staticClass: "dialogLayout",
    attrs: {
      "title": _vm.layoutDialog.title,
      "visible": _vm.layoutDialog.visible,
      "before-close": _vm.layoutDialogBeforeClose
    },
    on: {
      "update:visible": function($event) {
        _vm.$set(_vm.layoutDialog, "visible", $event)
      },
      "close": function($event) {
        _vm.layoutDialogClose(_vm.layoutDialog)
      }
    }
  }, [_c('content-spec', {
    attrs: {
      "layoutUrl": _vm.layoutDialog.url,
      "resetParams": _vm.layoutDialog.resetParams,
      "extraParams": _vm.layoutDialog.extraParams
    }
  })], 1)], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-f7863314", esExports)
  }
}

/***/ }),
/* 209 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(210);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 210 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_3d1837e4_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(212);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_3d1837e4_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/main-menu/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3d1837e4", Component.options)
  } else {
    hotAPI.reload("data-v-3d1837e4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 211 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var _vuex = __webpack_require__(6);

exports.default = {
  name: 'MainMenu',

  props: ["url"],

  data: function data() {
    return {
      uniqueOpened: true,
      searchValue: null,
      searchResultValues: [],
      loading: false,
      activeIndex: '1',
      itemType: {
        item: 'item',
        subItem: 'subItem',
        itemGroup: 'itemGroup',
        icon: 'icon'
      },
      mainMenu: {
        items: [{
          id: 1,
          label: 'Vatrast',
          layoutUrl: '#',
          icon: '',
          type: 'item'
        }]
      }
    };
  },

  created: function created() {
    this.getUrlData();
  },

  watch: {
    menu: function menu(value) {
      this.mainMenu = value;
    },
    searchResult: function searchResult(values) {
      this.searchResultValues = values;
    }
  },

  computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl', 'layouts']), {
    menu: function menu() {
      return this.mainMenu;
    },
    searchResult: function searchResult() {
      return this.searchResultValues;
    }
  }),

  methods: _extends({}, (0, _vuex.mapMutations)(['setLayoutUrlStandard', //mapea this.setLayoutUrl() to this.$store.commit('setLayoutUrl')
  'setIcon', 'removeLayout', 'setLayoutDialogUrl', 'setLayoutDialogVisibility']), {

    getUrlData: _.debounce(function () {
      var instance = this;
      var now = new Date().toLocaleString();
      axios.get(instance.baseUrl + instance.url + '?' + now).then(function (response) {
        instance.mainMenu = response.data;
      }).catch(function (error) {
        console.log(error);
      });
    }, 100),

    getLabel: function getLabel(originalLabel) {
      return originalLabel.length > 18 ? originalLabel.substring(0, 15) + '...' : originalLabel;
    },

    handleSelect: function handleSelect(key, keyPath) {
      var prop = key.split("|");
      var link = prop[0];
      var icon = prop[1];
      var mode = prop[2];
      var title = prop[3];
      var instance = this;
      if (mode == 'dialog') {
        this.setLayoutDialogUrl({
          'url': link,
          'title': title,
          'preventClose': false,
          'extraParams': {},
          'instance': instance
        });
      } else {
        var layoutUrl = link.layoutUrl;

        this.setLayoutUrlStandard(link);
        this.setIcon(icon);
      }
    },
    getItemIconClass: function getItemIconClass(icon) {
      return "sfa fa fa-" + icon;
    },


    remoteSearch: _.debounce(function () {
      var instance = this;
      var now = new Date().toLocaleString();
      axios.get(instance.baseUrl + 'js/data/searchItems.json?' + now).then(function (response) {
        instance.searchResultValues = response.data;
      }).catch(function (error) {
        //instance.answer = 'Error! Could not reach the API. ' + error
      });
    }, 500),

    searchSelect: function searchSelect() {
      this.searchValue = null;
    }
  })
};

/***/ }),
/* 212 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('el-menu', {
    staticClass: "disable-select el-menu-demo",
    attrs: {
      "default-active": _vm.activeIndex,
      "unique-opened": _vm.uniqueOpened,
      "mode": "vertical"
    },
    on: {
      "select": _vm.handleSelect
    }
  }, [_vm._l((_vm.menu.items), function(menuItem, menuItemIndex) {
    return _c('div', {
      key: menuItem.id
    }, [(menuItem.type === _vm.itemType.item) ? _c('el-menu-item', {
      attrs: {
        "index": menuItem.layoutUrl
      }
    }, [_c('i', {
      class: _vm.getItemIconClass(menuItem.icon),
      attrs: {
        "aria-hidden": "true"
      }
    }), _vm._v(_vm._s(menuItem.label))]) : _vm._e(), _vm._v(" "), (menuItem.type === _vm.itemType.subItem) ? _c('el-submenu', {
      attrs: {
        "index": menuItem.layoutUrl
      }
    }, [_c('template', {
      slot: "title"
    }, [_c('i', {
      class: _vm.getItemIconClass(menuItem.icon),
      attrs: {
        "aria-hidden": "true"
      }
    }), _vm._v(_vm._s(menuItem.label))]), _vm._v(" "), _vm._l((menuItem.subItems), function(subItem, subItemIndex) {
      return _c('div', {
        key: subItem.id
      }, [(subItem.type === _vm.itemType.item) ? _c('el-menu-item', {
        attrs: {
          "index": subItem.layoutUrl + '|' + _vm.getItemIconClass(subItem.icon) + '|' + subItem.container + '|' + subItem.label
        }
      }, [_c('i', {
        class: _vm.getItemIconClass(subItem.icon),
        attrs: {
          "aria-hidden": "true"
        }
      }), _vm._v(_vm._s(subItem.label) + "\n        ")]) : _vm._e(), _vm._v(" "), (subItem.type === _vm.itemType.itemGroup) ? _c('el-menu-item-group', {
        attrs: {
          "title": subItem.label
        }
      }, _vm._l((subItem.subItems), function(subItemLevel2, subItemIndex2) {
        return _c('div', {
          key: subItemLevel2.id
        }, [_c('el-menu-item', {
          attrs: {
            "index": subItemLevel2.layoutUrl + '|' + _vm.getItemIconClass(subItemLevel2.icon) + '|' + subItemLevel2.container + '|' + subItem.label
          }
        }, [_c('i', {
          class: _vm.getItemIconClass(subItemLevel2.icon),
          attrs: {
            "aria-hidden": "true"
          }
        }), _vm._v("\n                " + _vm._s(subItemLevel2.label) + "\n            ")])], 1)
      })) : _vm._e()], 1)
    })], 2) : _vm._e()], 1)
  }), _vm._v(" "), _vm._l((_vm.layouts), function(layout, layoutIndex) {
    return (!layout.isStandard) ? _c('el-menu-item', {
      key: layoutIndex,
      attrs: {
        "index": layout.url
      }
    }, [_c('i', {
      staticClass: "sfa fa fa-circle",
      attrs: {
        "aria-hidden": "true"
      }
    }), _vm._v(_vm._s(_vm.getLabel(layout.header.label))), _vm._v(" "), _c('i', {
      staticClass: "sfa fa fa-close closeButtonItem",
      on: {
        "click": function($event) {
          $event.stopPropagation();
          _vm.removeLayout(layoutIndex)
        }
      }
    })]) : _vm._e()
  })], 2)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-3d1837e4", esExports)
  }
}

/***/ }),
/* 213 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(214);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 214 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_0cca1065_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(216);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_0cca1065_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/column-spec/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0cca1065", Component.options)
  } else {
    hotAPI.reload("data-v-0cca1065", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 215 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var _vuex = __webpack_require__(6);

exports.default = {
  name: 'ColumnSpec',

  props: ["dataUrl", "specDef", "viewType", "specColumn", "filters", "resetParams", "extraParams"],

  data: function data() {
    return {
      actions: {
        save: {
          prompt: false,
          url: 'data/save/'
        },
        reset: {
          prompt: false
        }
      },
      loading: true,
      isNewEntity: false,
      newElement: false,
      deleteElement: false,
      viewTypes: {
        form: 1,
        formCollapse: 2,
        table: 3,
        graph: 4,
        layoutEditor: 5,
        layoutServiceEditor: 6
      },
      originalValues: {
        specs: {
          hash: '',
          label: '',
          fields: []
        },
        data: {
          specHash: '',
          entities: []
        }
      },
      contentSpec: {
        spec: {
          hash: '',
          label: '',
          fields: []
        },
        data: {
          specHash: '',
          entities: []
        }

      }
    };
  },

  created: function created() {
    this.newEntity();
    if (this.specDef.saveAction) {
      this.actions.save.url = this.specDef.saveAction;
    }
    this.resetData();
  },

  ready: function ready() {
    console.log(1);
  },

  computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl']), {
    label: function label() {
      var format = this.contentSpec.label.format;
      var output = "";
      for (var i = 0; i < format.length; i++) {
        if (typeof format[i] === 'string') {
          output += format[i];
        } else {
          var selObj = this.contentSpec.fields.filter(function (obj) {
            return obj.id == format[i];
          });
          output += selObj[0].value;
        }
      }
      return output;
    },
    autoRefresh: {
      get: function get() {
        if (this.resetParams.data) {
          this.resetData();
        }
        return this.resetParams.data;
      },
      set: function set(v) {
        this.resetParams.data = v;
      }
    }
  }),

  methods: _extends({}, (0, _vuex.mapMutations)(['setLayoutUrl', 'setLayoutDialogUrl', 'setLayoutDialogVisibility']), {

    clone: function clone(obj) {
      return JSON.parse(JSON.stringify(obj));
    },

    getSpecEntityLabel: function getSpecEntityLabel(hash, entity) {
      var label = '';
      var value = '';
      var spec = this.contentSpec.spec;
      for (var i = 0; i < spec.label.format.length; i++) {
        if (Number.isInteger(spec.label.format[i])) {
          value = this.getFieldData(entity, spec.label.format[i]).value;
          if (value != null) {
            label += value;
          }
        } else {
          label += spec.label.format[i];
        }
      }
      return label;
    },

    getItemIconClass: function getItemIconClass(icon) {
      return "sfa fa fa-" + icon + " icon";
    },


    getDataUrl: _.throttle(function (url) {
      var instance = this;
      var now = new Date().toLocaleString();
      axios.get(instance.baseUrl + url + '/' + JSON.stringify(instance.filters) + '?' + now).then(function (response) {
        instance.autoRefresh = false;
        instance.mergeData(response.data);
        instance.onGetData();
      }).catch(function (error) {
        //instance.answer = 'Error! Could not reach the API. ' + error
      });
    }, 0),

    onGetData: function onGetData() {
      if (!this.specColumn.multiple && this.specColumn.name == 'default' && this.contentSpec.data.entities.length > 0) {
        this.$emit('getData', this.contentSpec.data.entities[0]);
      }
    },

    mergeData: function mergeData(data) {
      this.contentSpec.data = data;
      this.originalValues.data = this.clone(this.contentSpec.data);
      this.$emit('sectionLoading', false);
      var isNewEntity = data.entities.length == 1 && data.entities[0].id == null;
      if (this.extraParams && this.extraParams.hasOwnProperty('newEntity')) {
        if (this.extraParams.newEntity) {
          this.contentSpec.data.entities = [];
          this.$emit('sectionLoading', false);
          this.addEntity();
          isNewEntity = true;
          //getData = false;
        }
      }

      if (isNewEntity && this.specColumn.name == 'default') {
        this.isNewEntity = true;
      }
    },

    newEntity: function newEntity() {
      var newEntity = {
        id: null
      };
      for (var key in this.specDef.fields) {
        if (key.id) {
          newEntity[key.id] = null;
        }
      }
      return newEntity;
    },

    addEntity: function addEntity() {
      if (this.specColumn.reverse) {
        this.contentSpec.data.entities.push(this.newEntity());
      } else {
        this.contentSpec.data.entities.unshift(this.newEntity());
      }
      this.newElement = true;
    },

    onDelete: function onDelete() {
      this.deleteElement = true;
    },

    resetData: function resetData() {
      this.getDataUrl(this.dataUrl);
      this.actions.reset.prompt = false;
      this.newElement = false;
      this.deleteElement = false;
    },

    resetDataPrompt: function resetDataPrompt() {
      var _this = this;

      this.$confirm('Si continúa perderá los cambios no guardados', 'Warning', {
        confirmButtonText: 'Continuar',
        cancelButtonText: 'Cancelar',
        type: 'warning'
      }).then(function () {
        _this.resetData();
      });
    },

    /*
    handleCommand(command) {
      if (command === 'add') {
        this.contentSpec.data.entities.push(this.newEntity());
      } else if (command === 'save') {
        this.save();
      }
      this.$message('click on item ' + command);
    },*/
    handleCommand: function handleCommand(command) {
      this.handleAction(command);
    },


    handleAction: function handleAction(actionLabel) {
      for (var a = 0; a < this.specColumn.actionLinks.length; a++) {
        if (this.specColumn.actionLinks[a].label == actionLabel) {
          var link = this.specColumn.actionLinks[a].link;

          link = link.replace("{{specHash}}", this.specDef.hash);

          switch (this.specColumn.actionLinks[a].mothod) {
            case "setLayout":
              this.setLayoutUrl(link);
              break;
            case "setLayoutDialog":
              var title = this.specColumn.actionLinks[a].title;
              var preventClose = this.specColumn.actionLinks[a].preventClose;
              if (this.specColumn.actionLinks[a].hasOwnProperty('extraParams')) {
                var extraParams = this.specColumn.actionLinks[a].extraParams;
              } else {
                var extraParams = {};
              }
              var instance = this;
              this.setLayoutDialogUrl({
                'url': link,
                'title': title,
                'preventClose': preventClose,
                'extraParams': extraParams,
                'instance': instance
              });
              break;
            case "get":
              //text = "I am not a fan of orange.";
              break;
            case "post":
              var obj = this.clone(this.specColumn.actionLinks[a].model);
              for (var p in obj) {
                obj[p] = obj[p].replace("{{specHash}}", this.specDef.hash);
              }
              var msg = this.specColumn.actionLinks[a].confirmation;
              var instance = this;
              this.$confirm(msg, '', {
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                type: 'warning'
              }).then(function () {
                instance.$emit('reset', false);
                axios.post(instance.baseUrl + link, JSON.stringify(obj)).then(function (response) {
                  instance.$emit('reset');
                  instance.$notify({
                    type: 'success',
                    message: response.data.msg,
                    position: 'bottom-right'
                  });
                }).catch(function (error) {
                  instance.$notify({
                    type: 'error',
                    message: 'Error al ejecutar acción',
                    position: 'bottom-right'
                  });
                });
              }).catch(function () {
                /*this.$message({
                  type: 'info',
                  message: 'Delete canceled'
                });*/
              });
              break;
            default:
              console.log('Undefined command type');
          }

          break;
        }
      }
    },

    getOriginalEntity: function getOriginalEntity(id) {
      for (var e = 0; e < this.originalValues.data.entities.length; e++) {
        if (this.originalValues.data.entities[e].id === id) {
          return this.originalValues.data.entities[e];
        }
      }
    },


    getSpecField: function getSpecField(fieldId) {
      for (var f = 0; f < this.specDef.fields.length; f++) {
        if (this.specDef.fields[f].id === fieldId) {
          return this.specDef.fields[f];
        }
      }
    },

    eventSave: function eventSave() {
      if (!this.isNewEntity) {
        this.save();
      }
    },

    save: _.debounce(function () {
      this.actions.save.prompt = false;
      if (this.specColumn.addElements == '1') {
        var filters = this.filters;
      } else {
        var filters = {};
      }
      var newData = {
        specHash: this.contentSpec.data.specHash,
        filters: filters,
        entities: []
      };
      for (var e = 0; e < this.contentSpec.data.entities.length; e++) {
        var newEntity = {
          id: this.contentSpec.data.entities[e].id
        };
        var currentId = this.contentSpec.data.entities[e].id;
        if (currentId == null) {
          newEntity = this.clone(this.contentSpec.data.entities[e]);
          newData.entities.push(newEntity);
        } else if (currentId < 0) {
          newEntity.id = currentId;
          newData.entities.push(newEntity);
        } else {
          var addEntity = false;
          for (var key in this.contentSpec.data.entities[e]) {
            var originalEntity = this.getOriginalEntity(currentId);
            if (this.contentSpec.data.entities[e][key] != originalEntity[key]) {
              newEntity[key] = this.contentSpec.data.entities[e][key];
              addEntity = true;
            }
          }
          if (addEntity) {
            newData.entities.push(newEntity);
          }
        }
      }

      if (newData.entities.length > 0) {
        var instance = this;
        axios.post(instance.baseUrl + this.actions.save.url, JSON.stringify(newData)).then(function (response) {
          /*instance.$notify({
            type: 'success',
            message: response.data.msg,
            position: 'bottom-right'
          });*/
          instance.originalValues.data.entities = instance.clone(instance.contentSpec.data.entities);
          instance.onGetData();
          if (response.data.layoutUrl != null) {
            instance.setLayoutUrl(response.data.layoutUrl);
          }
          instance.setLayoutDialogVisibility(false);
          instance.newElement = false;
          instance.deleteElement = false;
        }).catch(function (error) {
          console.log(error);
          instance.$notify({
            type: 'error',
            message: 'Entity creation error',
            position: 'bottom-right'
          });
        });
      }
    }, 350)
  })
};

/***/ }),
/* 216 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('transition', {
    attrs: {
      "name": "fadeSection"
    }
  }, [_c('el-card', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (!_vm.specColumn.loading),
      expression: "!specColumn.loading"
    }],
    staticClass: "el-col el-col-24 el-col-xs-24 el-col-sm-24 el-col-md-24 el-col-lg-24"
  }, [_c('div', {
    staticClass: "clearfix",
    attrs: {
      "slot": "header"
    },
    slot: "header"
  }, [_c('span', {
    style: ({
      color: _vm.specColumn.titleColor
    })
  }, [_c('i', {
    class: _vm.getItemIconClass(_vm.specColumn.icon),
    attrs: {
      "aria-hidden": "true"
    }
  }), _vm._v("\n      " + _vm._s(_vm.specColumn.label) + "\n      "), _c('i', {
    directives: [{
      name: "loading",
      rawName: "v-loading",
      value: (_vm.specColumn.loading || _vm.autoRefresh),
      expression: "specColumn.loading || autoRefresh"
    }]
  })]), _vm._v(" "), _c('div', {
    staticClass: "options",
    staticStyle: {
      "top": "-6px",
      "right": "-20px"
    }
  }, [(!_vm.specColumn.reverse && (_vm.specColumn.addElements == '1') && !(_vm.deleteElement || _vm.newElement) && ((_vm.viewType != _vm.viewTypes.layoutEditor) && (_vm.specColumn.addElements == '1'))) ? _c('el-button', {
    staticStyle: {
      "position": "inherit",
      "margin": "8px"
    },
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-plus"
    },
    on: {
      "click": _vm.addEntity
    }
  }) : _vm._e(), _vm._v(" "), ((_vm.deleteElement || _vm.newElement) && ((_vm.viewType != _vm.viewTypes.layoutEditor) && (_vm.specColumn.addElements == '1'))) ? _c('el-button-group', [_c('el-button', {
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-refresh"
    },
    on: {
      "click": function($event) {
        _vm.resetDataPrompt()
      }
    }
  }), _vm._v(" "), _c('el-button', {
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-check"
    },
    on: {
      "click": _vm.save
    }
  }), _vm._v(" "), (_vm.specColumn.addElements == '1') ? _c('el-button', {
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-plus"
    },
    on: {
      "click": _vm.addEntity
    }
  }) : _vm._e()], 1) : _vm._e()], 1), _vm._v(" "), (_vm.specColumn.actionLinks) ? _c('div', {
    staticClass: "options"
  }, [_vm._l((_vm.specColumn.actionLinks), function(action, index) {
    return ((index == 0) || (_vm.specColumn.actionLinks.length == 2 && index == 1)) ? _c('el-button', {
      key: action.label,
      attrs: {
        "type": "text",
        "size": "small"
      },
      on: {
        "click": function($event) {
          _vm.handleAction(action.label)
        }
      }
    }, [_vm._v("\n            " + _vm._s(action.label) + "\n          ")]) : _vm._e()
  }), _vm._v(" "), (_vm.specColumn.actionLinks.length > 2) ? _c('el-dropdown', {
    staticStyle: {
      "margin-left": "15px"
    },
    on: {
      "command": _vm.handleCommand
    }
  }, [_c('span', {
    staticClass: "el-dropdown-link"
  }, [_c('i', {
    staticClass: "el-icon-arrow-down el-icon--right"
  })]), _vm._v(" "), _c('el-dropdown-menu', {
    attrs: {
      "slot": "dropdown"
    },
    slot: "dropdown"
  }, _vm._l((_vm.specColumn.actionLinks), function(action, index) {
    return (index > 0) ? _c('el-dropdown-item', {
      key: action.label,
      attrs: {
        "command": action.label
      }
    }, [_vm._v(_vm._s(action.label))]) : _vm._e()
  }))], 1) : _vm._e()], 2) : _vm._e()]), _vm._v(" "), (!_vm.specColumn.loading) ? _c('div', [(_vm.viewType === _vm.viewTypes.formCollapse) ? _c('view-type-form-collapse', {
    attrs: {
      "spec": _vm.specDef,
      "entities": _vm.contentSpec.data.entities,
      "multiple": _vm.specColumn.multiple,
      "showLabels": _vm.specColumn.showLabels == '1' || !_vm.specColumn.showLabels
    },
    on: {
      "save": function($event) {
        _vm.eventSave()
      },
      "reset": function($event) {
        _vm.resetData()
      },
      "onDelete": function($event) {
        _vm.onDelete()
      }
    }
  }) : _vm._e(), _vm._v(" "), (_vm.viewType === _vm.viewTypes.form) ? _c('view-type-form', {
    attrs: {
      "spec": _vm.specDef,
      "entities": _vm.contentSpec.data.entities,
      "multiple": _vm.specColumn.multiple,
      "showLabels": _vm.specColumn.showLabels == '1' || !_vm.specColumn.showLabels
    },
    on: {
      "save": function($event) {
        _vm.eventSave()
      },
      "reset": function($event) {
        _vm.resetData()
      },
      "onDelete": function($event) {
        _vm.onDelete()
      }
    }
  }) : _vm._e(), _vm._v(" "), (_vm.viewType === _vm.viewTypes.table) ? _c('view-type-table', {
    attrs: {
      "spec": _vm.specDef,
      "entities": _vm.contentSpec.data.entities
    },
    on: {
      "reset": function($event) {
        _vm.resetData()
      }
    }
  }) : _vm._e(), _vm._v(" "), (_vm.viewType === _vm.viewTypes.layoutEditor) ? _c('layout-editor', {
    attrs: {
      "spec": _vm.specDef,
      "entity": _vm.contentSpec.data
    },
    on: {
      "reset": function($event) {
        _vm.resetData()
      }
    }
  }) : _vm._e(), _vm._v(" "), (_vm.viewType === _vm.viewTypes.layoutServiceEditor) ? _c('layout-service-editor', {
    attrs: {
      "spec": _vm.specDef,
      "entity": _vm.contentSpec.data,
      "layoutTypeName": _vm.specColumn.layoutTypeName
    },
    on: {
      "reset": function($event) {
        _vm.resetData()
      }
    }
  }) : _vm._e(), _vm._v(" "), _c('el-popover', {
    ref: "savePrompt",
    attrs: {
      "placement": "top",
      "width": "250"
    },
    model: {
      value: (_vm.actions.save.prompt),
      callback: function($$v) {
        _vm.$set(_vm.actions.save, "prompt", $$v)
      },
      expression: "actions.save.prompt"
    }
  }, [_c('p', [_vm._v("¿Confirma guardar los cambios?")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "text-align": "right",
      "margin": "0"
    }
  }, [_c('el-button', {
    attrs: {
      "size": "mini",
      "type": "text"
    },
    on: {
      "click": function($event) {
        _vm.actions.save.prompt = false
      }
    }
  }, [_vm._v("Cancelar")]), _vm._v(" "), _c('el-button', {
    attrs: {
      "type": "primary",
      "size": "mini"
    },
    on: {
      "click": _vm.save
    }
  }, [_vm._v("Confirmar")])], 1)]), _vm._v(" "), _c('el-popover', {
    ref: "resetPrompt",
    attrs: {
      "placement": "top",
      "width": "250"
    },
    model: {
      value: (_vm.actions.reset.prompt),
      callback: function($$v) {
        _vm.$set(_vm.actions.reset, "prompt", $$v)
      },
      expression: "actions.reset.prompt"
    }
  }, [_c('p', [_vm._v("¿Confirma descartar los cambios?")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "text-align": "right",
      "margin": "0"
    }
  }, [_c('el-button', {
    attrs: {
      "size": "mini",
      "type": "text"
    },
    on: {
      "click": function($event) {
        _vm.actions.reset.prompt = false
      }
    }
  }, [_vm._v("Cancelar")]), _vm._v(" "), _c('el-button', {
    attrs: {
      "type": "primary",
      "size": "mini"
    },
    on: {
      "click": function($event) {
        _vm.resetData()
      }
    }
  }, [_vm._v("Descartar cambios")])], 1)]), _vm._v(" "), (_vm.isNewEntity || _vm.specColumn.reverse) ? _c('el-button-group', [(_vm.specColumn.addElements == '1') ? _c('el-button', {
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-plus"
    },
    on: {
      "click": _vm.addEntity
    }
  }) : _vm._e(), _vm._v(" "), _c('el-button', {
    directives: [{
      name: "popover",
      rawName: "v-popover:resetPrompt",
      arg: "resetPrompt"
    }],
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-refresh"
    }
  }), _vm._v(" "), _c('el-button', {
    directives: [{
      name: "popover",
      rawName: "v-popover:savePrompt",
      arg: "savePrompt"
    }],
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-check"
    }
  })], 1) : _vm._e()], 1) : _vm._e()])], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0cca1065", esExports)
  }
}

/***/ }),
/* 217 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(218);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 218 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_06cca8f0_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(220);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_06cca8f0_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/content-spec/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-06cca8f0", Component.options)
  } else {
    hotAPI.reload("data-v-06cca8f0", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 219 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var _vuex = __webpack_require__(6);

exports.default = {
  name: 'ContentSpec',

  props: ["layoutUrl", "resetParams", "extraParams"],

  data: function data() {
    return {
      originalValues: {
        specs: [],
        data: []
      },
      contentSpecTypes: {
        rows: 1,
        tabs: 2,
        layoutEditor: 3
      },
      contentSpecEmpty: {
        specs: [{
          hash: '',
          label: '',
          showLinkActions: false,
          linkActions: [],
          fields: []
        }],
        data: [{
          specHash: '',
          entities: []
        }],
        layout: {
          header: {
            type: ""
          },
          filters: {},
          type: '',
          tabs: [{
            rows: [{
              columns: []
            }]
          }],
          rows: [{
            columns: []
          }]
        }
      },
      contentSpec: {}
    };
  },

  created: function created() {
    this.contentSpec = this.clone(this.contentSpecEmpty);
    this.getLayoutUrl();
  },

  watch: {
    _layoutUrl: function _layoutUrl(value) {
      this.layoutUrl = value;
      this.getLayoutUrl();
    },
    _specs: function _specs(value) {
      this.contentSpec.specs = value;
    }
  },

  computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl']), {
    _layoutUrl: function _layoutUrl() {
      return this.layoutUrl;
    },
    _specs: function _specs() {
      return this.contentSpec.specs;
    },
    label: function label() {
      var format = this.contentSpec.label.format;
      var output = "";
      for (var i = 0; i < format.length; i++) {
        if (typeof format[i] === 'string') {
          output += format[i];
        } else {
          var selObj = this.contentSpec.fields.filter(function (obj) {
            return obj.id == format[i];
          });
          output += selObj[0].value;
        }
      }
      return output;
    } /*
      autoRefresh: {
      get: function() {
        if (this.resetParams.spec) {
          this.getLayoutSpecs()
        }
        return this.resetParams.spec;
      },
      set: function(v) {
        this.resetParams.spec = v;
      }
      }*/
  }),

  methods: {
    clone: function clone(obj) {
      return JSON.parse(JSON.stringify(obj));
    },
    getLayoutUrl: _.throttle(function () {
      var instance = this;
      this.contentSpec.layout = this.clone(this.contentSpecEmpty.layout);
      var now = new Date().toLocaleString();
      axios.get(instance.baseUrl + instance.layoutUrl + '?' + now).then(function (response) {
        if (response.data.layout) {
          instance.contentSpec.layout = response.data.layout;
          instance.$emit('getHeader', response.data.layout.header);
          instance.getLayoutSpecs();
        }
      }).catch(function (error) {
        //instance.contentSpec.layout = instance.clone(instance.contentSpecEmpty.layout);
      });
    }, 0),
    getSpecUrl: _.throttle(function (url) {
      var instance = this;
      var now = new Date().toLocaleString();
      axios.get(instance.baseUrl + url + '?' + now).then(function (response) {
        instance.mergeSpec(response.data);
      }).catch(function (error) {
        //instance.answer = 'Error! Could not reach the API. ' + error
      });
    }, 0),

    getLayoutSpecs: function getLayoutSpecs() {
      switch (this.contentSpec.layout.type) {
        case 1:
        case 3:
          this.getRowsSpecs(this.contentSpec.layout.rows);
          break;
        case 2:
          for (var t = 0; t < this.contentSpec.layout.tabs.length; t++) {
            this.getRowsSpecs(this.contentSpec.layout.tabs[t].rows);
          }
          break;
      }
    },

    getRowsSpecs: function getRowsSpecs(rows) {
      for (var r = 0; r < rows.length; r++) {
        for (var c = 0; c < rows[r].columns.length; c++) {
          for (var s = 0; s < rows[r].columns[c].specs.length; s++) {
            this.getSpecUrl(rows[r].columns[c].specs[s].specUrl);
          }
        }
      }
    },

    mergeSpec: function mergeSpec(spec) {
      var isNew = true;
      for (var s = 0; s < this.contentSpec.specs.length; s++) {
        if (this.contentSpec.specs[s].hash === spec.hash) {
          this.contentSpec.specs[s] = spec;
          this.originalValues.specs[s] = spec;
          isNew = false;
          break;
        }
      }
      if (isNew) {
        this.contentSpec.specs.push(spec);
        this.originalValues.specs = this.clone(this.contentSpec.specs);
      }
    },

    getSpec: function getSpec(specDef) {
      for (var i = 0; i < this.contentSpec.specs.length; i++) {
        if (this.contentSpec.specs[i].hash == specDef.hash) {
          return this.contentSpec.specs[i];
        }
      }
    }
  }
};

/***/ }),
/* 220 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.contentSpec.layout.type === _vm.contentSpecTypes.tabs) ? _c('layout-tab', {
    attrs: {
      "tabs": _vm.contentSpec.layout.tabs,
      "specs": _vm._specs,
      "filters": _vm.contentSpec.layout.filters,
      "resetParams": _vm.resetParams
    },
    on: {
      "sectionLoading": function($event) {
        _vm.$emit('sectionLoading', $event)
      }
    }
  }) : _vm._e(), _vm._v(" "), (_vm.contentSpec.layout.type === _vm.contentSpecTypes.rows) ? _c('layout-row', {
    attrs: {
      "rows": _vm.contentSpec.layout.rows,
      "specs": _vm._specs,
      "filters": _vm.contentSpec.layout.filters,
      "resetParams": _vm.resetParams,
      "extraParams": _vm.extraParams
    },
    on: {
      "getData": function($event) {
        _vm.$emit('getData', $event)
      },
      "sectionLoading": function($event) {
        _vm.$emit('sectionLoading', $event)
      }
    }
  }) : _vm._e()], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-06cca8f0", esExports)
  }
}

/***/ }),
/* 221 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(222);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 222 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_bfe45a70_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(224);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_bfe45a70_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/layout-row/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bfe45a70", Component.options)
  } else {
    hotAPI.reload("data-v-bfe45a70", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 223 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  name: 'LayoutRow',

  props: ["rows", "specs", "filters", "resetParams", "extraParams"],

  data: function data() {
    return {};
  },

  created: function created() {
    //console.log(JSON.stringify(this.rows));
    //console.log(JSON.stringify(this.specs));
  },

  methods: {
    sectionLoading: function sectionLoading(specColumn, event) {
      specColumn.loading = event;
      this.$emit('sectionLoading', event);
    }
  }
};

/***/ }),
/* 224 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', _vm._l((_vm.rows), function(row, rowIndex) {
    return _c('el-row', {
      key: row.id,
      attrs: {
        "gutter": 5
      }
    }, _vm._l((row.columns), function(column, columnIndex) {
      return _c('el-col', {
        key: column.id,
        attrs: {
          "xs": column.xs,
          "sm": column.sm,
          "md": column.md,
          "lg": column.lg
        }
      }, _vm._l((column.specs), function(specColumn, specColumnIndex) {
        return _c('div', _vm._l((_vm.specs), function(specDef, specDefIndex) {
          return _c('div', {
            key: specDefIndex
          }, [(specDef.hash === specColumn.hash) ? _c('div', [_c('column-spec', {
            attrs: {
              "dataUrl": specColumn.dataUrl,
              "specDef": specDef,
              "viewType": specColumn.viewType,
              "specColumn": specColumn,
              "filters": _vm.filters,
              "resetParams": _vm.resetParams,
              "extraParams": _vm.extraParams
            },
            on: {
              "sectionLoading": function($event) {
                _vm.sectionLoading(specColumn, $event)
              },
              "getData": function($event) {
                _vm.$emit('getData', $event)
              }
            }
          })], 1) : _vm._e()])
        }))
      }))
    }))
  }))
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-bfe45a70", esExports)
  }
}

/***/ }),
/* 225 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(226);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 226 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1bff9626_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(228);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1bff9626_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/layout-tab/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1bff9626", Component.options)
  } else {
    hotAPI.reload("data-v-1bff9626", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 227 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  name: 'LayoutTab',

  props: ["tabs", "specs", "filters", "resetParams"],

  data: function data() {
    return {};
  },

  created: function created() {
    //console.log(JSON.stringify(this.rows));
    //console.log(JSON.stringify(this.specs));
  },

  methods: {}
};

/***/ }),
/* 228 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('el-card', {
    staticClass: "el-col el-col-24 el-col-xs-24 el-col-sm-24 el-col-md-24 el-col-lg-24"
  }, [_c('el-tabs', {
    attrs: {
      "tab-position": "top",
      "activeName": "default"
    }
  }, _vm._l((_vm.tabs), function(tab, tabIndex) {
    return _c('el-tab-pane', {
      key: tab.name,
      attrs: {
        "label": tab.label,
        "name": tab.name
      }
    }, [_c('layout-row', {
      attrs: {
        "rows": tab.rows,
        "specs": _vm.specs,
        "filters": _vm.filters,
        "resetParams": _vm.resetParams
      },
      on: {
        "sectionLoading": function($event) {
          _vm.$emit('sectionLoading', $event)
        }
      }
    })], 1)
  }))], 1)], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1bff9626", esExports)
  }
}

/***/ }),
/* 229 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__style_component_css__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__style_component_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__style_component_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__src_main_vue__ = __webpack_require__(232);



/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_1__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_1__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_1__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_1__src_main_vue__["a" /* default */]);


/***/ }),
/* 230 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(231);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../css-loader/index.js!../../../../postcss-loader/lib/index.js!./component.css", function() {
			var newContent = require("!!../../../../css-loader/index.js!../../../../postcss-loader/lib/index.js!./component.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 231 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(undefined);
// imports


// module
exports.push([module.i, ".buttonSpec {\n    font-size: 20px;\n    cursor: pointer;\n    color: #3b4241;\n   }\n.fieldElement{\n  position: relative;\n  border-radius: 0px;\n  border: 1px solid #c4dad7;\n  background-color: #f7fcfa;\n  color: #4e8c89;\n}\n\n.fieldElement .el-icon-close{\n  position: absolute !important;\n  float: right;\n  margin-top: 14px;\n  margin-right: 11px;\n}\n\n.fieldElement .el-icon-close:hover{\n  background-color: #6D9690;\n}\n.addColumnBtn{\n\n}\n.layoutEditor .el-dialog__wrapper .el-dialog .el-dialog__body {\n    padding: 0px 20px;width:\n}\n\n.specDetailsBtn{\n  float: right;\n  right: 0px;\n  color: #3b4241;\n  background-color: transparent;\n  font-size: 18px;\n  padding: 0px;\n}\n.specDetailsBtn:active{\n  color: #09725f;\n  background-color: transparent;\n  padding: 0px;\n}\n.specDetailsBtn:hover{\n  color: #09725f;\n  background-color: transparent;\n  padding: 0px;\n}\n.specDetailsBtn:focus{\n  color: #09725f;\n  background-color: transparent;\n  padding: 0px;\n}\n.specDetailsColumnBtn {\n  padding: 0px;\n}\n\n.layoutEditorRow {\n  background-color: transparent;\n  min-height: 50px;\n}\n\n.layoutEditorColumn {\n  background-color: #e3e3e3;\n  min-height: 50px;\n  padding: 6px !important;\n  border: 2px solid #fff !important;\n}\n\n.trashSection {\n  margin-left: 50%;\n  position: relative;\n  left: -13px;\n}\n\n.trashIcon{\n  color: #d24d29;\n  font-size: 22px;\n}\n\n.trashSection div {\n  visibility: hidden;\n  display: none;\n}\n\n.columnIcon{\n  padding: 1px 6px;\n}", ""]);

// exports


/***/ }),
/* 232 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_controller_js__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_controller_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_controller_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_c47e76f2_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(235);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */
/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_controller_js___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_c47e76f2_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/layout-editor/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c47e76f2", Component.options)
  } else {
    hotAPI.reload("data-v-c47e76f2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 233 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _vuex = __webpack_require__(6);

var _data = __webpack_require__(234);

var _data2 = _interopRequireDefault(_data);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'LayoutEditor',

  props: ["spec", "entity"],

  data: function data() {
    return (0, _data2.default)();
  },

  created: function created() {
    this.getLayout();
  },

  watch: {
    _specHash: function _specHash(value) {
      this.entity.specHash = value;
      this.getLayout();
    }
  },

  computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl']), {
    _specHash: function _specHash() {
      return this.entity.specHash;
    }
  }),

  methods: {
    clone: function clone(obj) {
      return JSON.parse(JSON.stringify(obj));
    },

    showFilters: function showFilters(specHash) {
      this.filtersVisibility = true;
    },

    getFieldData: function getFieldData(entity, fieldId) {
      return entity[fieldId]; /*
                              if((fieldId > 0) && ("values" in entity)){
                              for (var j = 0; j < entity.values.length; j++) {
                              if(entity.values[j].id == fieldId){
                              return entity.values[j];
                              }
                              }
                              }
                              return {
                              'id': fieldId,
                              'value': null
                              };*/
    },

    getItemIconClass: function getItemIconClass(icon) {
      return "sfa fa fa-" + icon + " icon";
    },


    removeField: function removeField(specData, field) {
      specData.entity.fields.push(field);
      for (var i = specData.fields.length - 1; i >= 0; i--) {
        if (specData.fields[i].id == field.id) {
          specData.fields.splice(i, 1);
        }
      }
      specData.entity.fields.sort(this.sortFields);
    },

    getLayout: _.debounce(function () {
      if (this._specHash) {
        var instance = this;
        var now = new Date().toLocaleString();
        axios.get(instance.baseUrl + 'layout/editLayout/' + instance._specHash + '/default?' + now).then(function (response) {
          //instance.layout = response.data;
          if (response.data.layout) {
            instance.specsOriginalValues = [];
            instance.layout = response.data.layout;
            instance.layoutOriginalValues = instance.clone(instance.layout);
            instance.layout.rows.forEach(function (row) {
              row.columns.forEach(function (column) {
                column.specs.forEach(function (spec) {
                  instance.getSpec(spec.specUrl);
                  //instance.getData(spec.dataUrl)
                });
              });
            });
          }
        }).catch(function (error) {
          //instance.answer = 'Error! Could not reach the API. ' + error
        });
      }
    }, 0),

    getSpec: function getSpec(url) {
      if (this._specHash) {
        var instance = this;
        var now = new Date().toLocaleString();
        axios.get(instance.baseUrl + url + '?' + now).then(function (response) {
          try {
            response.data.entity.fields.sort(instance.sortFields);
            var specHash = response.data.hash;
            var inArray = instance.specs.find(function (spec) {
              return spec.hash == specHash;
            });
            if (inArray === undefined) {
              instance.specs.push(response.data);
            }
            instance.specsOriginalValues.push(instance.clone(response.data));
          } catch (err) {
            console.log('-' + err);
          }
        }).catch(function (error) {
          console.log(error);
          //instance.answer = 'Error! Could not reach the API. ' + error
        });
      }
    },

    sortFields: function compare(a, b) {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    },

    addField: function addField(obj) {
      for (var f = obj.fields.length - 1; f >= 0; f--) {
        var fieldId = obj.fields[f].id;
        obj.specData.fields.push(obj.fields[f]);
        for (var i = obj.specData.entity.fields.length - 1; i >= 0; i--) {
          if (obj.specData.entity.fields[i].id == fieldId) {
            obj.specData.entity.fields.splice(i, 1);
          }
        }
      }
      obj.specData.entity.fields.sort(this.sortFields);
    },

    addColumn: function addColumn() {
      var newColumn = {
        id: null,
        xs: 3,
        sm: 3,
        md: 3,
        lg: 3,
        specs: []
      };
      this.activeRow.columns.push(newColumn);
      console.log(JSON.stringify(this.activeRow));
    },

    addSpec: function addSpec(obj) {
      var instance = this;
      var now = new Date().toLocaleString();
      var newSpec = {
        entityId: obj.entity.id,
        columnId: obj.column.id
      };
      axios.post(instance.baseUrl + 'layout/addSpec/', encodeURIComponent(JSON.stringify(newSpec))).then(function (response) {
        obj.column.specs.push(response.data);
        instance.getSpec(response.data.specUrl);
      }).catch(function (error) {
        //instance.answer = 'Error! Could not reach the API. ' + error
      });
    },

    removeColumn: function removeColumn(column, index) {
      if (column.id > 0) {
        column.id *= -1;
      } else {
        this.activeRow.columns.splice(index, 1);
      }
    },


    reset: function reset() {
      this.actions.reset.prompt = false;
      this.layout = this.clone(this.layoutOriginalValues);
      this.specs = this.clone(this.specsOriginalValues);
    },

    save: _.debounce(function () {
      this.actions.save.prompt = false;
      var instance = this;
      this.specs.forEach(function (spec) {
        if (spec.hash) {
          var original = instance.specsOriginalValues.find(function (originalSpec) {
            return originalSpec.hash == spec.hash;
          });
          if (JSON.stringify(spec) != JSON.stringify(original)) {
            var newData = instance.clone(spec);
            delete newData.entity.fields;
            /*newData.fields.forEach(function(field){
              delete field.values;
            })*/
            //console.log(JSON.stringify(newData))
            axios.post(instance.baseUrl + instance.actions.save.urlSpec, encodeURIComponent(JSON.stringify(newData))).then(function (response) {
              instance.$notify({
                type: 'success',
                message: response.data.msg,
                position: 'bottom-right'
              });
            }).catch(function (error) {
              instance.$notify({
                type: 'error',
                message: 'Error al guardar sección ' + spec.label + ', intente nuevamente',
                position: 'bottom-right'
              });
            });
          }
        }
      });
      axios.post(instance.baseUrl + instance.actions.save.urlLayout, encodeURIComponent(JSON.stringify(instance.layout))).then(function (response) {
        instance.$notify({
          type: 'success',
          message: response.data.msg,
          position: 'bottom-right'
        });
        instance.getLayout();
      }).catch(function (error) {
        instance.$notify({
          type: 'error',
          message: 'Layut save error',
          position: 'bottom-right'
        });
      });
    }, 100)
  }
};

/***/ }),
/* 234 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["default"] = dataModel;
function dataModel() {
  return {
        columnTabs: [
          {
            icon: 'desktop',
            prop: 'lg'
          },
          {
            icon: 'laptop',
            prop: 'md'
          },
          {
            icon: 'tablet',
            prop: 'sm'
          },
          {
            icon: 'phone',
            prop: 'xs'
          }
        ],
        activeRow: {
          columns:[]
        },
        columnOptions: false,
        activeSpec: {},
        specOptions: false,
        removedObjects: [],
        showTrashSpec: false,
        colWidthValues:[
          {label:"100%", value:24},
          {label:"87.5%", value:21},
          {label:"75%", value:18},
          {label:"62.5%", value:15},
          {label:"50%", value:12},
          {label:"37.5%", value:9},
          {label:"25%", value:6},
          {label:"12.5%", value:3},
          {label:"Ocultar", value:0}
        ],
        colWidthMobileValues:[
          {label:"Mostrar", value:24},
          {label:"Ocultar", value:0}
        ],
        actions: {
          save: {
            prompt: false,
            urlSpec: 'layout/saveSpec/',
            urlLayout: 'layout/saveLayout/'
          },
          reset: {
            prompt: false
          }
        },
        filtersVisibility: false,
        specsOriginalValues: [],
        specs: [],
        layoutOriginalValues:{
        },
        layout:{
          header: {
            type: "",
          },
          filters :{},
          type: '',
          rows:[
            {
              columns:[]
            }
          ],
          relatedEntities:[]
        },
        icons:[
          {label:'Calendario / Fecha',name:'calendar'},
          {label:'Reloj / Tiempo',name:'clock-o'},
          {label:'group',name:'group'},
          {label:'link',name:'link'},
          {label:'files-o',name:'files-o'},
          {label:'folder', name:'folder'},
          {label:'arrows-v', name:'arrows-v'},
          {label:'arrows-h', name:'arrows-h'},
          {label:'bar-chart-o', name:'bar-chart-o'},
          {label:'list-ul',name:'list-ul'},
          {label:'dashboard',name:'dashboard'},
          {label:'comments-o',name:'comments-o'},
          {label:'sitemap',name:'sitemap'},
          {label:'circle-o',name:'circle-o'},
          {label:'quote-left',name:'quote-left'},
          {label:'quote-right',name:'quote-right'},
          {label:'circle',name:'circle'},
          {label:'reply',name:'reply'},
          {label:'flag-o',name:'flag-o'},
          {label:'question',name:'question'},
          {label:'info',name:'info'},
          {label:'exclamation',name:'exclamation'},
          {label:'puzzle-piece',name:'puzzle-piece'},
          {label:'sun-o', name:'sun-o'},
          {label:'moon-o', name:'moon-o'},
          {label:'archive', name:'archive'},
          {label:'database', name:'database'},
          {label:'support', name:'support'},
          {label:'trash', name:'trash'},
          {label:'ship', name:'ship'},
          {label:'user-secret', name:'user-secret'},
          {label:'motorcycle', name:'motorcycle'},
          {label:'server', name:'server'},
          {label:'map-o', name:'map-o'},
          {label:'address-book', name:'address-book'},
          {label:'bookmark', name:'bookmark'},
          {label:'share', name:'share'},
          {label:'asterisk', name:'asterisk'},
          {label:'warning', name:'warning'}
        ],
        viewIcon:[
         {id:1,label:'Form', name:'list-alt'},
         {id:2,label:'Collapsed Form', name:'list'},
         {id:3,label:'Table', name:'table'},
         {id:4,label:'Chart', name:'bar-chart'}
         ]
      }
    }

/***/ }),
/* 235 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "layoutEditor"
  }, [_c('el-dialog', {
    attrs: {
      "title": "Disposición",
      "visible": _vm.columnOptions,
      "width": "400px"
    },
    on: {
      "update:visible": function($event) {
        _vm.columnOptions = $event
      }
    }
  }, [_c('el-tabs', {
    staticStyle: {
      "padding-bottom": "20px"
    },
    attrs: {
      "type": "border-card"
    }
  }, _vm._l((_vm.columnTabs), function(tab, tabIndex) {
    return _c('el-tab-pane', {
      key: tabIndex
    }, [_c('span', {
      attrs: {
        "slot": "label"
      },
      slot: "label"
    }, [_c('i', {
      class: _vm.getItemIconClass(tab.icon),
      attrs: {
        "aria-hidden": "true"
      }
    })]), _vm._v(" "), _c('el-form', {
      ref: "form",
      refInFor: true,
      staticStyle: {
        "background-color": "transparent",
        "border": "none"
      },
      attrs: {
        "label-width": "20px",
        "label-position": "right"
      }
    }, [_vm._l((_vm.activeRow.columns), function(column, columIndex) {
      return (column.id > 0 || column.id == null) ? _c('el-form-item', {
        key: columIndex,
        staticStyle: {
          "margin-bottom": "5px"
        },
        attrs: {
          "label": ""
        }
      }, [_c('b', [_vm._v("Columna " + _vm._s(columIndex + 1) + " ")]), _vm._v(" "), _c('el-select', {
        staticStyle: {
          "width": "105px"
        },
        model: {
          value: (column[tab.prop]),
          callback: function($$v) {
            _vm.$set(column, tab.prop, $$v)
          },
          expression: "column[tab.prop]"
        }
      }, _vm._l((_vm.colWidthValues), function(item) {
        return _c('el-option', {
          key: item.value,
          attrs: {
            "label": item.label,
            "value": item.value
          }
        })
      })), _vm._v(" "), _c('el-button', {
        staticClass: "specDetailsBtn specDetailsColumnBtn",
        attrs: {
          "type": "text"
        },
        on: {
          "click": function($event) {
            _vm.removeColumn(column, columIndex)
          }
        }
      }, [_c('i', {
        staticClass: "sfa fa fa-trash"
      })])], 1) : _vm._e()
    }), _vm._v(" "), _c('el-button', {
      staticClass: "specDetailsBtn specDetailsColumnBtn",
      attrs: {
        "type": "text"
      },
      on: {
        "click": function($event) {
          _vm.addColumn()
        }
      }
    }, [_c('i', {
      staticClass: "sfa fa fa-plus-circle"
    })])], 2)], 1)
  }))], 1), _vm._v(" "), _c('el-dialog', {
    attrs: {
      "title": "Propiedades de sección",
      "visible": _vm.specOptions,
      "width": "400px"
    },
    on: {
      "update:visible": function($event) {
        _vm.specOptions = $event
      }
    }
  }, [_c('el-form', {
    ref: "form",
    staticStyle: {
      "background-color": "transparent",
      "border": "none"
    },
    attrs: {
      "label-width": "70px",
      "label-position": "right"
    }
  }, [_c('el-form-item', {
    attrs: {
      "label": "Título"
    }
  }, [_c('el-input', {
    attrs: {
      "placeholder": "Ingrese el título"
    },
    model: {
      value: (_vm.activeSpec.label),
      callback: function($$v) {
        _vm.$set(_vm.activeSpec, "label", $$v)
      },
      expression: "activeSpec.label"
    }
  })], 1), _vm._v(" "), _c('el-form-item', {
    attrs: {
      "label": "Ícono"
    }
  }, [_c('el-select', {
    attrs: {
      "filterable": "",
      "reserve-keyword": "",
      "placeholder": "Seleccione un ícono"
    },
    model: {
      value: (_vm.activeSpec.icon),
      callback: function($$v) {
        _vm.$set(_vm.activeSpec, "icon", $$v)
      },
      expression: "activeSpec.icon"
    }
  }, _vm._l((_vm.icons), function(item) {
    return _c('el-option', {
      key: item.name,
      attrs: {
        "label": item.label,
        "value": item.name
      }
    }, [_c('span', {
      staticStyle: {
        "float": "left"
      }
    }, [_vm._v(_vm._s(item.label))]), _vm._v(" "), _c('span', {
      staticStyle: {
        "float": "right",
        "color": "#8492a6",
        "font-size": "13px"
      }
    }, [_c('i', {
      class: _vm.getItemIconClass(item.name),
      attrs: {
        "aria-hidden": "true"
      }
    })])])
  }))], 1), _vm._v(" "), _c('el-form-item', {
    attrs: {
      "label": "Color del Título"
    }
  }, [_c('el-color-picker', {
    model: {
      value: (_vm.activeSpec.titleColor),
      callback: function($$v) {
        _vm.$set(_vm.activeSpec, "titleColor", $$v)
      },
      expression: "activeSpec.titleColor"
    }
  })], 1), _vm._v(" "), _c('el-form-item', {
    attrs: {
      "label": "Tipo de vista"
    }
  }, [_c('el-select', {
    attrs: {
      "filterable": "",
      "reserve-keyword": "",
      "placeholder": "Seleccione un tipo de vista"
    },
    model: {
      value: (_vm.activeSpec.viewType),
      callback: function($$v) {
        _vm.$set(_vm.activeSpec, "viewType", $$v)
      },
      expression: "activeSpec.viewType"
    }
  }, _vm._l((_vm.viewIcon), function(item) {
    return _c('el-option', {
      key: item.name,
      attrs: {
        "label": item.label,
        "value": item.id
      }
    }, [_c('span', {
      staticStyle: {
        "float": "left"
      }
    }, [_vm._v(_vm._s(item.label))]), _vm._v(" "), _c('span', {
      staticStyle: {
        "float": "right",
        "color": "#8492a6",
        "font-size": "13px"
      }
    }, [_c('i', {
      class: _vm.getItemIconClass(item.name),
      attrs: {
        "aria-hidden": "true"
      }
    })])])
  }))], 1), _vm._v(" "), _c('el-form-item', {
    attrs: {
      "label": "Sección Principal"
    }
  }, [_c('el-switch', {
    attrs: {
      "active-value": "default",
      "inactive-value": "custom"
    },
    model: {
      value: (_vm.activeSpec.name),
      callback: function($$v) {
        _vm.$set(_vm.activeSpec, "name", $$v)
      },
      expression: "activeSpec.name"
    }
  })], 1), _vm._v(" "), _c('el-form-item', {
    attrs: {
      "label": "Permite agregar"
    }
  }, [_c('el-switch', {
    attrs: {
      "active-value": "1",
      "inactive-value": "0"
    },
    model: {
      value: (_vm.activeSpec.addElements),
      callback: function($$v) {
        _vm.$set(_vm.activeSpec, "addElements", $$v)
      },
      expression: "activeSpec.addElements"
    }
  })], 1), _vm._v(" "), _c('el-form-item', {
    attrs: {
      "label": "Mostrar etiquetas"
    }
  }, [_c('el-switch', {
    attrs: {
      "active-value": "1",
      "inactive-value": "0"
    },
    model: {
      value: (_vm.activeSpec.showLabels),
      callback: function($$v) {
        _vm.$set(_vm.activeSpec, "showLabels", $$v)
      },
      expression: "activeSpec.showLabels"
    }
  })], 1)], 1)], 1), _vm._v(" "), _vm._l((_vm.layout.rows), function(row, rowIndex) {
    return _c('el-row', {
      key: row.id,
      staticClass: "layoutEditorRow",
      attrs: {
        "gutter": 5
      }
    }, [_c('el-col', {
      attrs: {
        "xs": 24,
        "sm": 24,
        "md": 24,
        "lg": 24
      }
    }, [_c('el-row', {
      attrs: {
        "gutter": 5
      }
    }, _vm._l((row.columns), function(column, columnIndex) {
      return (column.id > 0 || column.id == null) ? _c('el-col', {
        key: column.id,
        staticClass: "layoutEditorColumn",
        attrs: {
          "xs": column.xs,
          "sm": column.sm,
          "md": column.md,
          "lg": column.lg
        }
      }, [_c('draggable', {
        staticClass: "dragArea",
        staticStyle: {
          "min-height": "50px"
        },
        attrs: {
          "options": {
            group: 'layoutEditor'
          }
        },
        model: {
          value: (column.specs),
          callback: function($$v) {
            _vm.$set(column, "specs", $$v)
          },
          expression: "column.specs"
        }
      }, _vm._l((column.specs), function(specColumn, specColumnIndex) {
        return _c('div', _vm._l((_vm.specs), function(specData, specDataIndex) {
          return (specData.hash == specColumn.hash) ? _c('el-card', {
            key: specData.id,
            staticClass: "el-col el-col-24 el-col-xs-24 el-col-sm-24 el-col-md-24 el-col-lg-24"
          }, [_c('div', [_c('div', {
            staticClass: "clearfix",
            staticStyle: {
              "border-bottom": "1px solid #ddd",
              "margin-bottom": "15px"
            },
            attrs: {
              "slot": "header"
            },
            slot: "header"
          }, [_c('span', {
            style: ({
              color: specColumn.titleColor
            })
          }, [_c('i', {
            class: _vm.getItemIconClass(specColumn.icon),
            attrs: {
              "aria-hidden": "true"
            }
          }), _vm._v(" "), _c('span', {
            staticStyle: {
              "width": "70%"
            },
            style: ({
              color: specColumn.titleColor
            })
          }, [_vm._v("\n                        " + _vm._s(specColumn.label) + "\n                      ")]), _vm._v(" "), _c('el-button', {
            staticClass: "specDetailsBtn",
            attrs: {
              "type": "text"
            },
            on: {
              "click": function($event) {
                column.specs.splice(specColumnIndex, 1)
              }
            }
          }, [_c('i', {
            staticClass: "sfa fa fa-trash"
          })]), _vm._v(" "), _c('el-dropdown', {
            staticStyle: {
              "margin-top": "6px",
              "float": "right"
            },
            on: {
              "command": _vm.addField
            }
          }, [_c('span', {
            staticClass: "el-dropdown-link"
          }, [_c('i', {
            staticClass: "el-icon-circle-plus-outline buttonSpec"
          })]), _vm._v(" "), _c('el-dropdown-menu', {
            attrs: {
              "slot": "dropdown"
            },
            slot: "dropdown"
          }, [_vm._l((specData.entity.fields), function(field, entityIndex) {
            return _c('el-dropdown-item', {
              key: field.id,
              attrs: {
                "command": {
                  specData: specData,
                  fields: [field]
                }
              }
            }, [_vm._v(_vm._s(field.name))])
          }), _vm._v(" "), _c('el-dropdown-item', {
            attrs: {
              "divided": "",
              "command": {
                specData: specData,
                fields: specData.entity.fields
              }
            }
          }, [_vm._v("Agregar Todos")])], 2)], 1), _vm._v(" "), _c('el-button', {
            staticClass: "specDetailsBtn",
            attrs: {
              "type": "text"
            },
            on: {
              "click": function($event) {
                _vm.activeSpec = specColumn;
                _vm.specOptions = true;
              }
            }
          }, [_c('i', {
            staticClass: "sfa fa fa-sliders"
          })])], 1)]), _vm._v(" "), _c('draggable', {
            staticClass: "dragArea",
            attrs: {
              "options": {
                group: specData.hash
              }
            },
            model: {
              value: (specData.fields),
              callback: function($$v) {
                _vm.$set(specData, "fields", $$v)
              },
              expression: "specData.fields"
            }
          }, _vm._l((specData.fields), function(field, entityIndex) {
            return _c('el-tag', {
              key: field.id,
              staticClass: "fieldElement",
              staticStyle: {
                "width": "100%",
                "padding": "10px",
                "display": "inline-table",
                "margin-bottom": "3px"
              },
              attrs: {
                "closable": "",
                "disable-transitions": true,
                "size": "small"
              },
              on: {
                "close": function($event) {
                  _vm.removeField(specData, field)
                }
              }
            }, [_vm._v("\n                          " + _vm._s(field.name))])
          }))], 1)]) : _vm._e()
        }))
      })), _vm._v(" "), _c('el-dropdown', {
        staticStyle: {
          "margin-top": "6px",
          "float": "right"
        },
        on: {
          "command": _vm.addSpec
        }
      }, [_c('span', {
        staticClass: "el-dropdown-link"
      }, [_c('i', {
        staticClass: "el-icon-circle-plus-outline buttonSpec"
      })]), _vm._v(" "), _c('el-dropdown-menu', {
        attrs: {
          "slot": "dropdown"
        },
        slot: "dropdown"
      }, _vm._l((_vm.layout.relatedEntities), function(relEntity) {
        return _c('el-dropdown-item', {
          key: relEntity.id,
          attrs: {
            "command": {
              entity: relEntity,
              column: column
            }
          }
        }, [_vm._v(_vm._s(relEntity.name) + "\n              ")])
      }))], 1)], 1) : _vm._e()
    }))], 1)], 1)
  }), _vm._v(" "), _c('el-popover', {
    ref: "savePrompt",
    attrs: {
      "placement": "top",
      "width": "250"
    },
    model: {
      value: (_vm.actions.save.prompt),
      callback: function($$v) {
        _vm.$set(_vm.actions.save, "prompt", $$v)
      },
      expression: "actions.save.prompt"
    }
  }, [_c('p', [_vm._v("¿Confirma guardar los cambios?")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "text-align": "right",
      "margin": "0"
    }
  }, [_c('el-button', {
    attrs: {
      "size": "mini",
      "type": "text"
    },
    on: {
      "click": function($event) {
        _vm.actions.save.prompt = false
      }
    }
  }, [_vm._v("Cancelar")]), _vm._v(" "), _c('el-button', {
    attrs: {
      "type": "primary",
      "size": "mini"
    },
    on: {
      "click": _vm.save
    }
  }, [_vm._v("Confirmar")])], 1)]), _vm._v(" "), _c('el-popover', {
    ref: "resetPrompt",
    attrs: {
      "placement": "top",
      "width": "250"
    },
    model: {
      value: (_vm.actions.reset.prompt),
      callback: function($$v) {
        _vm.$set(_vm.actions.reset, "prompt", $$v)
      },
      expression: "actions.reset.prompt"
    }
  }, [_c('p', [_vm._v("¿Confirma descartar los cambios?")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "text-align": "right",
      "margin": "0"
    }
  }, [_c('el-button', {
    attrs: {
      "size": "mini",
      "type": "text"
    },
    on: {
      "click": function($event) {
        _vm.actions.reset.prompt = false
      }
    }
  }, [_vm._v("Cancelar")]), _vm._v(" "), _c('el-button', {
    attrs: {
      "type": "primary",
      "size": "mini"
    },
    on: {
      "click": _vm.reset
    }
  }, [_vm._v("Descartar cambios")])], 1)]), _vm._v(" "), _c('el-button-group', [_c('el-button', {
    staticClass: "columnIcon",
    attrs: {
      "type": "primary",
      "size": "small"
    },
    on: {
      "click": function($event) {
        _vm.activeRow = _vm.layout.rows[0];
        _vm.columnOptions = true;
      }
    }
  }, [_c('i', {
    staticClass: "sfa fa fa-columns"
  })]), _vm._v(" "), _c('el-button', {
    directives: [{
      name: "popover",
      rawName: "v-popover:resetPrompt",
      arg: "resetPrompt"
    }],
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-refresh"
    }
  }), _vm._v(" "), _c('el-button', {
    directives: [{
      name: "popover",
      rawName: "v-popover:savePrompt",
      arg: "savePrompt"
    }],
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-check"
    }
  })], 1)], 2)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-c47e76f2", esExports)
  }
}

/***/ }),
/* 236 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__style_component_css__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__style_component_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__style_component_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__src_main_vue__ = __webpack_require__(239);



/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_1__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_1__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_1__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_1__src_main_vue__["a" /* default */]);


/***/ }),
/* 237 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(238);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(10)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../../../css-loader/index.js!../../../../postcss-loader/lib/index.js!./component.css", function() {
			var newContent = require("!!../../../../css-loader/index.js!../../../../postcss-loader/lib/index.js!./component.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 238 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(undefined);
// imports


// module
exports.push([module.i, ".buttonSpec {\n    font-size: 20px;\n    cursor: pointer;\n    color: #3b4241;\n   }\n.fieldElement{\n  position: relative;\n  border-radius: 0px;\n  border: 1px solid #c4dad7;\n  background-color: #f7fcfa;\n  color: #4e8c89;\n}\n\n.fieldElement .el-icon-close{\n  position: absolute !important;\n  float: right;\n  margin-top: 14px;\n  margin-right: 11px;\n}\n\n.fieldElement .el-icon-close:hover{\n  background-color: #6D9690;\n}\n.addColumnBtn{\n\n}\n.layoutEditor .el-dialog__wrapper .el-dialog .el-dialog__body {\n    padding: 0px 20px;width:\n}\n\n.specDetailsBtn{\n  float: right;\n  right: 0px;\n  color: #3b4241;\n  background-color: transparent;\n  font-size: 18px;\n  padding: 0px;\n}\n.specDetailsBtn:active{\n  color: #09725f;\n  background-color: transparent;\n  padding: 0px;\n}\n.specDetailsBtn:hover{\n  color: #09725f;\n  background-color: transparent;\n  padding: 0px;\n}\n.specDetailsBtn:focus{\n  color: #09725f;\n  background-color: transparent;\n  padding: 0px;\n}\n.specDetailsColumnBtn {\n  padding: 0px;\n}\n\n.layoutEditorRow {\n  background-color: transparent;\n  min-height: 50px;\n}\n\n.layoutEditorColumn {\n  background-color: #e3e3e3;\n  min-height: 50px;\n  padding: 6px !important;\n  border: 2px solid #fff !important;\n}\n\n.trashSection {\n  margin-left: 50%;\n  position: relative;\n  left: -13px;\n}\n\n.trashIcon{\n  color: #d24d29;\n  font-size: 22px;\n}\n\n.trashSection div {\n  visibility: hidden;\n  display: none;\n}\n\n.columnIcon{\n  padding: 1px 6px;\n}\n\n.fieldFormatSelect .el-tag__close {\n  display: none\n}\n.fieldFormatSelect .el-tag:hover .el-tag__close {\n  display: inline-block\n}", ""]);

// exports


/***/ }),
/* 239 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_controller_js__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_controller_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_controller_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1fea346f_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(242);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */
/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_controller_js___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1fea346f_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/layout-service-editor/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1fea346f", Component.options)
  } else {
    hotAPI.reload("data-v-1fea346f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 240 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _vuex = __webpack_require__(6);

var _data = __webpack_require__(241);

var _data2 = _interopRequireDefault(_data);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'LayoutServiceEditor',

  props: ["spec", "entity", "layoutTypeName"],

  data: function data() {
    return (0, _data2.default)();
  },

  created: function created() {
    this.getLayout();
  },

  watch: {
    _specHash: function _specHash(value) {
      this.entity.specHash = value;
      this.getLayout();
    },
    deduppingOptions: function deduppingOptions(value) {
      this.extras.dedupping = value;
    }
  },

  computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl']), {
    _specHash: function _specHash() {
      return this.entity.specHash;
    },
    deduppingOptions: function deduppingOptions() {
      return this.extras.dedupping;
    }
  }),

  methods: {
    clone: function clone(obj) {
      return JSON.parse(JSON.stringify(obj));
    },

    showFilters: function showFilters(specHash) {
      this.filtersVisibility = true;
    },

    getFieldData: function getFieldData(entity, fieldId) {
      return entity[fieldId];
    },

    getFieldsForDedupping: function getFieldsForDedupping() {
      var fieldOptions = [];
      var instance = this;
      instance.extras.dedupping.forEach(function (rEntity) {
        rEntity.fields = [];
        var selected = null;
        instance.specs.forEach(function (specData) {
          specData.fields.forEach(function (field) {
            if (field.modelId == rEntity.id) {
              var addField = {
                id: field.id,
                name: field.name
              };
              if (field.id == rEntity.fieldSelected) {
                selected = field.id;
              }
              rEntity.fields.push(addField);
            }
          });
        });
        rEntity.fields.sort(instance.sortFields);
        rEntity.fieldSelected = selected;
      });
    },

    getItemIconClass: function getItemIconClass(icon) {
      return "sfa fa fa-" + icon + " icon";
    },


    removeField: function removeField(specData, field) {
      specData.entity.fields.push(field);
      for (var i = specData.fields.length - 1; i >= 0; i--) {
        if (specData.fields[i].id == field.id) {
          specData.fields.splice(i, 1);
        }
      }
      specData.entity.fields.sort(this.sortFields);
      this.getFieldsForDedupping();
    },

    getLayout: _.debounce(function () {
      if (this._specHash) {
        var instance = this;
        var now = new Date().toLocaleString();
        axios.get(instance.baseUrl + 'layout/editLayout/' + instance._specHash + '/' + instance.layoutTypeName + '?' + now).then(function (response) {
          //instance.layout = response.data;
          if (response.data.layout) {
            instance.specsOriginalValues = [];
            instance.layout = response.data.layout;
            instance.layoutOriginalValues = instance.clone(instance.layout);
            instance.layout.rows.forEach(function (row) {
              row.columns.forEach(function (column) {
                column.specs.forEach(function (spec) {
                  instance.getSpec(spec.specUrl);
                  //instance.getData(spec.dataUrl)
                });
              });
            });
            instance.layout.relatedEntities.forEach(function (rEntity) {
              var deduppingEntity = {
                id: rEntity.id,
                name: rEntity.name,
                fields: [],
                fieldSelected: null
              };
              instance.extras.dedupping.push(deduppingEntity);
            });
          }
        }).catch(function (error) {
          //instance.answer = 'Error! Could not reach the API. ' + error
        });
      }
    }, 0),

    getSpec: function getSpec(url) {
      if (this._specHash) {
        var instance = this;
        var now = new Date().toLocaleString();
        axios.get(instance.baseUrl + url + '?' + now).then(function (response) {
          try {
            response.data.entity.fields.sort(instance.sortFields);
            var specHash = response.data.hash;
            var inArray = instance.specs.find(function (spec) {
              return spec.hash == specHash;
            });
            if (inArray === undefined) {
              instance.specs.push(response.data);
            }
            instance.specsOriginalValues.push(instance.clone(response.data));
            instance.getFieldsForDedupping();
          } catch (err) {
            console.log('-' + err);
          }
        }).catch(function (error) {
          console.log(error);
          //instance.answer = 'Error! Could not reach the API. ' + error
        });
      }
    },

    sortFields: function compare(a, b) {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    },

    addField: function addField(obj) {
      for (var f = obj.fields.length - 1; f >= 0; f--) {
        var fieldId = obj.fields[f].id;
        obj.specData.fields.push(obj.fields[f]);
        for (var i = obj.specData.entity.fields.length - 1; i >= 0; i--) {
          if (obj.specData.entity.fields[i].id == fieldId) {
            obj.specData.entity.fields.splice(i, 1);
          }
        }
      }
      obj.specData.entity.fields.sort(this.sortFields);
      this.getFieldsForDedupping();
    },

    addColumn: function addColumn() {
      var newColumn = {
        id: null,
        xs: 3,
        sm: 3,
        md: 3,
        lg: 3,
        specs: []
      };
      this.activeRow.columns.push(newColumn);
      console.log(JSON.stringify(this.activeRow));
    },

    addSpec: function addSpec(obj) {
      var instance = this;
      var now = new Date().toLocaleString();
      var newSpec = {
        entityId: obj.entity.id,
        columnId: obj.column.id
      };
      axios.post(instance.baseUrl + 'layout/addSpec/', encodeURIComponent(JSON.stringify(newSpec))).then(function (response) {
        obj.column.specs.push(response.data);
        instance.getSpec(response.data.specUrl);
      }).catch(function (error) {
        //instance.answer = 'Error! Could not reach the API. ' + error
      });
    },

    removeColumn: function removeColumn(column, index) {
      if (column.id > 0) {
        column.id *= -1;
      } else {
        this.activeRow.columns.splice(index, 1);
      }
    },


    reset: function reset() {
      this.actions.reset.prompt = false;
      this.layout = this.clone(this.layoutOriginalValues);
      this.specs = this.clone(this.specsOriginalValues);
      this.getFieldsForDedupping();
    },

    save: _.debounce(function () {
      this.actions.save.prompt = false;
      var instance = this;
      this.specs.forEach(function (spec) {
        if (spec.hash) {
          var original = instance.specsOriginalValues.find(function (originalSpec) {
            return originalSpec.hash == spec.hash;
          });
          if (JSON.stringify(spec) != JSON.stringify(original)) {
            var newData = instance.clone(spec);
            delete newData.entity.fields;
            /*newData.fields.forEach(function(field){
              delete field.values;
            })*/
            //console.log(JSON.stringify(newData))
            axios.post(instance.baseUrl + instance.actions.save.urlSpec, encodeURIComponent(JSON.stringify(newData))).then(function (response) {
              instance.$notify({
                type: 'success',
                message: response.data.msg,
                position: 'bottom-right'
              });
            }).catch(function (error) {
              instance.$notify({
                type: 'error',
                message: 'Error al guardar sección ' + spec.label + ', intente nuevamente',
                position: 'bottom-right'
              });
            });
          }
        }
      });
      axios.post(instance.baseUrl + instance.actions.save.urlLayout, encodeURIComponent(JSON.stringify(instance.layout))).then(function (response) {
        instance.$notify({
          type: 'success',
          message: response.data.msg,
          position: 'bottom-right'
        });
        instance.getLayout();
      }).catch(function (error) {
        instance.$notify({
          type: 'error',
          message: 'Layut save error',
          position: 'bottom-right'
        });
      });
    }, 100)
  }
};

/***/ }),
/* 241 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["default"] = dataModel;
function dataModel() {
  return {
        columnTabs: [
          {
            icon: 'desktop',
            prop: 'lg'
          },
          {
            icon: 'laptop',
            prop: 'md'
          },
          {
            icon: 'tablet',
            prop: 'sm'
          },
          {
            icon: 'phone',
            prop: 'xs'
          }
        ],
        activeRow: {
          columns:[]
        },
        columnOptions: false,
        activeSpec: {},
        specOptions: false,
        removedObjects: [],
        showTrashSpec: false,
        colWidthValues:[
          {label:"100%", value:24},
          {label:"87.5%", value:21},
          {label:"75%", value:18},
          {label:"62.5%", value:15},
          {label:"50%", value:12},
          {label:"37.5%", value:9},
          {label:"25%", value:6},
          {label:"12.5%", value:3},
          {label:"Ocultar", value:0}
        ],
        colWidthMobileValues:[
          {label:"Mostrar", value:24},
          {label:"Ocultar", value:0}
        ],
        actions: {
          save: {
            prompt: false,
            urlSpec: 'layout/saveSpec/',
            urlLayout: 'layout/saveLayout/'
          },
          reset: {
            prompt: false
          }
        },
        filtersVisibility: false,
        specsOriginalValues: [],
        specs: [],
        extras: {
          dedupping: []
        },
        layoutOriginalValues:{
        },
        layout:{
          header: {
            type: "",
          },
          filters :{},
          type: '',
          rows:[
            {
              columns:[]
            }
          ],
          relatedEntities:[]
        },
        icons:[
          {label:'Calendario / Fecha',name:'calendar'},
          {label:'Reloj / Tiempo',name:'clock-o'},
          {label:'group',name:'group'},
          {label:'link',name:'link'},
          {label:'files-o',name:'files-o'},
          {label:'folder', name:'folder'},
          {label:'arrows-v', name:'arrows-v'},
          {label:'arrows-h', name:'arrows-h'},
          {label:'bar-chart-o', name:'bar-chart-o'},
          {label:'list-ul',name:'list-ul'},
          {label:'dashboard',name:'dashboard'},
          {label:'comments-o',name:'comments-o'},
          {label:'sitemap',name:'sitemap'},
          {label:'circle-o',name:'circle-o'},
          {label:'quote-left',name:'quote-left'},
          {label:'quote-right',name:'quote-right'},
          {label:'circle',name:'circle'},
          {label:'reply',name:'reply'},
          {label:'flag-o',name:'flag-o'},
          {label:'question',name:'question'},
          {label:'info',name:'info'},
          {label:'exclamation',name:'exclamation'},
          {label:'puzzle-piece',name:'puzzle-piece'},
          {label:'sun-o', name:'sun-o'},
          {label:'moon-o', name:'moon-o'},
          {label:'archive', name:'archive'},
          {label:'database', name:'database'},
          {label:'support', name:'support'},
          {label:'trash', name:'trash'},
          {label:'ship', name:'ship'},
          {label:'user-secret', name:'user-secret'},
          {label:'motorcycle', name:'motorcycle'},
          {label:'server', name:'server'},
          {label:'map-o', name:'map-o'},
          {label:'address-book', name:'address-book'},
          {label:'bookmark', name:'bookmark'},
          {label:'share', name:'share'},
          {label:'asterisk', name:'asterisk'},
          {label:'warning', name:'warning'}
        ]
      }
    }

/***/ }),
/* 242 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "layoutEditor"
  }, [_vm._l((_vm.layout.rows), function(row, rowIndex) {
    return _c('el-row', {
      key: row.id,
      staticClass: "layoutEditorRow",
      attrs: {
        "gutter": 5
      }
    }, [_c('el-col', {
      attrs: {
        "xs": 24,
        "sm": 24,
        "md": 24,
        "lg": 24
      }
    }, [_c('el-row', {
      attrs: {
        "gutter": 5
      }
    }, _vm._l((row.columns), function(column, columnIndex) {
      return (column.id > 0 || column.id == null) ? _c('el-col', {
        key: column.id,
        staticClass: "layoutEditorColumn",
        attrs: {
          "xs": column.xs,
          "sm": column.sm,
          "md": column.md,
          "lg": column.lg
        }
      }, [_c('draggable', {
        staticClass: "dragArea",
        staticStyle: {
          "min-height": "50px"
        },
        attrs: {
          "options": {
            group: 'layoutEditor'
          }
        },
        model: {
          value: (column.specs),
          callback: function($$v) {
            _vm.$set(column, "specs", $$v)
          },
          expression: "column.specs"
        }
      }, _vm._l((column.specs), function(specColumn, specColumnIndex) {
        return _c('div', _vm._l((_vm.specs), function(specData, specDataIndex) {
          return (specData.hash == specColumn.hash) ? _c('el-card', {
            key: specData.id,
            staticClass: "el-col el-col-24 el-col-xs-24 el-col-sm-24 el-col-md-24 el-col-lg-24"
          }, [_c('div', [_c('draggable', {
            staticClass: "dragArea",
            attrs: {
              "options": {
                group: specData.hash
              }
            },
            model: {
              value: (specData.fields),
              callback: function($$v) {
                _vm.$set(specData, "fields", $$v)
              },
              expression: "specData.fields"
            }
          }, _vm._l((specData.fields), function(field, entityIndex) {
            return _c('div', {
              key: field.id
            }, [_c('el-tag', {
              staticClass: "fieldElement",
              staticStyle: {
                "width": "100%",
                "padding": "10px",
                "display": "inline-table",
                "margin-bottom": "3px"
              },
              attrs: {
                "closable": "",
                "disable-transitions": true,
                "size": "small"
              },
              on: {
                "close": function($event) {
                  _vm.removeField(specData, field)
                }
              }
            }, [_vm._v("\n                            " + _vm._s(field.name) + "\n                        ")]), _vm._v(" "), ((field.type == 1)) ? _c('el-select', {
              staticClass: "fieldFormatSelect",
              staticStyle: {
                "margin-left": "6%",
                "width": "94%"
              },
              attrs: {
                "multiple": "",
                "filterable": "",
                "allow-create": "",
                "placeholder": "Format"
              },
              model: {
                value: (field.fieldFormat.format),
                callback: function($$v) {
                  _vm.$set(field.fieldFormat, "format", $$v)
                },
                expression: "field.fieldFormat.format"
              }
            }, _vm._l((field.fieldFormat.entityFields), function(item) {
              return _c('el-option', {
                key: item.id,
                attrs: {
                  "label": item.name,
                  "value": item.id
                }
              })
            })) : _vm._e()], 1)
          })), _vm._v(" "), _c('el-button', {
            staticClass: "specDetailsBtn",
            attrs: {
              "type": "text"
            },
            on: {
              "click": function($event) {
                column.specs.splice(specColumnIndex, 1)
              }
            }
          }, [_c('i', {
            staticClass: "sfa fa fa-trash"
          })]), _vm._v(" "), _c('el-dropdown', {
            staticStyle: {
              "margin-top": "6px",
              "float": "right"
            },
            on: {
              "command": _vm.addField
            }
          }, [_c('span', {
            staticClass: "el-dropdown-link"
          }, [_c('i', {
            staticClass: "el-icon-circle-plus-outline buttonSpec"
          })]), _vm._v(" "), _c('el-dropdown-menu', {
            attrs: {
              "slot": "dropdown"
            },
            slot: "dropdown"
          }, [_vm._l((specData.entity.fields), function(field, entityIndex) {
            return _c('el-dropdown-item', {
              key: field.id,
              attrs: {
                "command": {
                  specData: specData,
                  fields: [field]
                }
              }
            }, [_vm._v(_vm._s(field.name))])
          }), _vm._v(" "), _c('el-dropdown-item', {
            attrs: {
              "divided": "",
              "command": {
                specData: specData,
                fields: specData.entity.fields
              }
            }
          }, [_vm._v("Agregar Todos")])], 2)], 1)], 1)]) : _vm._e()
        }))
      })), _vm._v(" "), _c('el-dropdown', {
        staticStyle: {
          "margin-top": "6px",
          "float": "right"
        },
        on: {
          "command": _vm.addSpec
        }
      }, [_c('span', {
        staticClass: "el-dropdown-link"
      }, [_c('i', {
        staticClass: "el-icon-circle-plus-outline buttonSpec"
      })]), _vm._v(" "), _c('el-dropdown-menu', {
        attrs: {
          "slot": "dropdown"
        },
        slot: "dropdown"
      }, _vm._l((_vm.layout.relatedEntities), function(relEntity) {
        return _c('el-dropdown-item', {
          key: relEntity.id,
          attrs: {
            "command": {
              entity: relEntity,
              column: column
            }
          }
        }, [_vm._v(_vm._s(relEntity.name) + "\n              ")])
      }))], 1)], 1) : _vm._e()
    })), _vm._v(" "), _c('el-row', {
      staticStyle: {
        "margin-top": "22px"
      }
    }, [_c('el-col', [_c('el-card', {
      staticClass: "el-col el-col-24 el-col-xs-24 el-col-sm-24 el-col-md-24 el-col-lg-24 serviceOptions"
    }, [_c('div', {
      staticClass: "clearfix",
      staticStyle: {
        "display": "inherit !important"
      },
      attrs: {
        "slot": "header"
      },
      slot: "header"
    }, [_c('span', [_vm._v("Deduplicación")])]), _vm._v(" "), _c('el-form', {
      attrs: {
        "label-position": "right"
      },
      nativeOn: {
        "submit": function($event) {
          $event.preventDefault();
        }
      }
    }, _vm._l((_vm.deduppingOptions), function(entity, entityIndex) {
      return _c('el-form-item', {
        key: entity.id,
        attrs: {
          "label": entity.name
        }
      }, [_c('el-select', {
        attrs: {
          "filterable": "",
          "placeholder": "Select"
        },
        model: {
          value: (entity.fieldSelected),
          callback: function($$v) {
            _vm.$set(entity, "fieldSelected", $$v)
          },
          expression: "entity.fieldSelected"
        }
      }, _vm._l((entity.fields), function(field, entityIndex) {
        return _c('el-option', {
          key: field.id,
          attrs: {
            "label": field.name,
            "value": field.id
          }
        })
      }))], 1)
    }))], 1)], 1)], 1)], 1)], 1)
  }), _vm._v(" "), _c('el-popover', {
    ref: "savePrompt",
    attrs: {
      "placement": "top",
      "width": "250"
    },
    model: {
      value: (_vm.actions.save.prompt),
      callback: function($$v) {
        _vm.$set(_vm.actions.save, "prompt", $$v)
      },
      expression: "actions.save.prompt"
    }
  }, [_c('p', [_vm._v("¿Confirma guardar los cambios?")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "text-align": "right",
      "margin": "0"
    }
  }, [_c('el-button', {
    attrs: {
      "size": "mini",
      "type": "text"
    },
    on: {
      "click": function($event) {
        _vm.actions.save.prompt = false
      }
    }
  }, [_vm._v("Cancelar")]), _vm._v(" "), _c('el-button', {
    attrs: {
      "type": "primary",
      "size": "mini"
    },
    on: {
      "click": _vm.save
    }
  }, [_vm._v("Confirmar")])], 1)]), _vm._v(" "), _c('el-popover', {
    ref: "resetPrompt",
    attrs: {
      "placement": "top",
      "width": "250"
    },
    model: {
      value: (_vm.actions.reset.prompt),
      callback: function($$v) {
        _vm.$set(_vm.actions.reset, "prompt", $$v)
      },
      expression: "actions.reset.prompt"
    }
  }, [_c('p', [_vm._v("¿Confirma descartar los cambios?")]), _vm._v(" "), _c('div', {
    staticStyle: {
      "text-align": "right",
      "margin": "0"
    }
  }, [_c('el-button', {
    attrs: {
      "size": "mini",
      "type": "text"
    },
    on: {
      "click": function($event) {
        _vm.actions.reset.prompt = false
      }
    }
  }, [_vm._v("Cancelar")]), _vm._v(" "), _c('el-button', {
    attrs: {
      "type": "primary",
      "size": "mini"
    },
    on: {
      "click": _vm.reset
    }
  }, [_vm._v("Descartar cambios")])], 1)]), _vm._v(" "), _c('el-button-group', [_c('el-button', {
    directives: [{
      name: "popover",
      rawName: "v-popover:resetPrompt",
      arg: "resetPrompt"
    }],
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-refresh"
    }
  }), _vm._v(" "), _c('el-button', {
    directives: [{
      name: "popover",
      rawName: "v-popover:savePrompt",
      arg: "savePrompt"
    }],
    attrs: {
      "type": "primary",
      "size": "small",
      "icon": "el-icon-check"
    }
  })], 1)], 2)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1fea346f", esExports)
  }
}

/***/ }),
/* 243 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(244);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 244 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_e08917f6_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(246);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_e08917f6_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/text-field/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e08917f6", Component.options)
  } else {
    hotAPI.reload("data-v-e08917f6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 245 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  name: 'TextField',

  props: ["field", "entity", "disabled"],

  data: function data() {
    return {
      subtype: {
        text: [5, 'text'],
        textarea: [6, 7, 'textarea'],
        label: ['label']
      }
    };
  },
  methods: {
    isSubType: function isSubType(field, typeCompare) {
      var subType = field.subtype ? field.subtype : field.type;
      return _.indexOf(typeCompare, subType) > -1;
    },
    emitSave: function emitSave() {
      this.$emit('save', true);
    },
    emitReset: function emitReset() {
      this.$emit('reset', true);
    }
  }
};

/***/ }),
/* 246 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "text-field"
  }, [(_vm.isSubType(_vm.field, _vm.subtype.text)) ? _c('el-input', {
    attrs: {
      "type": "text",
      "placeholder": _vm.field.placeholder,
      "disabled": _vm.disabled
    },
    on: {
      "blur": function($event) {
        _vm.emitSave()
      }
    },
    nativeOn: {
      "keyup": [function($event) {
        if (!('button' in $event) && _vm._k($event.keyCode, "enter", 13, $event.key)) { return null; }
        _vm.emitSave()
      }, function($event) {
        if (!('button' in $event) && _vm._k($event.keyCode, "esc", 27, $event.key)) { return null; }
        _vm.emitReset()
      }]
    },
    model: {
      value: (_vm.entity[_vm.field.id]),
      callback: function($$v) {
        _vm.$set(_vm.entity, _vm.field.id, $$v)
      },
      expression: "entity[field.id]"
    }
  }) : _vm._e(), _vm._v(" "), (_vm.isSubType(_vm.field, _vm.subtype.textarea)) ? _c('el-input', {
    attrs: {
      "type": "textarea",
      "autosize": {
        minRows: 3,
        maxRows: 20
      },
      "placeholder": _vm.field.placeholder,
      "disabled": _vm.disabled
    },
    on: {
      "blur": function($event) {
        _vm.emitSave()
      }
    },
    nativeOn: {
      "keyup": function($event) {
        if (!('button' in $event) && _vm._k($event.keyCode, "esc", 27, $event.key)) { return null; }
        _vm.emitReset()
      }
    },
    model: {
      value: (_vm.entity[_vm.field.id]),
      callback: function($$v) {
        _vm.$set(_vm.entity, _vm.field.id, $$v)
      },
      expression: "entity[field.id]"
    }
  }) : _vm._e(), _vm._v(" "), (_vm.isSubType(_vm.field, _vm.subtype.label)) ? _c('label', {
    staticClass: "el-form-item__label"
  }, [_vm._v("\n    " + _vm._s(_vm.entity[_vm.field.id]) + "\n  ")]) : _vm._e()], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-e08917f6", esExports)
  }
}

/***/ }),
/* 247 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(248);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 248 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(249);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_0dab8fe4_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(250);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_0dab8fe4_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/date-field/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0dab8fe4", Component.options)
  } else {
    hotAPI.reload("data-v-0dab8fe4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 249 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  name: 'DateField',

  props: ["field", "entity"],

  data: function data() {
    return {
      subtype: {
        date: [8, 'date'],
        datetime: [10, 'datetime']
      }
    };
  },

  methods: {
    isSubType: function isSubType(field, typeCompare) {
      var subType = field.subtype ? field.subtype : field.type;
      return _.indexOf(typeCompare, subType) > -1;
    },
    emitSave: function emitSave() {
      this.$emit('save', true);
    },
    emitReset: function emitReset() {
      this.$emit('reset', true);
    }
  }
};

/***/ }),
/* 250 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "text-field"
  }, [(_vm.isSubType(_vm.field, _vm.subtype.date)) ? _c('el-date-picker', {
    attrs: {
      "type": "date",
      "placeholder": _vm.field.placeholder,
      "format": "dd/MM/yyyy",
      "value-format": "yyyy-MM-dd"
    },
    on: {
      "blur": function($event) {
        _vm.emitSave()
      },
      "change": function($event) {
        _vm.emitSave()
      }
    },
    model: {
      value: (_vm.entity[_vm.field.id]),
      callback: function($$v) {
        _vm.$set(_vm.entity, _vm.field.id, $$v)
      },
      expression: "entity[field.id]"
    }
  }) : _vm._e(), _vm._v(" "), (_vm.isSubType(_vm.field, _vm.subtype.datetime)) ? _c('el-date-picker', {
    attrs: {
      "type": "datetime",
      "placeholder": _vm.field.placeholder,
      "format": "dd/MM/yyyy",
      "value-format": "yyyy-MM-dd"
    },
    on: {
      "blur": function($event) {
        _vm.emitSave()
      },
      "change": function($event) {
        _vm.emitSave()
      }
    },
    model: {
      value: (_vm.data.value),
      callback: function($$v) {
        _vm.$set(_vm.data, "value", $$v)
      },
      expression: "data.value"
    }
  }) : _vm._e()], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0dab8fe4", esExports)
  }
}

/***/ }),
/* 251 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(252);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 252 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_95eea92e_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(254);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_95eea92e_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/number-field/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-95eea92e", Component.options)
  } else {
    hotAPI.reload("data-v-95eea92e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 253 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//

exports.default = {
  name: 'NumberField',

  props: ["field", "entity"],

  data: function data() {
    return {
      controls: false,
      subtype: {
        int: 'int',
        decimal: 'decimal'
      }
    };
  }
};

/***/ }),
/* 254 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "entity-field"
  }, [(_vm.field.subtype === _vm.subtype.int) ? _c('el-input-number', {
    staticClass: "form-control",
    attrs: {
      "controls": _vm.controls,
      "placeholder": _vm.field.placeholder
    },
    model: {
      value: (_vm.entity[_vm.field.id]),
      callback: function($$v) {
        _vm.$set(_vm.entity, _vm.field.id, $$v)
      },
      expression: "entity[field.id]"
    }
  }) : _vm._e(), _vm._v(" "), (_vm.field.subtype === _vm.subtype.decimal) ? _c('el-input-number', {
    staticClass: "form-control",
    attrs: {
      "controls": _vm.controls,
      "placeholder": _vm.field.placeholder
    },
    model: {
      value: (_vm.entity[_vm.field.id]),
      callback: function($$v) {
        _vm.$set(_vm.entity, _vm.field.id, $$v)
      },
      expression: "entity[field.id]"
    }
  }) : _vm._e()], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-95eea92e", esExports)
  }
}

/***/ }),
/* 255 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(256);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 256 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_ed5ad254_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(258);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_ed5ad254_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/select-field/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ed5ad254", Component.options)
  } else {
    hotAPI.reload("data-v-ed5ad254", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 257 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var _vuex = __webpack_require__(6);

exports.default = {
  name: 'SelectField',

  props: ["field", "entity", "disabled"],

  data: function data() {
    return {
      appendToBody: true,
      subtype: {
        single: 'single',
        grouping: 'grouping'
      },
      type: {
        text: 'text',
        number: 'number',
        select: 'select',
        check: 'check',
        switch: 'switch'
      },
      allOptions: [],
      ready: false
    };
  },

  mounted: function mounted() {
    if (this.field.remoteUrl) {
      var instance = this;
      var value = this.entity[this.field.id] > 0 ? this.entity[this.field.id] : '';
      axios.get(instance.baseUrl + instance.field.remoteUrl + '/' + value).then(function (response) {
        instance.setOptions(response.data);
      });
    } else {
      this.ready = true;
    }
  },

  computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl']), {
    _options: {
      get: function get() {
        return this.field.options;
      },
      set: function set(value) {
        this.field.options = value;
      }
    },
    _dataValue: function _dataValue() {
      return this.entity[this.field.id];
    }
  }),

  methods: {
    clone: function clone(obj) {
      return JSON.parse(JSON.stringify(obj));
    },

    emitSave: function emitSave() {
      this.$emit('save', true);
    },

    emitReset: function emitReset() {
      this.$emit('reset', true);
    },

    setOptions: function setOptions(options) {
      if (this.field.subtype === this.subtype.grouping) {
        this.field.groups = options;
      } else {
        //this._options = options;
        this.allOptions = options;
        this.field.options = options;
      }
      this.ready = true;
      this.$forceUpdate();
    },

    remoteMethod: _.throttle(function (query) {
      var instance = this;
      if (query !== '') {
        this.loading = false;
        axios.get(instance.baseUrl + instance.field.remoteUrl + '/' + query).then(function (response) {
          instance.setOptions(response.data);
        }).catch(function (error) {
          console.log(error);
          //instance.answer = 'Error! Could not reach the API. ' + error
        });
      } else {
        var value = this.entity[this.field.id] > 0 ? this.entity[this.field.id] : '';
        axios.get(instance.baseUrl + instance.field.remoteUrl + '/' + value).then(function (response) {
          instance.setOptions(response.data);
        });
      }
    }, 100)
  }
};

/***/ }),
/* 258 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "entity-field"
  }, [(_vm.ready) ? _c('el-select', {
    attrs: {
      "disabled": _vm.disabled,
      "multiple": _vm.field.multiple,
      "filterable": _vm.field.filterable,
      "placeholder": _vm.field.placeholder,
      "reserve-keyword": "",
      "popper-append-to-body": _vm.appendToBody,
      "remote": _vm.field.remote,
      "remote-method": _vm.remoteMethod,
      "collapse-tags": _vm.field.subtype === _vm.subtype.orderable
    },
    on: {
      "blur": function($event) {
        _vm.emitSave()
      },
      "change": function($event) {
        _vm.emitSave()
      }
    },
    model: {
      value: (_vm.entity[_vm.field.id]),
      callback: function($$v) {
        _vm.$set(_vm.entity, _vm.field.id, $$v)
      },
      expression: "entity[field.id]"
    }
  }, [_vm._l((_vm.field.groups), function(group) {
    return (_vm.field.subtype === _vm.subtype.grouping) ? _c('el-option-group', {
      key: group.label,
      attrs: {
        "label": group.label
      }
    }, _vm._l((group.options), function(item) {
      return _c('el-option', {
        key: item.value,
        attrs: {
          "label": item.label,
          "value": item.value
        }
      })
    })) : _vm._e()
  }), _vm._v(" "), _vm._l((_vm.field.options), function(item) {
    return (_vm.field.subtype === _vm.subtype.single) ? _c('el-option', {
      key: item.value,
      attrs: {
        "label": item.label,
        "value": item.value
      }
    }) : _vm._e()
  })], 2) : _vm._e()], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-ed5ad254", esExports)
  }
}

/***/ }),
/* 259 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(260);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 260 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_6b125287_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(262);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_6b125287_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/transfer-field/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6b125287", Component.options)
  } else {
    hotAPI.reload("data-v-6b125287", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 261 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var _vuex = __webpack_require__(6);

exports.default = {
  name: 'TransferField',

  props: ["field", "entity", "disabled"],

  data: function data() {
    return {};
  },

  mounted: function mounted() {
    if (this.field.remoteUrl) {
      var instance = this;
      var value = this.entity[this.field.id] > 0 ? this.entity[this.field.id] : '';
      axios.get(instance.baseUrl + instance.field.remoteUrl + '/' + value).then(function (response) {
        instance.setOptions(response.data);
      });
    } else {
      this.ready = true;
    }
  },

  computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl'])),

  methods: {

    setOptions: function setOptions(options) {
      this.allOptions = options;
      this.field.options = options;
      this.$forceUpdate();
    },

    getFieldData: function getFieldData(values, fieldId) {
      if (fieldId > 0) {
        for (var j = 0; j < values.length; j++) {
          if (values[j].id == fieldId) {
            return values[j];
          }
        }
      }
      return {
        'id': fieldId,
        'value': null
      };
    },

    emitSave: function emitSave() {
      this.$emit('save', true);
    },

    emitReset: function emitReset() {
      this.$emit('reset', true);
    }
  }
};

/*

<style>
  .transfer-footer {
    margin-left: 20px;
    padding: 6px 5px;
  }
</style>

<script>
  export default {
    data() {
      const generateData = _ => {
        const data = [];
        for (let i = 1; i <= 15; i++) {
          data.push({
            key: i,
            label: `Option ${ i }`,
            disabled: i % 4 === 0
          });
        }
        return data;
      };
      return {
        data: generateData(),
        value3: [1],
        renderFunc(h, option) {
          return <span>{ option.key } - { option.label }</span>;
        }
      };
    },

    methods: {
      handleChange(value, direction, movedKeys) {
        console.log(value, direction, movedKeys);
      }
    }
  };
  */

/***/ }),
/* 262 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "entity-field"
  }, [_c('el-transfer', {
    attrs: {
      "filterable": "",
      "filter-placeholder": "",
      "data": _vm.field.options,
      "titles": _vm.field.titles,
      "props": {
        key: 'value',
        label: 'label'
      }
    },
    on: {
      "blur": function($event) {
        _vm.emitSave()
      },
      "change": function($event) {
        _vm.emitSave()
      }
    },
    model: {
      value: (_vm.entity[_vm.field.id]),
      callback: function($$v) {
        _vm.$set(_vm.entity, _vm.field.id, $$v)
      },
      expression: "entity[field.id]"
    }
  })], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-6b125287", esExports)
  }
}

/***/ }),
/* 263 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(264);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 264 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1948c8f4_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(266);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1948c8f4_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/list-field/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1948c8f4", Component.options)
  } else {
    hotAPI.reload("data-v-1948c8f4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 265 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; //
//
//
//
//
//
//
//
//
//
//
//

var _vuex = __webpack_require__(6);

exports.default = {
  name: 'ListField',

  props: ["url", "field", "data"],

  data: function data() {
    return {
      allOptions: []
    };
  },

  mounted: function mounted() {
    var instance = this;
    axios.get(instance.baseUrl + instance.field.remoteUrl).then(function (response) {
      instance.allOptions = response.data.list;
      instance.field.options = instance.clone(instance.allOptions);
    }).catch(function (error) {
      //instance.answer = 'Error! Could not reach the API. ' + error
    });
  },

  watch: {
    _options: function _options(value) {
      this.field.options = value;
    },
    _dataValue: function _dataValue(value) {
      this.data.value = value;
    }
  },

  computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl']), {
    _options: function _options() {
      return this.field.options;
    },
    _dataValue: function _dataValue() {
      return this.data.value;
    },
    draggableOption: function draggableOption() {
      return { 'group': this.field.remoteUrl };
    }
  }),

  methods: {
    clone: function clone(obj) {
      return JSON.parse(JSON.stringify(obj));
    },

    getItemByValue: function getItemByValue(value) {
      var item = this.allOptions.filter(function (item) {
        return item.value === value.toString();
      });
      if (item.length > 0) return item[0].label;else return '';
    },

    getFieldData: function getFieldData(values, fieldId) {
      if (fieldId > 0) {
        for (var j = 0; j < values.length; j++) {
          if (values[j].id == fieldId) {
            return values[j];
          }
        }
      }
      return {
        'id': fieldId,
        'value': null
      };
    },

    remoteMethod: function remoteMethod(query) {
      var _this = this;

      if (query !== '') {
        this.loading = true;
        setTimeout(function () {
          _this.loading = false;
          var instance = _this;
          var now = new Date().toLocaleString();
          axios.get(instance.baseUrl + instance.field.remoteUrl + '?' + now).then(function (response) {
            instance.field.options = response.data.list.filter(function (item) {
              return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
            });
          }).catch(function (error) {
            //instance.answer = 'Error! Could not reach the API. ' + error
          });
        }, 200);
      } else {
        this.field.options = this.clone(this.allOptions);
      }
    }
  }
};

/***/ }),
/* 266 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "entity-field",
    staticStyle: {
      "display": "inline-block"
    }
  }, [_c('draggable', {
    staticClass: "dragArea",
    attrs: {
      "options": _vm.draggableOption
    },
    model: {
      value: (_vm.data.value),
      callback: function($$v) {
        _vm.$set(_vm.data, "value", $$v)
      },
      expression: "data.value"
    }
  }, [_vm._l((_vm.data.value), function(item) {
    return _c('div', {
      key: item
    }, [_vm._v(_vm._s(_vm.getItemByValue(item)))])
  }), _vm._v(" "), _vm._l((_vm.data.value), function(item) {
    return _c('el-tag', {
      key: item,
      attrs: {
        "size": "small"
      }
    }, [_vm._v(_vm._s(_vm.getItemByValue(item)))])
  })], 2)], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1948c8f4", esExports)
  }
}

/***/ }),
/* 267 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(268);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 268 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_9e1e4dbc_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(270);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_9e1e4dbc_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/check-field/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-9e1e4dbc", Component.options)
  } else {
    hotAPI.reload("data-v-9e1e4dbc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 269 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
//
//
//
//
//
//
//

exports.default = {
	name: 'CheckField',

	props: ["field", "entity"],

	data: function data() {
		return {};
	}
};

/***/ }),
/* 270 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "entity-field"
  }, [_c('b-form-checkbox', {
    attrs: {
      "value": "true",
      "unchecked-value": "false"
    },
    model: {
      value: (_vm.entity[_vm.field.id]),
      callback: function($$v) {
        _vm.$set(_vm.entity, _vm.field.id, $$v)
      },
      expression: "entity[field.id]"
    }
  }, [_vm._v(_vm._s(_vm.field.name ? _vm.field.name : '') + "\n\t  ")])], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-9e1e4dbc", esExports)
  }
}

/***/ }),
/* 271 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(272);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 272 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_6919b99e_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(274);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_6919b99e_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/switch-field/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6919b99e", Component.options)
  } else {
    hotAPI.reload("data-v-6919b99e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 273 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  name: 'SwitchField',

  props: ["field", "entity", "disabled"],

  data: function data() {
    return {};
  }
};

/***/ }),
/* 274 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "entity-field"
  }, [_c('el-switch', {
    attrs: {
      "disabled": _vm.disabled,
      "active-text": "",
      "inactive-text": ""
    },
    model: {
      value: (_vm.entity[_vm.field.id]),
      callback: function($$v) {
        _vm.$set(_vm.entity, _vm.field.id, $$v)
      },
      expression: "entity[field.id]"
    }
  })], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-6919b99e", esExports)
  }
}

/***/ }),
/* 275 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(276);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 276 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1abf3e30_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(278);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1abf3e30_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/view-type-form/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1abf3e30", Component.options)
  } else {
    hotAPI.reload("data-v-1abf3e30", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 277 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  name: 'ViewTypeForm',

  props: ["spec", "entities", "multiple", "showLabels"],

  data: function data() {
    return {
      type: {
        number: [3, 4, 'number'],
        switch: [2, 'switch'],
        text: [5, 6, 7, 'text'],
        date: [8, 9, 10, 'datetime'],
        select: [1, 16, 18, 19, 'select'],
        check: [11, 17, 'check'],
        transfer: [20, 'transfer']
      }
    };
  },

  methods: {
    emitSave: function emitSave(entityId) {
      if (!this.multiple || entityId != null) {
        this.$emit('save', true);
      }
    },
    emitReset: function emitReset(entityId) {
      if (!this.multiple || entityId != null) {
        this.$emit('reset', true);
      }
    },
    isType: function isType(fieldType, typeCompare) {
      return _.indexOf(typeCompare, fieldType) > -1;
    },
    evalCondition: function evalCondition(field, entity) {
      return this.evalBooleanExpression(field, 'visible', entity, true);
    },
    evalDisabled: function evalDisabled(field, entity) {
      return this.evalBooleanExpression(field, 'disabled', entity, false);
    },
    evalBooleanExpression: function evalBooleanExpression(field, property, entity, defaultValue) {
      if (field[property]) {
        var expr = field[property];
        for (var property in entity) {
          expr = expr.replace("{{" + property + "}}", entity[property]);
        }
        try {
          return eval(expr);
        } catch (err) {
          return defaultValue;
        }
      }
      return defaultValue;
    },
    getFieldLabel: function getFieldLabel(field) {
      return this.showLabels ? field.label + ':' : null;
    },
    getFieldData: function getFieldData(entity, fieldId) {
      return { value: entity[fieldId] };
    },
    removeEntity: function removeEntity(entityIndex) {
      if (this.entities[entityIndex].id > 0) {
        this.entities[entityIndex].id = this.entities[entityIndex].id * -1;
        this.$emit('onDelete', true);
      } else {
        this.entities.splice(entityIndex, 1);
      }
    }
  }
};

/***/ }),
/* 278 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', _vm._l((_vm.entities), function(entity, entityIndex) {
    return (!(entity.id < 0)) ? _c('div', {
      key: entity.id,
      staticClass: "entity-form"
    }, [_c('el-form', {
      class: [_vm.showLabels ? '' : 'noLabels', (_vm.entities.length > 1) && (_vm.spec.fields.length > 1) ? 'formEntitySeparator' : ''],
      attrs: {
        "label-position": "right"
      },
      nativeOn: {
        "submit": function($event) {
          $event.preventDefault();
        }
      }
    }, [(_vm.multiple) ? _c('el-button', {
      class: ['deleteButton', entity.id < 0 ? 'fa fa-undo' : 'fa fa-times'],
      attrs: {
        "type": "text"
      },
      on: {
        "click": function($event) {
          _vm.removeEntity(entityIndex)
        }
      }
    }) : _vm._e(), _vm._v(" "), _vm._l((_vm.spec.fields), function(field, fieldIndex) {
      return _c('div', {
        key: fieldIndex
      }, [(_vm.evalCondition(field, entity)) ? _c('el-form-item', {
        attrs: {
          "label": _vm.getFieldLabel(field)
        }
      }, [(_vm.isType(field.type, _vm.type.text)) ? _c('text-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.number)) ? _c('number-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.select)) ? _c('select-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity),
          "fieldName": field.id
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.check)) ? _c('check-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.switch)) ? _c('switch-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.date)) ? _c('date-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.transfer)) ? _c('transfer-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity),
          "fieldName": field.id
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e()], 1) : _vm._e()], 1)
    })], 2)], 1) : _vm._e()
  }))
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1abf3e30", esExports)
  }
}

/***/ }),
/* 279 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(280);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 280 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1113116c_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(282);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1113116c_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/view-type-form-collapse/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1113116c", Component.options)
  } else {
    hotAPI.reload("data-v-1113116c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 281 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  name: 'ViewTypeFormCollapse',

  props: ["spec", "entities", "multiple", "showLabels"],

  data: function data() {
    return {
      type: {
        number: [3, 4, 'number'],
        switch: [2, 'switch'],
        text: [5, 6, 7, 'text'],
        date: [8, 9, 10, 'datetime'],
        select: [1, 16, 18, 19, 'select'],
        check: [11, 17, 'check'],
        transfer: [20, 'transfer']
      },
      activeName: 0
    };
  },

  methods: {
    emitSave: function emitSave(entityId) {
      if (!this.multiple || entityId != null) {
        this.$emit('save', true);
      }
    },
    emitReset: function emitReset(entityId) {
      if (!this.multiple || entityId != null) {
        this.$emit('reset', true);
      }
    },
    isType: function isType(fieldType, typeCompare) {
      return _.indexOf(typeCompare, fieldType) > -1;
    },
    evalCondition: function evalCondition(field, entity) {
      return this.evalBooleanExpression(field, 'visible', entity, true);
    },
    evalDisabled: function evalDisabled(field, entity) {
      return this.evalBooleanExpression(field, 'disabled', entity, false);
    },
    evalBooleanExpression: function evalBooleanExpression(field, property, entity, defaultValue) {
      if (field[property]) {
        var expr = field[property];
        for (var property in entity) {
          expr = expr.replace("{{" + property + "}}", entity[property]);
        }
        try {
          return eval(expr);
        } catch (err) {
          return defaultValue;
        }
      }
      return defaultValue;
    },
    getFieldLabel: function getFieldLabel(field) {
      return this.showLabels ? field.label + ':' : null;
    },
    getSpecEntityLabel: function getSpecEntityLabel(entity) {
      var label = '';
      var value = '';
      var spec = this.spec;
      var keys = Object.keys(entity);
      for (var i = 0; i < spec.label.format.length; i++) {
        if (Number.isInteger(spec.label.format[i])) {
          value = entity[keys[spec.label.format[i]]];
          if (value != null) {
            label += value;
          }
        } else {
          label += spec.label.format[i];
        }
      }

      if (entity.id < 0) {
        label += ' (para ser eliminado)';
      }
      return label;
    },
    removeEntity: function removeEntity(entityIndex) {
      if (this.entities[entityIndex].id > 0) {
        this.entities[entityIndex].id = this.entities[entityIndex].id * -1;
        this.$emit('onDelete', true);
      } else {
        this.entities.splice(entityIndex, 1);
      }
    }
  }
};

/***/ }),
/* 282 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', _vm._l((_vm.entities), function(entity, entityIndex) {
    return _c('el-collapse', {
      key: entityIndex,
      attrs: {
        "accordion": ""
      },
      model: {
        value: (_vm.activeName),
        callback: function($$v) {
          _vm.activeName = $$v
        },
        expression: "activeName"
      }
    }, [(!(entity.id < 0)) ? _c('el-collapse-item', {
      attrs: {
        "name": entityIndex
      }
    }, [_c('template', {
      slot: "title"
    }, [_c('p', {
      domProps: {
        "innerHTML": _vm._s(_vm.getSpecEntityLabel(entity))
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "entity-form"
    }, [_c('el-form', {
      class: [_vm.showLabels ? '' : 'noLabels', (_vm.entities.length > 1) && (_vm.spec.fields.length > 1) ? 'formEntitySeparator' : ''],
      attrs: {
        "label-position": "right"
      },
      nativeOn: {
        "submit": function($event) {
          $event.preventDefault();
        }
      }
    }, [(_vm.multiple) ? _c('el-button', {
      class: ['deleteButton', entity.id < 0 ? 'fa fa-undo' : 'fa fa-times'],
      attrs: {
        "type": "text"
      },
      on: {
        "click": function($event) {
          _vm.removeEntity(entityIndex)
        }
      }
    }) : _vm._e(), _vm._v(" "), _vm._l((_vm.spec.fields), function(field, fieldIndex) {
      return _c('div', {
        key: fieldIndex
      }, [(_vm.evalCondition(field, entity)) ? _c('el-form-item', {
        attrs: {
          "label": _vm.getFieldLabel(field)
        }
      }, [(_vm.isType(field.type, _vm.type.text)) ? _c('text-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.number)) ? _c('number-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.select)) ? _c('select-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity),
          "fieldName": field.id
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.check)) ? _c('check-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.switch)) ? _c('switch-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.date)) ? _c('date-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity)
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e(), _vm._v(" "), (_vm.isType(field.type, _vm.type.transfer)) ? _c('transfer-field', {
        attrs: {
          "field": field,
          "entity": entity,
          "disabled": _vm.evalDisabled(field, entity),
          "fieldName": field.id
        },
        on: {
          "save": function($event) {
            _vm.emitSave(entity.id)
          },
          "reset": function($event) {
            _vm.emitReset(entity.id)
          }
        }
      }) : _vm._e()], 1) : _vm._e()], 1)
    })], 2)], 1)], 2) : _vm._e()], 1)
  }))
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1113116c", esExports)
  }
}

/***/ }),
/* 283 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__src_main_vue__ = __webpack_require__(284);


/* istanbul ignore next */
__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].install = function(Vue) {
  Vue.component(__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */].name, __WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);
};

/* harmony default export */ __webpack_exports__["a"] = (__WEBPACK_IMPORTED_MODULE_0__src_main_vue__["a" /* default */]);


/***/ }),
/* 284 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1483ae34_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__ = __webpack_require__(286);
var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_1483ae34_hasScoped_false_vue_loader_lib_selector_type_template_index_0_main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "packages/view-type-table/src/main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1483ae34", Component.options)
  } else {
    hotAPI.reload("data-v-1483ae34", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 285 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var _vuex = __webpack_require__(6);

exports.default = {
	name: 'ViewTypeTable',

	props: ["spec", "entities"],
	data: function data() {
		return {};
	},
	computed: _extends({}, (0, _vuex.mapGetters)(['baseUrl'])),
	methods: _extends({}, (0, _vuex.mapMutations)(['setLayoutUrl', 'setLayoutDialogUrl']), {
		getItemIconClass: function getItemIconClass(icon) {
			return "sfa fa fa-" + icon;
		},
		getButtonClass: function getButtonClass(isIcon) {
			if (isIcon) {
				return "buttonAction";
			} else {
				return "";
			}
		},


		clone: function clone(obj) {
			return JSON.parse(JSON.stringify(obj));
		},

		getFieldLabel: function getFieldLabel(field) {
			return field.label + ':';
		},

		handleCommand: function handleCommand(command) {
			this.handleAction(command.entityIndex, command.actionLabel);
		},


		handleAction: function handleAction(entityIndex, actionLabel) {
			for (var a = 0; a < this.spec.actionLinks.length; a++) {
				if (this.spec.actionLinks[a].label == actionLabel) {
					var link = this.spec.actionLinks[a].link;
					for (var f = 0; f < this.spec.fields.length; f++) {
						link = link.replace("{{" + this.spec.fields[f].label + "}}", this.entities[entityIndex][this.spec.fields[f].id]);
					}

					link = link.replace("{{specHash}}", this.spec.hash);
					link = link.replace("{{entityId}}", this.entities[entityIndex].id);

					switch (this.spec.actionLinks[a].mothod) {
						case "setLayout":
							this.setLayoutUrl(link);
							break;
						case "setLayoutDialog":
							var title = this.spec.actionLinks[a].title;
							for (var f = 0; f < this.spec.fields.length; f++) {
								title = title.replace("{{" + this.spec.fields[f].label + "}}", this.entities[entityIndex][this.spec.fields[f].id]);
							}
							var preventClose = this.spec.actionLinks[a].preventClose;
							if (this.spec.actionLinks[a].hasOwnProperty('extraParams')) {
								var extraParams = this.spec.actionLinks[a].extraParams;
							} else {
								var extraParams = {};
							}
							var instance = this;
							this.setLayoutDialogUrl({
								'url': link,
								'title': title,
								'preventClose': preventClose,
								'extraParams': extraParams,
								'instance': instance
							});
							break;
						case "get":
							//text = "I am not a fan of orange.";
							break;
						case "post":
							var obj = this.clone(this.spec.actionLinks[a].model);
							for (var p in obj) {
								if (obj.hasOwnProperty(p) && typeof obj[p] == "string") {
									for (var f = 0; f < this.spec.fields.length; f++) {
										obj[p] = obj[p].replace("{{" + this.spec.fields[f].label + "}}", this.entities[entityIndex][this.spec.fields[f].id]);
									}
									obj[p] = obj[p].replace("{{specHash}}", this.spec.hash);
									obj[p] = obj[p].replace("{{id}}", this.entities[entityIndex].id);
								}
							}

							var msg = this.spec.actionLinks[a].confirmation;
							if (this.entities[entityIndex].defaultLabel) {
								msg = msg.replace("{{defaultLabel}}", this.entities[entityIndex].defaultLabel);
							}
							for (var f = 0; f < this.spec.fields.length; f++) {
								msg = msg.replace("{{" + this.spec.fields[f].label + "}}", this.entities[entityIndex][this.spec.fields[f].id]);
							}
							var instance = this;
							this.$confirm(msg, '', {
								confirmButtonText: 'Si',
								cancelButtonText: 'No',
								type: 'warning'
							}).then(function () {
								instance.$emit('reset', false);
								axios.post(instance.baseUrl + link, JSON.stringify(obj)).then(function (response) {
									instance.$emit('reset');
									instance.$notify({
										type: 'success',
										message: response.data.msg,
										position: 'bottom-right'
									});
								}).catch(function (error) {
									instance.$notify({
										type: 'error',
										message: 'Error al ejecutar acción',
										position: 'bottom-right'
									});
								});
							}).catch(function () {
								/*this.$message({
          type: 'info',
          message: 'Delete canceled'
        });*/
							});
							break;
						default:
							console.log('Undefined command type');
					}

					break;
				}
			}
		}
	})
};

/***/ }),
/* 286 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('el-table', {
    staticStyle: {
      "width": "100%"
    },
    attrs: {
      "data": _vm.entities
    }
  }, [_vm._l((_vm.spec.fields), function(field, fieldIndex) {
    return (field.subtype != 'hidden') ? _c('el-table-column', {
      key: field.id,
      attrs: {
        "label": field.label,
        "prop": field.id
      }
    }) : _vm._e()
  }), _vm._v(" "), (_vm.spec.showActionLinks) ? _c('el-table-column', {
    attrs: {
      "fixed": "right",
      "label": "Acciones",
      "width": "150"
    },
    scopedSlots: _vm._u([{
      key: "default",
      fn: function(scope) {
        return [_vm._l((_vm.spec.actionLinks), function(action, index) {
          return _c('div', {
            staticClass: "buttonAction"
          }, [(action.icon && ((index <= 1) || (_vm.spec.actionLinks.length == 3 && index == 2))) ? _c('el-button', {
            key: action.label,
            staticClass: "buttonAction",
            attrs: {
              "type": "text",
              "size": "small"
            },
            on: {
              "click": function($event) {
                _vm.handleAction(scope.$index, action.label)
              }
            }
          }, [_c('i', {
            class: _vm.getItemIconClass(action.icon),
            attrs: {
              "aria-hidden": "true"
            }
          })]) : _vm._e(), _vm._v(" "), (!action.icon && ((index <= 1) || (_vm.spec.actionLinks.length == 3 && index == 2))) ? _c('el-button', {
            key: action.label,
            attrs: {
              "type": "text",
              "size": "small"
            },
            on: {
              "click": function($event) {
                _vm.handleAction(scope.$index, action.label)
              }
            }
          }, [_vm._v("\n\t\t        \t\t" + _vm._s(action.label) + "\n\t\t       \t")]) : _vm._e()], 1)
        }), _vm._v(" "), (_vm.spec.actionLinks.length > 3) ? _c('el-dropdown', {
          staticStyle: {
            "margin-left": "15px"
          },
          on: {
            "command": _vm.handleCommand
          }
        }, [_c('span', {
          staticClass: "el-dropdown-link"
        }, [_c('i', {
          staticClass: "el-icon-arrow-down el-icon--right"
        })]), _vm._v(" "), _c('el-dropdown-menu', {
          attrs: {
            "slot": "dropdown"
          },
          slot: "dropdown"
        }, _vm._l((_vm.spec.actionLinks), function(action, index) {
          return (index > 1) ? _c('el-dropdown-item', {
            key: action.label,
            attrs: {
              "command": {
                entityIndex: scope.$index,
                actionLabel: action.label
              }
            }
          }, [_vm._v(_vm._s(action.label))]) : _vm._e()
        }))], 1) : _vm._e()]
      }
    }])
  }) : _vm._e()], 2)], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1483ae34", esExports)
  }
}

/***/ }),
/* 287 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_Main_vue__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_Main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_Main_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_426b26b2_hasScoped_false_vue_loader_lib_selector_type_template_index_0_Main_vue__ = __webpack_require__(293);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(288)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_Main_vue___default.a,
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_426b26b2_hasScoped_false_vue_loader_lib_selector_type_template_index_0_Main_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/Main.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Main.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-426b26b2", Component.options)
  } else {
    hotAPI.reload("data-v-426b26b2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 288 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(289);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(290)("40c17b91", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-426b26b2\",\"scoped\":false,\"hasInlineConfig\":false}!../../vue-loader/lib/selector.js?type=styles&index=0!./Main.vue", function() {
     var newContent = require("!!../../css-loader/index.js!../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-426b26b2\",\"scoped\":false,\"hasInlineConfig\":false}!../../vue-loader/lib/selector.js?type=styles&index=0!./Main.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 289 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(undefined);
// imports


// module
exports.push([module.i, "\n#app {\n  font-family: Helvetica, sans-serif;\n  text-align: center;\n}\n", ""]);

// exports


/***/ }),
/* 290 */
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(291)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),
/* 291 */
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),
/* 292 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//

exports.default = {};

/***/ }),
/* 293 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('app', {
    attrs: {
      "urlMenu": "js/data/menu.json"
    }
  })
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-426b26b2", esExports)
  }
}

/***/ })
],[195]);