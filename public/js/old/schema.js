// register global component
Vue.component('my-components', {
	template: '<button v-on:click="counter += 1">{{ counter }}</button>',
	// data is technically a function, so Vue won't
	// complain, but we return the same object
	// reference for each component instance
	data: function () {
	  return {
	    counter: 0
	  }
	}
})

Vue.component('my-component', {
	template: '\
	<div>\
		<button id="show-right" @click="showRight = true">New Post</button>\
		<b-alert :show.sync="showRight" state="success" dismissible @dismissed="showRight=false">\
          This is an alert\
        </b-alert>\
    </div>',
	props:[
		    'showRight'
		  ]
})

Vue.component('button-counter', {
  template: '<button v-on:click="increment" v-show="counter < 3">{{ counter }}</button>',
  data: function () {
    return {
      counter: 0
    }
  },
  methods: {
    increment: function () {
      this.counter += 1
      this.$emit('increment')
    }
  },
})

var Child = {
  template: '<div>A custom component!</div>'
}


var schema = new Vue({
  	el: '#schema',
  	data: {
    	message: 'Hello Vue!',
    	type: 'A',
    	items: [
    		{
    			name: 's',
    			message: 'zzz'
    		},
    		{
    			name: 'qs',
    			message: "zzz"
    		}
    	],
    	total: 0,
    	value: '2017-01-01',
    	disabled: false,
    	format: 'dd-MM-yyyy',
    	clear: true,
    	placeholder: '',
    	disabledDaysOfWeek: []
  	},
  	methods: {
		reverseMessage: function () {		
	    	this.message = this.message.split('').reverse().join('')
		},
		addItem: function(e) {
			e.preventDefault();
		 	this.items.push(Vue.util.extend({}, {name:'sw', message:'x'}));
		},
		incrementTotal: function () {
	      this.total += 1
	    }
	},
	components: {
		'my-component': Child
	}
})

schema.type = 'C';



    var vm = new Vue({
        components: {
        },
        el: "#app",
        data: {
        	duration: 3000,
            showRight: false,
            showTop: false
        }
    })
