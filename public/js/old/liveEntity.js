Vue.component('live-text-field', {
  template: '\
    <div class="form-group">\
      <label for="fieldOwnerid" class="col-sm-2 control-label">{{field.label}}</label>\
      <div v-if="field.type === text" class="col-sm-10">\
         <input class="form-control" type="text" v-model="newField.value"></input>\
      </div>\
    </div>',
    props: ["url","field"],
    data: function () {
      return {
        newField:{},
        text: 'text'  
      }
    },
    created: function () {
      this.newField = this.field;
    },
    watch: {
      value: function (newValue) {
        this.newField.value = newValue; 
        this.setUrlData();      
      }
    },
    computed: {
      value: function() {
        return this.newField.value;        
      }
    },
    methods: {
      getUrlData: _.debounce(
        function () {
          var instance = this          
          axios.get(instance.url)
            .then(function (response) {
              instance.values = response.data;
            })
            .catch(function (error) {
              //instance.answer = 'Error! Could not reach the API. ' + error
            })          
        },
        // This is the number of milliseconds we wait for the
        // user to stop typing.
        //500
      ),
      setUrlData: _.debounce(
        function () {
          var instance = this          
          axios.get(instance.url)
            .then(function (response) {
              //instance.values = response.data;
              
              console.log(instance.newField.value);
            })
            .catch(function (error) {
              //instance.answer = 'Error! Could not reach the API. ' + error
            })          
        },
        2000
        // This is the number of milliseconds we wait for the
        // user to stop typing.
        //500
      )
    }
});

Vue.component('entity-live-form', {
  template: '\
    <div class="form-horizontal">\
      <div v-for="(field, index) in entity.fields">\
          <div v-if="field.type === text">\
             <live-text-field :url="url" :field="field">{{ index }} </text-field>\
          </div>\
      </div>\
      <div class="form-group">\
        <div class="col-sm-offset-2 col-sm-10">\
            <input value="Save" class="btn btn-default" type="submit" v-on:click="save()">\
            <input value="Reload" class="btn btn-default" type="button" v-on:click="getUrlData()">\
        </div>\
      </div>\
    </div>',
    props: ["url"],
    data: function () {
      return {
        originalValues: {fields:[]},
        values: {fields:[]},
        text: 'text'  
      }
    },
    created: function () {
      this.getUrlData();
    },
    watch: {
      entity: function (value) {
        this.values = value;        
      }
    },
    computed: {
      entity: function() {
        return this.values;        
      }
    },
    methods: {
      getUrlData: _.debounce(
        function () {
          var instance = this          
          axios.get(instance.url)
            .then(function (response) {
              instance.values = response.data;       
            })
            .catch(function (error) {
              //instance.answer = 'Error! Could not reach the API. ' + error
            })          
        },
        // This is the number of milliseconds we wait for the
        // user to stop typing.
        //500
      ),
      save: function() {
        console.log(JSON.stringify(this.values));
      }
    }
});

var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello Vue!',
      type: 'A',
      aa:null,
      entity: [
              {
                  name: 's',
                  message: 'zzz'
              },
              {
                  name: 'qs',
                  message: "zzz"
              }
          ],
      entity_20_items: []
    },
    methods: {
      loadItems: function () {alert(this.url);
        this.entity_20_items=['1','2'];
      },
      updateValue: function() {
          /*this.$http.get(this.url).then(function(response) { 
            alert(1);
              this.items = response.data;
          }.bind(this));*/
      }
    }
})
