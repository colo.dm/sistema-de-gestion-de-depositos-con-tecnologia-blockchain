Vue.component('text-field', {
  template: '\
    <div>\
      <input v-if="field.subtype === subtype.title" class="form-control input-title" type="text" :placeholder="field.placeholder"\
          v-model="field.value"/>\
      <input v-if="field.subtype === subtype.text" class="form-control" type="text" :placeholder="field.placeholder"\
          v-model="field.value"/>\
      <textarea v-if="field.subtype === subtype.textArea" class="form-control" type="text" :placeholder="field.placeholder"\
          v-model="field.value"/>\
    </div>',
  props: ["url","field"],
  data: function () {
    return {
      subtype: { 
        text: 'text',
        textArea: 'textArea',
        title: 'title' 
      }
    }
  }
});

Vue.component('number-field', {
  template: '\
    <div>\
      <input v-if="field.subtype === subtype.int"  class="form-control" type="text" :placeholder="field.placeholder"\
        ref="input" v-bind:value="field.value" v-on:input="updateValue($event.target.value)"\
        v-on:keypress="isInt"/>\
      <input v-if="field.subtype === subtype.decimal" class="form-control" type="text" :placeholder="field.placeholder"\
        ref="input" v-bind:value="field.value" v-on:input="updateValue($event.target.value)"\
        v-on:keypress="isDecimal"/>\
    </div>',
    props: ["url","field"],
    data: function () {
      return {
        subtype: {
          int: 'int',
          decimal: 'decimal'
        },
        value: null
      }
    },
    created: function () {
      this.separator = 46; //.
      this.decimals = 2;
    },
    methods: {
      isInt: function(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode > 31 && (charCode < 48 || charCode > 57))) {
          event.preventDefault();
        } else {
          return true;
        }
      },
      isDecimal: function(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== this.separator ) {
          event.preventDefault();
        } else {
          return true;
        }
      },
      updateValue: function (value) {
        var formattedValue = value;
        switch (this.field.subtype){
          case this.subtype.decimal:
            formattedValue.trim()
            .slice(
              0,
              value.indexOf('.') === -1
                ? value.length
                : value.indexOf('.') + 3
            );
            this.$emit('input', Number(formattedValue).toFixed(this.decimals));
            break;
          case this.subtype.int:
            this.$emit('input', parseInt(formattedValue, 10));
            break;
        }
        if (formattedValue !== value) {
          //this.$refs.input.value = formattedValue
        }        
      }
    }
});

Vue.component('select-field', {
  template: '\
    <div>\
      <v-select v-model="field.value" :options="field.options" :multiple="field.multiple" :taggable="field.taggable"\
      v-bind:class="[field.multiple ? \'\' : singleSelect]"></v-select>\
    </div>',
  props: ["url","field"],
  data: function () {
    return {
      singleSelect: 'v-select-single'
    }
  }
});

Vue.component('check-field', {
  template: '\
    <div>\
      <b-form-checkbox v-model="field.value" value="true" unchecked-value="false">{{field.label ? field.label : text}}\
      </b-form-checkbox>\
    </div>',
  props: ["url","field","text"],
  data: function () {
    return {
    }
  }
});


Vue.component('entity-form', {
  template: '\
    <div class="entity-form">\
      <b-card class="mb-2" show-header show-footer>\
        <h3 slot="header" class="text-muted">\
            <label class="col-sm-2 control-label">{{label}}</label>\
        </h3>\
        <div class="form-horizontal">\
          <div v-for="(field, index) in entity.fields">\
            <div class="form-group row">\
              <label class="col-sm-2 control-label">{{field.label}}</label>\
              <div class="col-sm-3">\
                <text-field   v-if="field.type === type.text" :field="field"/>\
                <number-field v-if="field.type === type.number" :field="field" v-model="field.value"/>\
                <select-field v-if="field.type === type.select" :field="field" v-model="field.value"/>\
                <check-field v-if="field.type === type.check" :field="field" />\
              </div>\
            </div>\
          </div>\
        </div>\
        <small slot="footer" class="text-muted">\
              <div class="form-group">\
                <div class="col-sm-offset-2 col-sm-10">\
                    <input value="Save" class="btn btn-default" type="submit" v-on:click="save()">\
                    <input value="Reload" class="btn btn-default" type="button" v-on:click="getUrlData()">\
                    <input value="Add Field" class="btn btn-default" type="button" v-on:click="addField()">\
                </div>\
              </div>\
        </small>\
      </b-card>\
    </div>',
    props: ["url"],
    data: function () {
      return {
        originalValues: {
          fields:[]
        },
        values: {
          label:{
            format:[]
          },
          fields:[]
        },
        type: {
          text: 'text',
          number: 'number',
          select: 'select',
          check: 'check'
        }
      }
    },
    created: function () {
      this.getUrlData();
    },
    watch: {
      entity: function (value) {
        this.values = value;        
      }
    },
    computed: {
      entity: function() {
        return this.values;        
      },
      label: function(){
        var format = this.values.label.format;
        var output = "";
        for (var i = 0; i < format.length; i++) {
          if (typeof format[i] === 'string'){
            output += format[i]; 
          } else {
            var selObj = this.values.fields.filter(function( obj ) {
              return obj.id == format[i];
            });
            output += selObj[0].value;
          }
        }
        return output;
      }
    },
    methods: {
      clone: function( obj ){
        return JSON.parse(JSON.stringify(obj));
      },
      getUrlData: _.debounce(
        function () {
          var instance = this          
          axios.get(instance.url)
            .then(function (response) {
              instance.values = response.data;
              instance.originalValues = instance.clone(instance.values); 
            })
            .catch(function (error) {
              //instance.answer = 'Error! Could not reach the API. ' + error
            })          
        },
        500
      ),
      save: function() {
        var entity = {
          'id': this.originalValues.id,
          'type': this.originalValues.type,
          'spec': {
            name:{
              value: this.values.spec.name.value
            }
          },
          'fields':[]
        };
        for (var i = 0; i < this.values.fields.length; i++) {
          if (this.values.fields[i].value != this.originalValues.fields[i].value){
            var field = {
              'id': this.values.fields[i].id,
              'value': this.values.fields[i].value
            };
            entity.fields.push(field);
          }
        }
        console.log(JSON.stringify(this.originalValues));
        console.log(JSON.stringify(this.values));
        console.log(JSON.stringify(entity));
      }
    }
});


Vue.component('spec-entity-form', {
  template: '\
    <div >\
      <b-card v-if="entity.spec.editable" class="mb-2" show-header show-footer>\
        <h3 slot="header" class="text-muted">\
            <text-field class="entityNameForm" :field="entity.spec.name"/>\
        </h3>\
        <div class="form-horizontal">\
          <div v-for="(field, index) in entity.fields">\
            <div class="form-group row">\
              <label v-if="!entity.spec.editable" class="col-sm-2 control-label">{{field.label}}</label>\
              <input v-if="entity.spec.editable" type="text" v-model="field.label" :placeholder="field.labelPlaceholder"\
              class="col-sm-2 control-label input-label"></input>\
              <div class="col-sm-3">\
                <select-field v-if="field.type === type.select" :field="field" v-model="field.value"/>\
              </div>\
              <div class="col-sm-3">\
                <check-field :field="field.required" :text="required"/>\
              </div>\
            </div>\
            <div v-if="field.value.details" class="form-group row spec-type-options">\
              <label class="col-sm-2 control-label spec-type-label-hidden"></label>\
              <div class="spec-type-options-detail"  v-for="(option, index) in field.value.details">\
                <label class="col-sm-6 control-label">{{option.label}}</label>\
                <div class="col-sm-10">\
                  <text-field   v-if="option.type === type.text" :field="option"/>\
                  <number-field v-if="option.type === type.number" :field="option" />\
                  <select-field v-if="option.type === type.select" :field="option" />\
                  <check-field v-if="option.type === type.check" :field="option" />\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
        <small slot="footer" class="text-muted">\
              <div class="form-group">\
                <div class="col-sm-offset-2 col-sm-10">\
                    <input value="Save" class="btn btn-default" type="submit" v-on:click="save()">\
                    <input value="Reload" class="btn btn-default" type="button" v-on:click="getUrlData()">\
                    <input value="Add Field" class="btn btn-default" type="button" v-on:click="addField()">\
                </div>\
              </div>\
        </small>\
      </b-card>\
    </div>',
    props: ["url"],
    data: function () {
      return {
        originalValues: {
          spec:{editable:false},
          fields:[]
        },
        values: {
          spec:{editable:false},
          fields:[]
        },
        type: {
          text: 'text',
          number: 'number',
          select: 'select',
          check: 'check'
        },
        required: "Required"
      }
    },
    created: function () {
      this.getUrlData();
    },
    watch: {
      entity: function (value) {
        this.values = value;        
      }
    },
    computed: {
      entity: function() {
        return this.values;        
      }
    },
    methods: {
      clone: function( obj ){
        return JSON.parse(JSON.stringify(obj));
      },
      getUrlData: _.debounce(
        function () {
          var instance = this          
          axios.get(instance.url)
            .then(function (response) {
              instance.values = response.data;
              instance.originalValues = instance.clone(instance.values); 
            })
            .catch(function (error) {
              //instance.answer = 'Error! Could not reach the API. ' + error
            })          
        },
        500
      ),
      save: function() {
        var entity = {
          'id': this.originalValues.id,
          'type': this.originalValues.type,
          'spec': {
            name:{
              value: this.values.spec.name.value
            }
          },
          'fields':[]
        };/*
        for (var i = 0; i < this.values.fields.length; i++) {
          if (this.values.fields[i].value != this.originalValues.fields[i].value){
            var field = {
              'id': this.values.fields[i].id,
              'value': this.values.fields[i].value
            };
            entity.fields.push(field);
          }
        }*/
        console.log(JSON.stringify(this.originalValues));
        console.log(JSON.stringify(this.values));
        console.log(JSON.stringify(entity));
      },
      addField: function() {
        this.entity.fields.push(this.clone(this.values.newField));
      }
    }
});

var app = new Vue({
    el: '#app',
    data: {
      dismissCountDown: null,
      showDismissibleAlert: false
    },
    methods: {
      countDownChanged(dismissCountDown) {
          this.dismissCountDown = dismissCountDown;
        },
        showAlert() {
          this.dismissCountDown = 5;
        }
    }
});